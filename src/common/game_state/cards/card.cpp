#include "card.h"

#include "../../exceptions/BohnanzaException.h"


// Constructors and Deconstructors
card::card(std::string id) : unique_serializable(id) { }

card::card(std::string id, serializable_value<int> *type)
        : unique_serializable(id), _bean_type(type)
{ }

card::card(int type) : unique_serializable(), _bean_type(new serializable_value<int>(type)){ }


card::~card() { }

// Class functions

int card::get_bean_type() const noexcept {
    return _bean_type->get_value();
}

bool card::can_be_played_on(const card *const other) const noexcept {
    // return true if bean_type is matching
    return (this->get_bean_type() == other->get_bean_type());
}


void card::write_into_json(rapidjson::Value &json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);

    rapidjson::Value value_val(rapidjson::kObjectType);
    _bean_type->write_into_json(value_val, allocator);
    json.AddMember("bean_type", value_val, allocator);
}


card *card::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") && json.HasMember("bean_type")) {
        return new card(json["id"].GetString(), serializable_value<int>::from_json(json["bean_type"].GetObject()));
    } else {
        throw BohnanzaException("Could not parse json of card. Was missing 'id' or 'val'.");
    }
}

