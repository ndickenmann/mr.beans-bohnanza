# Mr.Beans-Bohnanza: To Bean or not to Bean?
Welcome to the card game Bohnanza! This project aims to provide a digital version of the popular card game, implemented in C++. Bohnanza is a strategic trading card game where players cultivate and harvest different types of beans.

The implementation uses the following libraries:
GUI : wxWidgets (https://www.wxwidgets.org/)
Network interface: sockpp (https://github.com/fpagliughi/sockpp)
Object serialization: rapidjson (https://rapidjson.org/md_doc_tutorial.html)
Unit tests: googletest (https://github.com/google/googletest)

The project was built for the lecture "Software Engineering" at ETH Zürich.

Hope you enjoy :)

## Rules
Bohnanza is a strategic trading card game where players take on the role of bean farmers. The objective of the game is to plant, harvest, and sell different types of beans in order to earn coins.

The game is played with a deck of cards, where each card represents a different bean. Players start with a hand of cards and must strategically plant them in their two bean fields. The order in which the cards are played is important because players are not allowed to rearrange the cards in their fields.

During each turn, the first card of the active player is added to the seed stack. All cards that land on the seed stacks next to the fields, have to be planted before the game can continue. Trading is a crucial aspect of the game, as players can negotiate and make deals to obtain the specific bean cards they need. All cards acquired during trading land on the seed stacks.

At the end of their turn, the active player draws two new cards from the deck. These cards can be traded on subsequent turns. Players can also choose to harvest beans from their fields, earning coins based on the number and type of beans they collect.

The game continues with players taking turns until the draw deck is depleted.  The player with the most coins at the end of the game is the winner.

It's important to manage your hand, make strategic trades, and maximize your earnings by harvesting and selling beans at the right time. The game combines elements of negotiation, strategy, and timing, making each playthrough unique and engaging.

The official rules of the game can be found here (or by clicking the logo in the connection panel):
https://www.riograndegames.com/wp-content/uploads/2013/02/Bohnanza-Rules.pdf

We have made some simplifications of the rules in this implementation:
- Players cannot play a second card at the start of their turn
- Seed stacks are limited to two cards and therefore trading also is limited to two different card types
- Trading is limited to a maximum of three offered and three requested cards
- We (only) implemented the base version of the game, which (only) supports 3-5 players

## Compile instructions
This project only works on UNIX systems (Linux / MacOS). We recommend using [Ubuntu](https://ubuntu.com/#download), as it offers the easiest way to set up wxWidgets. Therefore, we explain installation only for Ubuntu systems. The following was tested on a Ubuntu 20.4 system, but should also work for earlier versions of Ubuntu.

**Note:** If you create a virtual machine, we recommend to give the virtual machine **at least 12GB** of (dynamic) harddrive space (CLion and wxWidgets need quite a lot of space).

### 1. Prepare OS Environment

#### Ubuntu 20.4
The OS should already have git installed. If not, you can use: 
`sudo apt-get install git`

Then use  `git clone` to fetch this repository.

Execute the following commands in a console:
1. `sudo apt-get update`
2. `sudo apt-get install build-essential` followed by `sudo reboot`
3. if on virtual machine : install guest-additions (https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm) and then `sudo reboot`
4. `sudo snap install clion --classic` this installs the latest stable CLion version
5. `sudo apt-get install libwxgtk3.0-gtk3-dev` this installs wxWidgets (GUI library used in this project)


### 2. Compile Code
1. Open Clion
2. Click `File > Open...` and there select the **/sockpp** folder of this project
3. Click `Build > Build all in 'Debug'`
4. Wait until sockpp is compiled (from now on you never have to touch sockpp again ;))
5. Click `File > Open...` select the **/cse-lama-example-project** folder
6. Click `Build > Build all in 'Debug'`
7. Wait until Lama-server, Lama-client and Lama-tests are compiled

## Run the Game
1. Open a console in the project folder, navigate into "cmake-build-debug" `cd cmake-build-debug`
2. Run server `./Bohnanza-server`
3. In new consoles run as many clients as you want players `./Bohnanza-client`


## Playing instructions

As general information: Since the order of the hand cards matter in this game it is important to note that the next card to play is on the left and the hand builds to the right.
Also: The amount of coins you earn for harvesting a specific number of beans is declared on the bean cards.

### Start of a Round
At the beginning of each round the first card of the active player automatically gets put on the first seed stack.

### Planting
When planting is possible and allowed a plant button appears. To plant a card the player has to select the card on the seed stack and press the plant button. This has to be done for each card separately.

### Harvesting
When harvesting is possible and allowed a harvest button appears. To harvest a bean field the field has to be selected and the harvest button pressed. Earned coins are automatically calculated and added.

### Trading
The active player can trade with all players simultaneously while all other players only have one trading option with the active player. All players can see all offered and requested cards but uninvolved trades have a lighter background and no accept button on the left.
If you are in the non-active position, you can request cards directly from the middle pile and the active player does not need to offer them.
As soon as two players press the accept button on the left, the trade gets executed automatically if the offered and requested cards match.

#### Active Player
The active player can select the trade partner by clicking on the middle cards in the corresponding trading row. The currently selected trade partner is indicated with green borders on the offered and requested cards from the active player.
When the active player has no intention to trade he han finish the trade with the button on the bottom left and the remaining middle cards are put on his seed stacks.
Selected middle cards from the other player do not have to be offered by the active player and are automatically taken from the middle pile.

#### Other Players
Non-active players can (but don't have to) request a middle card by clicking on it. The active player does not have to match this selected middle card in his offered cards.

## Known Issues

- When playing this game on separate devices via a network, sometimes the firewall cutts of requests and responses, which leads to a tcp connection error in the response listener. This often happens in the trading sequence since all player are communicating a lot with each other. As a result the client loses connection and the game becomes unplayable due to the next listed error
- To recognize a connection loss from a player on server side would require us to rewrite the networking part a lot. We would need to introduce client_alive requests and wait with a timeout for a client response. We decided to focus on the game and leave this implementation out. As a consequence a disconnection of the client allways results in an unplayable game.
- To recognize a server crash on client side we would need to introduce ping from client to server similarly to the issue above. 
- Some special characters, specifically ä ö and ü of the german alphabet cant be represented with the wx widgets library. Changing the charset did not show any effect.
- Client requests sent in a very short time interval leads to a crash on the server side. This error should technically not be possible to trigger on a normal human playthrough.
- wxWidgets has a very bad high resolution image handling in terms of display quality. (Or at least we do not know how to fix it.)

## Our Team

- Fabian the legend
- Tizian de grossi
- Flo de hustler
- 2x nicola
