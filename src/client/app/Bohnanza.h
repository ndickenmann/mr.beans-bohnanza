#ifndef LAMAUI_BOHNANZA_H
#define LAMAUI_BOHNANZA_H

#include <wx/wx.h>
#include "../windows/GameWindow.h"
#include "../GameController.h"


// Main app class
class Bohnanza : public wxApp
{
public:
    virtual bool OnInit();
};


#endif //LAMAUI_BOHNANZA_H
