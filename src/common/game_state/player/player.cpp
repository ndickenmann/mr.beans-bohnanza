#include "player.h"

#include "../../exceptions/BohnanzaException.h"

player::player(std::string name) : unique_serializable() {
    this->_player_name = new serializable_value<std::string>(name);
    this->_coins = new serializable_value<int>(0);
    this->_hand = new hand();
    this->_seed_stacks.push_back(new seed_stack());
    this->_seed_stacks.push_back(new seed_stack());

    this->_bean_fields.push_back(new field_stack());
    this->_bean_fields.push_back(new field_stack());
}

player::player(std::string id,
           serializable_value<std::string>* name,
           serializable_value<int>* coins,
           hand* hand,
           std::vector<seed_stack*> seed_stacks, 
           std::vector<field_stack*> bean_fields):
           unique_serializable(id),
           _player_name(name),
           _coins(coins),
           _hand(hand),
           _seed_stacks(seed_stacks),
           _bean_fields(bean_fields)
           {}

player::~player() {
    if (_player_name != nullptr) {
        delete _hand;
        delete _player_name;
        delete _coins;
        delete _seed_stacks.at(0);
        delete _seed_stacks.at(1);
        delete _bean_fields.at(0);
        delete _bean_fields.at(1);

        _hand = nullptr;
        _player_name = nullptr;
        _coins = nullptr;
        _seed_stacks.at(0) = nullptr;
        _seed_stacks.at(1) = nullptr;
        _bean_fields.at(0) = nullptr;
        _bean_fields.at(1) = nullptr;
    }
}

//#ifdef LAMA_SERVER
player::player(std::string id, std::string name) :
        unique_serializable(id)
{
    this->_player_name = new serializable_value<std::string>(name);
    this->_coins = new serializable_value<int>(0);
    this->_hand = new hand();
    this->_seed_stacks.push_back(new seed_stack());
    this->_seed_stacks.push_back(new seed_stack());

    this->_bean_fields.push_back(new field_stack());
    this->_bean_fields.push_back(new field_stack());
}

std::string player::get_game_id() {
    return _game_id;
}

void player::set_game_id(std::string game_id) {
    _game_id = game_id;
}

std::vector<field_stack*> player::get_bean_fields() const {
    return _bean_fields;
};

std::vector<seed_stack*> player::get_seed_stacks() const {
    return _seed_stacks;
};
//#endif

bool player::empty_seed_stack(){
    return (_seed_stacks.at(0)->get_nof_beans() == 0
    && _seed_stacks.at(1)->get_nof_beans() == 0);
}

int player::get_score() const noexcept {
    return _coins->get_value();
}

std::string player::get_player_name() const noexcept {
    return this->_player_name->get_value();
}

hand* player::get_non_const_hand() const noexcept {
    return this->_hand;
}

const hand* player::get_hand() const noexcept {
    return this->_hand;
}

int player::get_nof_cards() const noexcept {
    return _hand->get_nof_cards();
}

bool player::test_harvest(int stack, std::string& err){
    err = "Bean Protection Rule: Chosen field has 1 bean while the other has more than one bean. The chosen"
          " field can therefore not be harvested";
    // comparing sizes of stacks
    return !((_bean_fields.at(stack)->get_count() == 1
    && (_bean_fields.at(!stack)->get_count()) > 1));
}


//#ifdef LAMA_SERVER
void player::setup_round() {
    _hand->setup_round();
    _seed_stacks.at(0)->empty_stack();
    _seed_stacks.at(1)->empty_stack();
    _bean_fields.at(0)->setup_stack();
    _bean_fields.at(1)->setup_stack();
}


bool player::add_card(card *card, std::string &err) {
    return _hand->add_card(card, err);
}

bool player::remove_card(std::string card_id, card*& card, std::string &err) {
    card = nullptr;
    return _hand->remove_card(card_id, card, err);
}
// Checks first if the first card is on seed_stack, plants the whole seed stack and then clears seed_stack
bool player::plant_bean(std::string card_id, std::string& err){
#ifdef DEBUG
    std::cout << "player@plant_bean :> accessed on server for player " + this->get_player_name() << std::endl;
#endif

    // Declaring card and try_get_bean gives it its value
    card* first_card;
    std::vector<card*> cards_on_seed_stack;

    // Checking if cards is on either of the seed_stacks

    if (_seed_stacks.at(0)->try_get_bean(card_id, first_card)){
#ifdef DEBUG
        std::cout << "player@plant_bean :> checking seed stack 0" << std::endl;
#endif

        // determine on which field stack the card has to be planted
        int which_field = -1;

        // test on which the card can be planted
        bool can_first_field = _bean_fields.at(0)->can_plant(card_id, first_card);
        bool can_second_field = _bean_fields.at(1)->can_plant(card_id, first_card);
        if (can_first_field && can_second_field){ // The card can be planted on both but maybe
            // one field already has cards of that type
            if (_bean_fields.at(0)->get_count() > 0){
                which_field = 0;
            } else if (_bean_fields.at(1)->get_count() > 0){
                which_field = 1;
            } else which_field = 0; // Make 0 the default case
        }
        else if (can_first_field){
            which_field = 0;
        } else if (can_second_field){
            which_field = 1;
        } else {
            err = "The card could not be planted on either field_stack";
#ifdef DEBUG
            std::cout << "player@plant_bean :> no free bean field available" << std::endl;
#endif
            return false;
        }


        // Get all beans and check if each can be planted
        cards_on_seed_stack = _seed_stacks.at(0)->get_beans();


        for (card* bean : cards_on_seed_stack){

            // If card can't be planted on determined field_stacks, return false
            if (_bean_fields.at(which_field)->can_plant(card_id, first_card)){
                bool temp = _bean_fields.at(which_field)->plant_bean(bean, err);

                // Remove card from _seed_stack
                return _seed_stacks.at(0)->remove_card(bean->get_id(), bean, err);
                //else return true;
            } else{
                err = "One of the cards on the seed_stack could not be planted to the field_stack";
#ifdef DEBUG
                std::cout << "player@plant_bean :> not able to plant bean from seed stack 0" << std::endl;
#endif
                return false;
            }

        }


    } else if (_seed_stacks.at(1)->try_get_bean(card_id, first_card)){
        std::cout << "player@plant_bean :> checking seed stack 1" << std::endl;

        // determine on which field stack the card has to be planted
        int which_field = -1;
        if (_bean_fields.at(0)->can_plant(card_id, first_card)){
            which_field = 0;
        } else if (_bean_fields.at(1)->can_plant(card_id, first_card)){
            which_field = 1;
        } else{
            err = "The card could not be planted on either field_stack";
            return false;
        }


        // Get all beans and check if each can be planted
        cards_on_seed_stack = _seed_stacks.at(1)->get_beans();

        for (card* bean : cards_on_seed_stack){


            // If card can't be planted on determined field_stacks, return false
            if (_bean_fields.at(which_field)->can_plant(card_id, first_card)){
                bool temp = _bean_fields.at(which_field)->plant_bean(bean, err);

                // Remove card from _seed_stack
                return temp && _seed_stacks.at(1)->remove_card(bean->get_id(), bean, err);
            } else{
                err = "One of the cards on the seed_stack could not be planted to the field_stack";
                return false;
            }

        }

    }
    return 0;
}

bool player::play_card(std::string& err){
    card* first;
    // Getting top card in hand
    first = _hand->get_first_card();
    
    // Add top card to one of the seed stacks and remove it from hand
    if(_seed_stacks.at(0)->can_plant(first->get_id(), first)){

        // Checking if everything worked
        if(_seed_stacks.at(0)->add_card(first, err)
            && _hand->remove_card(first->get_id(), first, err)) return 1;
        else err = "Card could not be removed from hand";

    } else if (_seed_stacks.at(1)->can_plant(first->get_id(), first)){

        // Checking if everything worked
        if(_seed_stacks.at(1)->add_card(first, err)
            && _hand->remove_card(first->get_id(), first, err)) return 1;
        else err = "Card could not be removed from hand";

    } else{
        err = "Card could not be added to seed_stack";
    }
    return 0;
}

bool player::take_card(card* card, std::string& err){

    // Checking if bean_type of card is on one of the seed stacks
    if (_seed_stacks.at(0)->can_plant(card->get_id(), card)){

       return _seed_stacks.at(0)->add_card(card, err);

    } else if (_seed_stacks.at(1)->can_plant(card->get_id(), card)){
        
        return _seed_stacks.at(1)->add_card(card, err);

    }
    err = "Card could not be placed on one of the seed stacks";
    return false;
}


// Harvest beans, adds gained coins to player and then resets the stack
bool player::harvest_beans(int stack, std::string &err){
#ifdef DEBUG
    std::cout << "player.cpp@harvest_beans: state update message has been generated" << std::endl;
#endif
    if(test_harvest(stack, err)){
        _coins->set_value(_coins->get_value() + _bean_fields.at(stack)->harvest_beans());
        _bean_fields.at(stack)->setup_stack();
        return 1;
    }
    return 0;
}

//#endif


void player::write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);

    /* id is automatically added */
    // rapidjson::Value id_val(_id.c_str(), allocator);
    // json.AddMember("id", id_val, allocator);

    rapidjson::Value name_val(rapidjson::kObjectType);
    _player_name->write_into_json(name_val, allocator);
    json.AddMember("player_name", name_val, allocator);

    rapidjson::Value coins_val(rapidjson::kObjectType);
    _coins->write_into_json(coins_val, allocator);
    json.AddMember("coins", coins_val, allocator);

    rapidjson::Value hand_val(rapidjson::kObjectType);
    _hand->write_into_json(hand_val, allocator);
    json.AddMember("hand", hand_val, allocator);

    rapidjson::Value left_stack_val(rapidjson::kObjectType);
    _seed_stacks.at(0)->write_into_json(left_stack_val, allocator);
    json.AddMember("left_stack", left_stack_val, allocator);

    rapidjson::Value right_stack_val(rapidjson::kObjectType);
    _seed_stacks.at(1)->write_into_json(right_stack_val, allocator);
    json.AddMember("right_stack", right_stack_val, allocator);

    rapidjson::Value left_field_val(rapidjson::kObjectType);
    _bean_fields.at(0)->write_into_json(left_field_val, allocator);
    json.AddMember("left_field", left_field_val, allocator);

    rapidjson::Value right_field_val(rapidjson::kObjectType);
    _bean_fields.at(1)->write_into_json(right_field_val, allocator);
    json.AddMember("right_field", right_field_val, allocator);


}


player *player::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id")
        && json.HasMember("player_name")
        && json.HasMember("coins")
        && json.HasMember("hand")
        && json.HasMember("left_stack")
        && json.HasMember("right_stack")
        && json.HasMember("left_field")
        && json.HasMember("right_field"))
    {
        return new player(
                json["id"].GetString(),
                serializable_value<std::string>::from_json(json["player_name"].GetObject()),
                serializable_value<int>::from_json(json["coins"].GetObject()),
                hand::from_json(json["hand"].GetObject()),
                {seed_stack::from_json(json["left_stack"].GetObject()),
                seed_stack::from_json(json["right_stack"].GetObject())},
                {field_stack::from_json(json["left_field"].GetObject()),
                field_stack::from_json(json["right_field"].GetObject())});
    } else {
        throw BohnanzaException("Failed to deserialize player from json. Required json entries were missing.");
    }
}
