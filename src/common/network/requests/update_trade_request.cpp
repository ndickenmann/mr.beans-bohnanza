#include "update_trade_request.h"
#include "../../serialization/vector_utils.h"
// Public constructor


update_trade_request::update_trade_request(std::string player_id, trade_state* trade_state)
        : client_request( client_request::create_base_class_properties(RequestType::update_trade, uuid_generator::generate_uuid_v4(), player_id )),
          _trade_state(trade_state)
{ }

// private constructor for deserialization
update_trade_request::update_trade_request(client_request::base_class_properties props , trade_state* trade_state):
        client_request(props),
        _trade_state(trade_state)
{ }

void update_trade_request::write_into_json(rapidjson::Value &json,
                                        rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    client_request::write_into_json(json, allocator);

    rapidjson::Value trade_state_val(rapidjson::kObjectType);
    _trade_state->write_into_json(trade_state_val, allocator);
    json.AddMember("trade_state", trade_state_val, allocator);
}

update_trade_request* update_trade_request::from_json(const rapidjson::Value& json) {
    if (json.HasMember("trade_state")) {
        return new update_trade_request(client_request::extract_base_class_properties(json),
                                        trade_state::from_json(json["trade_state"].GetObject()));
    } else {
        throw BohnanzaException("Could not parse update_trade_request from json. offered_cards, accepted_cards or requested_cards is missing.");
    }
}

