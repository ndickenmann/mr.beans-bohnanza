
#ifndef BOHNANZA_HAND_H
#define BOHNANZA_HAND_H

#include <vector>
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../cards/card.h"

class hand : public unique_serializable {

private:
    std::vector<card*> _cards;


    hand(std::string id);
    hand(std::string id, std::vector<card*> cards);
    card* remove_card(std::vector<card*>::iterator pos);
    card* remove_card(int idx);
    card* remove_card(card* card);

public:
    hand();
    ~hand();


// serializable interface
    static hand* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

// accessors
    int get_nof_cards() const;
    void clear_hand();
    const std::vector<card*> get_cards() const;
    card* get_first_card() const;
    bool try_get_bean(const std::string& card_id, card*& hand_card) const;

//#ifdef LAMA_SERVER
    // state update functions
    void setup_round();
    bool add_card(card* card, std::string& err);
    bool remove_card(std::string card_id, card*& played_card, std::string& err);
//#endif

    std::vector<card*>::iterator get_card_iterator();
};






#endif //BOHNANZA_HAND_H