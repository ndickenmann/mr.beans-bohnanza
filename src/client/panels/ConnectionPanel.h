#ifndef TEST_CONNECTIONPANEL_H
#define TEST_CONNECTIONPANEL_H

#include <wx/wx.h>
#include "../uiElements/InputField.h"


class ConnectionPanel : public wxPanel {

public:
    ConnectionPanel(wxWindow* parent);

    wxString getServerAddress();
    wxString getServerPort();
    wxString getPlayerName();

    float getAspectRatio(int imageWidth, int imageHeight, int windowWidth, int windowHeight, float ratio = 0.6); // ratio Parameter is automatically adjusted to be in [0.1, 1.0]

private:
    InputField* _serverAddressField;
    InputField* _serverPortField;
    InputField* _playerNameField;

    wxSize ourMinSize = wxSize(1000, 720);
};

#endif //TEST_CONNECTIONPANEL_H