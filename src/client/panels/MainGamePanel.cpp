#include "MainGamePanel.h"
#include "../GameController.h"

// constructor
MainGamePanel::MainGamePanel(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(1200, 900)) {}


//whenever a new gamestate is received, this function gets called to build the main game panel according to the new game state
void MainGamePanel::buildGameState(game_state* gameState, player* me) {

    // remove any existing UI
    this->DestroyChildren();

    std::vector<player*> players = gameState->get_players();
    int numberOfPlayers = players.size();

    // find our own player object in the list of players
    int myPosition = -1;
    std::vector<player*>::iterator it = std::find_if(players.begin(), players.end(), [me](const player* x) {
        return x->get_id() == me->get_id();
    });
    if (it < players.end()) {
        me = *it;
        myPosition = it - players.begin();
    } else {
        GameController::showError("Game state error", "Could not find this player among players of server game.");
        return;
    }

    // Main sizer of the main game panel
    mainSizer = new wxBoxSizer( wxHORIZONTAL );

    // show loading screen if the game isn't ready yet
    this->buildLoadingScreen(gameState, me);

    // show our own player
    this->buildLeft(gameState, me);

    // add interaction capability to UI
    this->addLogic(gameState, me);

     // build other players
    this->buildRight(gameState, me);

    // update layout
    this->SetSizer(mainSizer);
    this->Layout();
#ifdef DEBUG
    std::cout << "MainGamePanel@buildGameState: End of buildGameState, current phase: " + std::to_string(gameState->get_phase_id()) << std::endl;
#endif
}

// draws the loading screen before the game starts
void MainGamePanel::buildLoadingScreen(game_state* gameState, player* me){
    if(gameState->is_started()){
        return;
    }
    left = new wxBoxSizer(wxVERTICAL);
    firstLeft = new wxBoxSizer(wxHORIZONTAL);
    secondLeft = new wxBoxSizer(wxHORIZONTAL);
    thirdLeft = new wxBoxSizer(wxHORIZONTAL);

    wxStaticText* playerName = buildStaticText(
            "Your Name:\n" + me->get_player_name(),
            wxDefaultPosition,
            wxDefaultSize,
            wxALIGN_CENTER
    );
    playerName->SetForegroundColour(wxColour(0, 0, 0));

    wxStaticText* waitingText = buildStaticText(
            "Waiting for Game to Start...\nNumber of Waiting Players: " + std::to_string(gameState->get_players().size()),
            wxDefaultPosition,
            wxDefaultSize,
            wxID_ANY
    );
    waitingText->SetForegroundColour(wxColour(0, 0, 0));

    startGameButton = new wxButton(
            this,
            wxID_ANY,
            wxT("Start Game"),
            wxDefaultPosition,
            wxDefaultSize,
            0
    );
    startGameButton->Bind(
            wxEVT_BUTTON,
            [](wxCommandEvent& event) {
                GameController::startGame();
            }
    );

    firstLeft->Add(playerName, 1, wxALIGN_TOP, normalBorder);
    secondLeft->Add( waitingText, 1, wxALIGN_CENTER, normalBorder );
    thirdLeft->Add( startGameButton, 1, wxALIGN_CENTER, normalBorder );

    left->Add( firstLeft, 1, wxALIGN_CENTER, normalBorder );
    left->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    left->Add( secondLeft, 1, wxALIGN_CENTER, normalBorder );
    left->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    left->Add( thirdLeft, 1, wxALIGN_CENTER, normalBorder );
    mainSizer->Add( left, 1, wxALIGN_CENTER, normalBorder );
}

// draws the left side of the main game panel, which corresponds to the client's in game player
// the left side is structured in rows and gets drawn from top to bottom
void MainGamePanel::buildLeft(game_state* gameState, player* me){
    if(!gameState->is_started()){
        return;
    }
    
    left = new wxBoxSizer(wxVERTICAL);

    this->buildFirstRow(gameState, me);

    this->buildSecondRow(gameState, me);

    this->buildThirdRow(gameState, me);
    
    this->buildFourthRow(gameState, me);
    
    this->buildFifthRow(gameState, me);

    left->Add(firstLeft, 1, wxALIGN_CENTER, normalBorder);
    left->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    left->Add(secondLeft, 1, wxALIGN_CENTER, normalBorder);
    left->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    left->Add(thirdLeft, 1, wxEXPAND, normalBorder);
    left->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    left->Add(fourthLeft, 1, wxALIGN_CENTER, normalBorder);
    left->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    left->Add(fifthLeft, 1, wxALIGN_CENTER, normalBorder);
    mainSizer->Add(left, 2, wxALIGN_CENTER, normalBorder);
}

// this draws the first row of the left side of the main game panel
void MainGamePanel::buildFirstRow(game_state *gameState, player *me) {
    firstLeft = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText* playerName = buildStaticText(
            "Your name: " + me->get_player_name(),
            wxDefaultPosition,
            wxDefaultSize,
            wxALIGN_CENTER
    );
    playerName->SetForegroundColour(wxColour(0, 0, 0));
    firstLeft->Add( playerName, 0, wxALIGN_TOP, normalBorder );
}

// this draws the second row of the left side of the main game panel
void MainGamePanel::buildSecondRow(game_state *gameState, player *me)   {
    secondLeft = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* drawPileSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticText* drawCardAmount = buildStaticText(
             std::to_string(gameState->get_draw_pile()->get_nof_cards()) + " Cards left",
             wxDefaultPosition,
             wxDefaultSize,
             wxID_ANY
    );
    drawCardAmount->SetForegroundColour(wxColour(0, 0, 0));
    drawPile = new ImagePanel(this,
                              "assets/bohn_back.png",
                              wxBITMAP_TYPE_ANY,
                              wxDefaultPosition,
                              cardSize
    );
    drawPileSizer->Add(drawPile, 0, wxALIGN_CENTER, normalBorder );
    drawPileSizer->Add(drawCardAmount, 0, wxALIGN_CENTER| wxTOP, normalBorder );

    std::string err;
    auto middlePile = gameState->get_middle_pile();
    std::string firstCardFile;
    std::string secondCardFile;
#ifdef DEBUG
    std::cout << "MainGamePanel@buildSecondRow: Number of cards in middle pile: " << middlePile->get_nof_cards() << std::endl;
#endif
    if(middlePile->get_nof_cards() == 0){
        firstCardFile = "assets/background.png";
        secondCardFile = "assets/background.png";
    } else if(middlePile->get_nof_cards() == 1) {
        firstCardFile = "assets/bohn_" + std::to_string(middlePile->get_card(0, err)->get_bean_type()) + ".png";
        secondCardFile = "assets/background.png";
    }
    else{
        firstCardFile = "assets/bohn_" + std::to_string(middlePile->get_card(0, err)->get_bean_type()) + ".png";
        secondCardFile = "assets/bohn_" + std::to_string(middlePile->get_card(1, err)->get_bean_type()) + ".png";
    }
    firstFaceCard = new ImagePanel(this,
           firstCardFile,
           wxBITMAP_TYPE_ANY,
           wxDefaultPosition,
           cardSize
    );
    secondFaceCard = new ImagePanel(this,
            secondCardFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            cardSize
    );

    tradeButton = new wxButton(
            this,
            wxID_ANY,
            wxT("Trade"),
            wxDefaultPosition,
            wxDefaultSize,
            0
    );
    tradeButton->SetBackgroundColour("#b9ca88");

    secondLeft->Add( drawPileSizer, 0, wxALIGN_CENTER, normalBorder );
    secondLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    secondLeft->Add( secondFaceCard, 0, wxALIGN_CENTER, normalBorder );
    secondLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    secondLeft->Add( firstFaceCard, 0, wxALIGN_CENTER, normalBorder );
    secondLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    secondLeft->Add( tradeButton, 0, wxALIGN_CENTER|wxRESERVE_SPACE_EVEN_IF_HIDDEN, normalBorder );
}

// this draws the third row of the left side of the main game panel
void MainGamePanel::buildThirdRow(game_state *gameState, player *me) {
    thirdLeft = new wxBoxSizer(wxHORIZONTAL);
    plantButton = new wxButton(
            this,
            wxID_ANY,
            wxT("Plant"),
            wxDefaultPosition,
            wxDefaultSize,
            0
    );
    plantButton->SetBackgroundColour("#b9ca88");

    auto firstSeedType = me->get_seed_stacks()[0]->get_stack_bean_type();
    auto firstSeedCount = me->get_seed_stacks()[0]->get_nof_beans();
    std::string firstSeedFile = "assets/bohn_" + std::to_string(firstSeedType) + ".png";
    if (firstSeedCount == 0) {
        firstSeedFile = "assets/seedstack.png";
    }
    wxBoxSizer* firstSeedStackSizer = new wxBoxSizer(wxVERTICAL);
    firstSeedStack = new ImagePanel(
            this,
            firstSeedFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            reducedCardSize
    );
    wxStaticText* firstSeedStackAmount = buildStaticText(
             std::to_string(me->get_seed_stacks()[0]->get_nof_beans()) + " Seeds",
            wxDefaultPosition,
            wxDefaultSize,
            wxID_ANY
    );
    firstSeedStackAmount->SetForegroundColour(wxColour(0, 0, 0));
    firstSeedStackSizer->Add(firstSeedStack, 0, wxALIGN_CENTER, normalBorder );
    firstSeedStackSizer->Add(firstSeedStackAmount, 0, wxALIGN_CENTER| wxTOP, normalBorder );

    auto secondSeedType = me->get_seed_stacks()[1]->get_stack_bean_type();
    auto secondSeedCount = me->get_seed_stacks()[1]->get_nof_beans();
    std::string secondSeedFile = "assets/bohn_" + std::to_string(secondSeedType) + ".png";
    if (secondSeedCount == 0) {
        secondSeedFile = "assets/seedstack.png";
    }
    wxBoxSizer* secondSeedStackSizer = new wxBoxSizer(wxVERTICAL);
    secondSeedStack = new ImagePanel(
            this,
            secondSeedFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            reducedCardSize
    );
    wxStaticText* secondSeedStackAmount = buildStaticText(
             std::to_string(me->get_seed_stacks()[1]->get_nof_beans()) + " Seeds",
            wxDefaultPosition,
            wxDefaultSize,
            wxID_ANY
    );
    secondSeedStackAmount->SetForegroundColour(wxColour(0, 0, 0));
    secondSeedStackSizer->Add(secondSeedStack, 0, wxALIGN_CENTER, normalBorder );
    secondSeedStackSizer->Add(secondSeedStackAmount, 0, wxALIGN_CENTER| wxTOP, normalBorder );

    auto firstBeanType = me->get_bean_fields()[0]->get_stack_bean_type();
    auto firstBeanCount = me->get_bean_fields()[0]->get_count();
    std::string firstBeanFile = "assets/bohn_" + std::to_string(firstBeanType) + ".png";
    if (firstBeanCount == 0) {
        firstBeanFile = "assets/beanfield.png";
    }
    wxPanel* beanfield1 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    beanfield1->SetBackgroundColour(wxColor("#a98467"));

    wxBoxSizer* firstBeanFieldSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticText* firstBeanFieldAmount = new wxStaticText(beanfield1, wxID_ANY,  std::to_string(firstBeanCount) + " Beans", wxDefaultPosition, wxDefaultSize);

    firstBeanFieldAmount->SetForegroundColour(wxColour(0, 0, 0));
    firstBeanField = new ImagePanel(
            beanfield1,
            firstBeanFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            cardSize
    );
    firstBeanFieldSizer->Add(firstBeanField, 0, wxALIGN_CENTER|wxTOP|wxRIGHT|wxLEFT, normalBorder );
    firstBeanFieldSizer->Add(firstBeanFieldAmount, 0, wxALIGN_CENTER| wxALL, normalBorder );
    beanfield1-> SetSizerAndFit( firstBeanFieldSizer );

    auto secondBeanType = me->get_bean_fields()[1]->get_stack_bean_type();
    auto secondBeanCount = me->get_bean_fields()[1]->get_count();
    std::string secondBeanFile = "assets/bohn_" + std::to_string(secondBeanType) + ".png";
    if (secondBeanCount == 0) {
        secondBeanFile = "assets/beanfield.png";
    }

    wxPanel* beanfield2 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    beanfield2->SetBackgroundColour(wxColor("#a98467"));

    wxBoxSizer* secondBeanFieldSizer = new wxBoxSizer(wxVERTICAL);
    wxStaticText* secondBeanFieldAmount = new wxStaticText(beanfield2, wxID_ANY,  std::to_string(secondBeanCount) + " Beans", wxDefaultPosition, wxDefaultSize);

    secondBeanFieldAmount->SetForegroundColour(wxColour(0, 0, 0));
    secondBeanField = new ImagePanel(
            beanfield2,
            secondBeanFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            cardSize
    );
    secondBeanFieldSizer->Add(secondBeanField, 0, wxALIGN_CENTER|wxTOP|wxRIGHT|wxLEFT, normalBorder );
    secondBeanFieldSizer->Add(secondBeanFieldAmount, 0, wxALIGN_CENTER| wxALL, normalBorder );
    beanfield2-> SetSizerAndFit( secondBeanFieldSizer );

    wxBoxSizer* coinSellSizer = new wxBoxSizer(wxVERTICAL);

    std::string cardFile = "assets/bohn_cash.png";
    ImagePanel* cashicon = new ImagePanel(
            this,
            cardFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            wxSize(60,60)
    );
    wxStaticText* coinAmount = buildStaticText(
             std::to_string(me->get_score()) + " Coins",
            wxDefaultPosition,
            wxDefaultSize,
            wxID_ANY
    );
    coinAmount->SetForegroundColour(wxColour(0, 0, 0));
    harvestButton = new wxButton(
            this,
            wxID_ANY,
            wxT("Harvest"),
            wxDefaultPosition,
            wxDefaultSize,
            0
    );
    harvestButton->SetBackgroundColour("#b9ca88");

    coinSellSizer->Add( cashicon, 0, wxALIGN_CENTER|wxALL, normalBorder );
    coinSellSizer->Add(coinAmount, 0, wxALIGN_CENTER, normalBorder );
    coinSellSizer->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    coinSellSizer->Add( harvestButton, 0, wxALIGN_CENTER|wxRESERVE_SPACE_EVEN_IF_HIDDEN, normalBorder );

    thirdLeft->Add( plantButton, 0, wxALIGN_CENTER|wxRESERVE_SPACE_EVEN_IF_HIDDEN, normalBorder );
    thirdLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    thirdLeft->Add( firstSeedStackSizer, 0, wxALIGN_CENTER, normalBorder );
    thirdLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    thirdLeft->Add( secondSeedStackSizer, 0, wxALIGN_CENTER, normalBorder );
    thirdLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    thirdLeft->Add( beanfield1, 0, wxALIGN_CENTER, normalBorder );
    thirdLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    thirdLeft->Add( beanfield2, 0, wxALIGN_CENTER, normalBorder );
    thirdLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    thirdLeft->Add( coinSellSizer, 0, wxALIGN_CENTER, normalBorder );
}

// this draws the fourth row of the left side of the main game panel
void MainGamePanel::buildFourthRow(game_state *gameState, player *me) {
    fourthLeft = new wxBoxSizer(wxHORIZONTAL);
    fourthLeft->Add(0,0,1, wxEXPAND, 0);

    int numberOfCards = me->get_hand()->get_nof_cards();
    wxSize scaledCardSize = MainGamePanel::cardSize;
    if (numberOfCards * (MainGamePanel::cardSize.GetWidth() + 10) >
        this->GetSize().GetWidth()*2/3-8) { // 8 -> 4 pixel padding on both sides
        int scaledCardWidth = ((this->GetSize().GetWidth()*2/3-8) / numberOfCards) - 10;
        double cardAspectRatio =
                (double) MainGamePanel::cardSize.GetHeight() / (double) MainGamePanel::cardSize.GetWidth();
        int scaledCardHeight = (int) ((double) scaledCardWidth * cardAspectRatio);
        scaledCardSize = wxSize(scaledCardWidth, scaledCardHeight);
    }

    for(auto card : me->get_hand()->get_cards()){
        std::string cardFile = "assets/bohn_" + std::to_string(card->get_bean_type()) + ".png";
        ImagePanel* handCard = new ImagePanel(
                this,
                cardFile,
                wxBITMAP_TYPE_ANY,
                wxDefaultPosition,
                scaledCardSize
        );
        fourthLeft->Add( handCard, 0, wxALIGN_CENTER|wxALL, normalBorder );
        //fourthLeft->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    }
    fourthLeft->Add(0,0,1, wxEXPAND, 0);
}

// this draws the fifth row of the left side of the main game panel
void MainGamePanel::buildFifthRow(game_state *gameState, player *me) {
    fifthLeft = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText* activePlayerText = buildStaticText(
            "The Active Player is: " + gameState->get_active_player()->get_player_name(),
            wxDefaultPosition,
            wxDefaultSize,
            wxALIGN_CENTER
    );
    activePlayerText->SetForegroundColour(wxColour(0, 0, 0));
    fifthLeft->Add( activePlayerText, 0, wxALIGN_CENTER, normalBorder );
}

// add logic and interactivity to the left side of the main game panel
void MainGamePanel::addLogic(game_state *gameState, player *me) {
    if(!gameState->is_started() || gameState->is_finished()){
        return;
    }

    // Logic for the trade button
    auto myId = me->get_id();
    auto activeId = gameState->get_active_player()->get_id();
    auto phaseId = gameState->get_phase_id();
    tradeButton->Bind(
        wxEVT_BUTTON,
        [&, myId, activeId, phaseId](wxCommandEvent& event) {
            if(myId == activeId && phaseId == 1 && !GameController::getStartedTrading()) {
                GameController::initTrade();
            }
            else if(phaseId == 1 && GameController::getStartedTrading()){
                GameController::openTradingPanel();
            }
            if(lastClicked != nullptr){
                lastClicked->updateInteraction(-1);
            }
            lastClicked = nullptr;
        }
    );
    if(gameState->get_phase_id() != 1){
        tradeButton->Hide();
    }

    // Logic for harvestButton
    auto firstFieldToBeHarvested = me->get_bean_fields()[0];
    auto secondFieldToBeHarvested = me->get_bean_fields()[1];
    harvestButton->Bind(
        wxEVT_BUTTON,
        [&, firstFieldToBeHarvested, secondFieldToBeHarvested](wxCommandEvent& event) {
            if(lastClicked == firstBeanField){
                GameController::harvestField("0");
            }
            else if(lastClicked == secondBeanField){
                GameController::harvestField("1");
            }
            if(lastClicked != nullptr){
                lastClicked->updateInteraction(-1);
            }
            lastClicked = nullptr;
        }
    );
    if(gameState->get_phase_id() == 1 || (me->get_bean_fields()[0]->get_count()== 0 && me->get_bean_fields()[1]->get_count()== 0)){
        harvestButton->Hide();

    }

    // Logic for the plant button
    int firstSeedCount = me->get_seed_stacks()[0]->get_nof_beans();
    int secondSeedCount = me->get_seed_stacks()[1]->get_nof_beans();
    std::string firstSeedId;
    std::string secondSeedId;
    if(firstSeedCount > 0){firstSeedId = me->get_seed_stacks()[0]->get_beans()[0]->get_id();}
    if(secondSeedCount > 0){secondSeedId = me->get_seed_stacks()[1]->get_beans()[0]->get_id();}
    plantButton->Bind(
        wxEVT_BUTTON,
        [&, firstSeedCount, firstSeedId, secondSeedCount, secondSeedId](wxCommandEvent& event) {
            if(firstSeedCount > 0 && lastClicked == firstSeedStack) {
                GameController::plantCard(firstSeedId);
            }
            else if(secondSeedCount > 0 && lastClicked == secondSeedStack){
                GameController::plantCard(secondSeedId);
            }
            if(lastClicked != nullptr){
                lastClicked->updateInteraction(-1);
            }
            lastClicked = nullptr;
        }
    );
    if(gameState->get_phase_id() == 1 || (me->get_seed_stacks()[0]->get_nof_beans()== 0 && me->get_seed_stacks()[1]->get_nof_beans()== 0)){
        plantButton->Hide();
    }

    //firstBeanField logic
    auto firstBeanFieldCount = me->get_bean_fields()[0]->get_count();
    firstBeanField->setInteractionMode(firstBeanFieldCount > 0);
    firstBeanField->SetCursor(wxCursor(wxCURSOR_HAND));
    firstBeanField->Bind(wxEVT_ENTER_WINDOW,[&, firstBeanFieldCount](wxMouseEvent& event){ if(firstBeanFieldCount > 0){firstBeanField->updateInteraction(1);}});
    firstBeanField->Bind(wxEVT_LEAVE_WINDOW,[&](wxMouseEvent& event){ firstBeanField->updateInteraction(0);});
    firstBeanField->Bind(
        wxEVT_LEFT_UP,
        [&](wxMouseEvent& event){
            if (lastClicked == nullptr) {
                firstBeanField->updateInteraction(2);
                lastClicked = firstBeanField;
            } else if (lastClicked == firstBeanField) {
                firstBeanField->updateInteraction(2);
                lastClicked = nullptr;
            } else {
                lastClicked->updateInteraction(-1);
                firstBeanField->updateInteraction(2);
                lastClicked = firstBeanField;
            }
            if(!firstBeanField->getInteractionMode()){
                lastClicked = nullptr;
            }
        }
    );

    // secondBeanField logic
    auto secondBeanFieldCount = me->get_bean_fields()[1]->get_count();
    secondBeanField->setInteractionMode(secondBeanFieldCount > 0);
    secondBeanField->SetCursor(wxCursor(wxCURSOR_HAND));
    secondBeanField->Bind(wxEVT_ENTER_WINDOW,[&, secondBeanFieldCount](wxMouseEvent& event){ if(secondBeanFieldCount > 0){secondBeanField->updateInteraction(1);}});
    secondBeanField->Bind(wxEVT_LEAVE_WINDOW,[&](wxMouseEvent& event){ secondBeanField->updateInteraction(0);});
    secondBeanField->Bind(
        wxEVT_LEFT_UP,
        [&](wxMouseEvent& event){
            if (lastClicked == nullptr) {
                secondBeanField->updateInteraction(2);
                lastClicked = secondBeanField;
            } else if (lastClicked == secondBeanField) {
                secondBeanField->updateInteraction(2);
                lastClicked = nullptr;
            } else {
                lastClicked->updateInteraction(-1);
                secondBeanField->updateInteraction(2);
                lastClicked = secondBeanField;
            }
            if(!secondBeanField->getInteractionMode()){
                lastClicked = nullptr;
            }
        }
    );

    // firstSeedStack logic
    auto firstSeedStackCount = me->get_seed_stacks()[0]->get_nof_beans();
    firstSeedStack->setInteractionMode(firstSeedStackCount > 0);
    firstSeedStack->SetCursor(wxCursor(wxCURSOR_HAND));
    firstSeedStack->Bind(wxEVT_ENTER_WINDOW,[&, firstSeedStackCount](wxMouseEvent& event){ if(firstSeedStackCount > 0){firstSeedStack->updateInteraction(1);}});
    firstSeedStack->Bind(wxEVT_LEAVE_WINDOW,[&](wxMouseEvent& event){ firstSeedStack->updateInteraction(0);});
    firstSeedStack->Bind(
        wxEVT_LEFT_UP,
        [&](wxMouseEvent& event){
            if (lastClicked == nullptr) {
                firstSeedStack->updateInteraction(2);
                lastClicked = firstSeedStack;
            } else if (lastClicked == firstSeedStack) {
                firstSeedStack->updateInteraction(2);
                lastClicked = nullptr;
            } else {
                lastClicked->updateInteraction(-1);
                firstSeedStack->updateInteraction(2);
                lastClicked = firstSeedStack;
            }
            if(!firstSeedStack->getInteractionMode()){
                lastClicked = nullptr;
            }
        }
    );

    // secondSeedStack logic
    auto secondSeedStackCount = me->get_seed_stacks()[1]->get_nof_beans();
    secondSeedStack->setInteractionMode(secondSeedStackCount > 0);
    secondSeedStack->SetCursor(wxCursor(wxCURSOR_HAND));
    secondSeedStack->Bind(wxEVT_ENTER_WINDOW,[&, secondSeedStackCount](wxMouseEvent& event){ if(secondSeedStackCount > 0){secondSeedStack->updateInteraction(1);}});
    secondSeedStack->Bind(wxEVT_LEAVE_WINDOW,[&](wxMouseEvent& event){ secondSeedStack->updateInteraction(0);});
    secondSeedStack->Bind(
        wxEVT_LEFT_UP,
        [&](wxMouseEvent& event){
            if (lastClicked == nullptr) {
                secondSeedStack->updateInteraction(2);
                lastClicked = secondSeedStack;
            } else if (lastClicked == secondSeedStack) {
                secondSeedStack->updateInteraction(2);
                lastClicked = nullptr;
            } else {
                lastClicked->updateInteraction(-1);
                secondSeedStack->updateInteraction(2);
                lastClicked = secondSeedStack;
            }
            if(!secondSeedStack->getInteractionMode()){
                lastClicked = nullptr;
            }
        }
    );
}

// build the right side of the main game panel which corresponds to the other players
void MainGamePanel::buildRight(game_state *gameState, player *me) {
    if(!gameState->is_started()){
        return;
    }
    wxPanel* viualpartition = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(3,-1));
    viualpartition->SetBackgroundColour(wxColor("#adc178"));
    mainSizer->Add(viualpartition, 0, wxEXPAND|wxLEFT, normalBorder);

    right = new wxBoxSizer(wxVERTICAL);

    for(auto player : gameState->get_players()){
        if(me->get_id() != player->get_id()) {
            this->buildOtherPlayer(gameState, me, player);
        }
    }
    //right->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    //mainSizer->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder*3));
    mainSizer->Add(right, 1, wxALIGN_CENTER, normalBorder);
}

// build a specific other player of the game
void MainGamePanel::buildOtherPlayer(game_state *gameState,player *me, player *player) {
    wxStaticText* otherPlayerName = buildStaticText(
            "Player: " + player->get_player_name(),
            wxDefaultPosition,
            wxDefaultSize,
            wxALIGN_CENTER
    );
    otherPlayerName->SetForegroundColour(wxColour(0, 0, 0));
    right->Add(otherPlayerName, 0, wxALIGN_CENTER|wxALL, normalBorder );


    wxBoxSizer* playerBeanFieldSizer = new wxBoxSizer(wxHORIZONTAL);
    for(int i = 0; i < player->get_bean_fields().size(); i++){

        wxPanel* beanfield1 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
        beanfield1->SetBackgroundColour(wxColor("#a98467"));

        wxBoxSizer* vertSizer = new wxBoxSizer(wxVERTICAL);
        auto beanType = player->get_bean_fields()[i]->get_stack_bean_type();
        auto beanCount = player->get_bean_fields()[i]->get_count();
        std::string cardFile = "assets/bohn_" + std::to_string(beanType) + ".png";
        if(beanCount == 0){
            cardFile = "assets/beanfield.png";
        }
        ImagePanel* beanField = new ImagePanel(
                beanfield1,
                cardFile,
                wxBITMAP_TYPE_ANY,
                wxDefaultPosition,
                reducedCardSize
        );
        wxStaticText* beanFieldAmount = new wxStaticText(beanfield1, wxID_ANY,  std::to_string(beanCount) + " Beans", wxDefaultPosition, wxDefaultSize);

        beanFieldAmount->SetForegroundColour(wxColour(0, 0, 0));
        vertSizer->Add(beanField, 0, wxALIGN_CENTER|wxTOP|wxLEFT|wxRIGHT, normalBorder );
        vertSizer->Add(beanFieldAmount, 0, wxALIGN_CENTER| wxALL, normalBorder );
        beanfield1-> SetSizerAndFit( vertSizer );
        playerBeanFieldSizer->Add(beanfield1, 0, wxALIGN_CENTER|wxLEFT, normalBorder);
        playerBeanFieldSizer->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    }

    wxBoxSizer* coinSizer = new wxBoxSizer(wxVERTICAL);
    std::string cardFile = "assets/bohn_cash.png";
    ImagePanel* cashicon = new ImagePanel(
            this,
            cardFile,
            wxBITMAP_TYPE_ANY,
            wxDefaultPosition,
            wxSize(40,40)
    );
    wxStaticText* playerCoinAmount = buildStaticText(
            std::to_string(player->get_score()) + " Coins",
            wxDefaultPosition,
            wxDefaultSize,
            wxID_ANY
    );
    coinSizer->Add( cashicon, 0, wxALIGN_CENTER|wxALL, normalBorder );
    playerCoinAmount->SetForegroundColour(wxColour(0, 0, 0));
    coinSizer->Add(playerCoinAmount, 0, wxALIGN_CENTER, normalBorder );
    playerBeanFieldSizer->Add(coinSizer,1, wxALIGN_CENTER, normalBorder);
    right->Add(playerBeanFieldSizer, 0, wxALIGN_CENTER, normalBorder);
    //right->Add(CreateSpacer(this), wxSizerFlags().Border(wxALL, normalBorder));
    if(player != gameState->get_players()[gameState->get_players().size()-1] && (me!=gameState->get_players()[gameState->get_players().size()-1] || player != gameState->get_players()[gameState->get_players().size()-2])) {
        wxPanel *viualpartition = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 3));
        viualpartition->SetBackgroundColour(wxColor("#adc178"));
        right->Add(viualpartition, 0, wxEXPAND | wxTOP, normalBorder);
    }
}

// helper function to display text
wxStaticText* MainGamePanel::buildStaticText(std::string content, wxPoint position, wxSize size, long textAlignment){
    wxStaticText* staticText = new wxStaticText(this, wxID_ANY, content, position, size, textAlignment);
    return staticText;
}

// helper function to create spacers
wxPanel* MainGamePanel::CreateSpacer(wxWindow* parent, int width, int height){
    wxPanel* spacer = new wxPanel(parent);
    spacer->SetMinSize(wxSize(width, height));
    return spacer;
}