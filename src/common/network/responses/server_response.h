// Base class for all messages sent from the server to the client.
// It offers a function to deserialize a server_response subclass from a valid json.

#ifndef BOHN_SERVER_RESPONSE_H
#define BOHN_SERVER_RESPONSE_H

#include <string>
#include <unordered_map>
#include "../../exceptions/BohnanzaException.h"
#include "../../exceptions/BohnanzaException.h"

#include "../../serialization/serializable.h"

// Identifier for the different response types.
// The ResponseType is sent with every server_response to identify the type of server_response
// during deserialization on the client side.
enum ResponseType {
    req_response,
    trade_update_response,
    trade_update_broadcast,
    state_diff_msg,
    full_state_msg
};

class server_response : public serializable {
private:

    // for deserialization
    static const std::unordered_map<std::string, ResponseType> _string_to_response_type;
    // for serialization
    static const std::unordered_map<ResponseType, std::string> _response_type_to_string;

protected:
    ResponseType _type;

    struct base_class_properties {
        ResponseType type;
    };

    explicit server_response(base_class_properties); // base constructor
    static base_class_properties create_base_class_properties(ResponseType type);
    static base_class_properties extract_base_class_properties(const rapidjson::Value& json);

public:
    ResponseType get_type() const;

    // Tries to create the specific server_response from the provided json.
    // Throws exception if parsing fails -> Use only inside "try{ }catch()" block
    static server_response* from_json(const rapidjson::Value& json);

    // Serializes the server_response into a json object that can be sent over the network
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

#define BOHN_CLIENT
#ifdef BOHN_CLIENT
    virtual void Process() const = 0;
#endif
};


#endif //BOHN_SERVER_RESPONSE_H
