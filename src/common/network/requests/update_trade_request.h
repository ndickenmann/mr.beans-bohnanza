#ifndef BOHN_UPDATE_TRADE_REQUEST_H
#define BOHN_UPDATE_TRADE_REQUEST_H

#include <vector>
#include <string>
#include "client_request.h"
#include "../../trade_state.h"
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../../serialization/vector_utils.h"
#include "../../serialization/unique_serializable.h"

class update_trade_request : public client_request{

private:
    trade_state* _trade_state;

    /*
     * Private constructor for deserialization
     */
    update_trade_request(base_class_properties, trade_state* trade_state);

public:

    [[nodiscard]] trade_state*  get_trade_state() const { return this->_trade_state; }

    /*
     * Constructor to update trade 
     */
    update_trade_request(std::string player_id, trade_state* trade_state);

    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static update_trade_request* from_json(const rapidjson::Value& json);
};


#endif //BOHN_UPDATE_TRADE_REQUEST_H
