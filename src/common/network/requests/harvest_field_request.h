#ifndef BOHN_HARVEST_FIELD_REQUEST_H
#define BOHN_HARVEST_FIELD_REQUEST_H


#include <string>
#include "client_request.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class harvest_field_request : public client_request{

private:
     std::string _field_id;

    /*
     * Private constructor for deserialization
     */
    harvest_field_request(base_class_properties, std::string field_id);

public:

    [[nodiscard]] std::string get_field_id() const { return this->_field_id; }

    /*
     * Constructor to harvest a field 
     */
    harvest_field_request(std::string player_id, std::string field_id);

    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static harvest_field_request* from_json(const rapidjson::Value& json);
};


#endif //BOHN_HARVEST_FIELD_REQUEST_H
