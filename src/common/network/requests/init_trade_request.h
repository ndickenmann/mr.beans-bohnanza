#ifndef BOHN_INIT_TRADE_REQUEST_H
#define BOHN_INIT_TRADE_REQUEST_H


#include <string>
#include "../../serialization/unique_serializable.h"
#include "../../serialization/vector_utils.h"
#include "client_request.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class init_trade_request : public client_request{

private:
    

    /*
     * Private constructor for deserialization
     */
    init_trade_request(base_class_properties);

public:


    /*
     * Constructor to init trade 
     */
    init_trade_request(std::string player_id);

    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static init_trade_request* from_json(const rapidjson::Value& json);
};


#endif //BOHN_INIT_TRADE_REQUEST_H
