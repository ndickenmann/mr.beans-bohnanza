

#ifndef BOHNANZA_MIDDLE_PILE_H
#define BOHNANZA_MIDDLE_PILE_H

#include "card.h"
#include <vector>
#include <string>
#include <algorithm>
#include "../../serialization/serializable.h"
#include "../../serialization/unique_serializable.h"
#include "../../serialization/serializable_value.h"
#include "../../game_state/player/player.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class middle_pile : public unique_serializable {
private:
    std::vector<card*> _cards;

    /*
     * Deserialization constructor
     */
    middle_pile(std::string id, std::vector<card*>& cards);

    // from_diff constructor
    middle_pile(std::string id);

public:
// constructors
    middle_pile();
    middle_pile(std::vector<card*>& cards);
    ~middle_pile();


// accessors
    bool is_empty() const noexcept;
    // should always be 2 or 0
    int get_nof_cards() const noexcept;

//#ifdef BOHNANZA_SERVER
    // state update functions
    void reset_middle();
    bool add_two_cards(card*& first_card, card*& second_card, std::string& err);

    // returns either the left (int value 0) or right card (int value 1)
    card* get_card(int card, std::string& err);
    card* remove_card(int remove_card, std::string& err);
//#endif

// serialization
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static middle_pile* from_json(const rapidjson::Value& json);
};

#endif //BOHNANZA_middle_pile_H