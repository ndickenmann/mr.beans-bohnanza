#ifndef BOHN_EXCEPTION_H
#define BOHN_EXCEPTION_H

#include <string>

class BohnanzaException : public std::exception {
private:
    std::string _msg;
public:
    explicit BohnanzaException(const std::string& message) : _msg(message) { };

    const char* what() const noexcept override {
        return _msg.c_str();
    }
};

#endif //BOHN_EXCEPTION_H
