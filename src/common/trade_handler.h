//
// Created by widizzi on 16/05/23.
//

#ifndef BOHNANZA_TRADE_HANDLER_H
#define BOHNANZA_TRADE_HANDLER_H

#include <vector>
#include <map>
#include <utility>
#include "game_state/game_state.h"
#include "game_state/player/player.h"
#include "trade_state.h"

class trade_handler : public unique_serializable {
private:
    std::vector<player*> _trade_partners;
    std::map<player*, std::pair<bool, bool>> acceptor;

    trade_state* _trade_state;

public:
    trade_handler();

    bool start_trade(game_state* _game_state);
    bool finish_trade();
    bool update_trade(trade_state* trade_state);
    bool check_accept(trade_state* trade_state);
    bool execute_trade(trade_state* trade_state, player* player_one, player* player_two);
    trade_state* get_trade_state();

};

#endif //BOHNANZA_TRADE_HANDLER_H
