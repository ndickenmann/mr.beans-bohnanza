
#ifndef BOHNANZA_SEED_STACK_H
#define BOHNANZA_SEED_STACK_H

#include <vector>
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../cards/card.h"


class seed_stack : public unique_serializable {

private:
    std::vector<card*> _traded_beans;

    seed_stack(std::string id);
    seed_stack(std::string id, std::vector<card*> traded_beans);
    card* remove_card(card* card);
public:
    seed_stack();
    ~seed_stack();

// serializable interface
    static seed_stack* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

// accessors
    int get_stack_bean_type() const;
    int get_nof_beans() const;
    const std::vector<card*> get_beans() const;

    bool try_get_bean(const std::string& card_id, card*& seed_stack_bean) const;
    bool can_plant(const std::string& card_id, card*& field_stack_card) const;

//#ifdef BOHNANZA_SERVER
    // state update functions
    void empty_stack();
    bool add_card(card* card, std::string& err);
    bool remove_card(std::string card_id, card*& played_card, std::string& err);
//#endif

    std::vector<card*>::iterator get_card_iterator();
};


#endif // BOHNANZA_SEED_STACK_H