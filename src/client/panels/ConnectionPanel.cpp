#include "ConnectionPanel.h"


#include "../uiElements/ImagePanel.h"
#include "../../common/network/default.conf"
#include "../GameController.h"


ConnectionPanel::ConnectionPanel(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(1200, 900)) {

    wxColor color = wxColor("#f0ead2");
    this->SetBackgroundColour(color);

    wxBoxSizer* verticalLayout = new wxBoxSizer(wxVERTICAL);

    wxSize windowSize = parent->GetSize();
    int logoWidth = 1767; // Real pixel width of Bohnanza logo
    int logoHeight = 2484; // Real pixel height of Bohnanza logo
    float ratio = getAspectRatio(logoWidth, logoHeight, windowSize.GetWidth(), windowSize.GetHeight());
    logoWidth *= 1.0*ratio;
    logoHeight *= 1.0*ratio;

    ImagePanel* logo = new ImagePanel(this, "assets/bohnanza_logo.png", wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(logoWidth, logoHeight));
    verticalLayout->Add(logo, 0, wxALIGN_CENTER | wxTOP | wxLEFT | wxRIGHT, 10);
    logo->SetToolTip("Bohnanza game manual");
    logo->SetCursor(wxCursor(wxCURSOR_HAND));
    logo->Bind(wxEVT_LEFT_UP, [](wxMouseEvent &event) {
        //logo->updateInteraction(2);
        wxLaunchDefaultBrowser("https://www.riograndegames.com/wp-content/uploads/2013/02/Bohnanza-Rules.pdf");

    });

    this->_serverAddressField = new InputField(
        this, // parent element
        "Server address:", // label
        100, // width of label
        default_server_host, // default value (variable from "default.conf")
        240 // width of field
    );
    this->_serverAddressField->SetForegroundColour(wxColour(0, 0, 0));
    verticalLayout->Add(this->_serverAddressField, 0, wxTOP | wxLEFT | wxRIGHT, 10);

    this->_serverPortField = new InputField(
        this, // parent element
        "Server port:", // label
        100, // width of label
        wxString::Format("%i", default_port), // default value (variable from "default.conf")
        240 // width of field
    );
    this->_serverPortField->SetForegroundColour(wxColour(0, 0, 0));
    verticalLayout->Add(this->_serverPortField, 0, wxTOP | wxLEFT | wxRIGHT, 10);

    this->_playerNameField = new InputField(
        this, // parent element
        "Player name:", // label
        100, // width of label
        "", // default value
        240 // width of field
    );
    this->_playerNameField->SetForegroundColour(wxColour(0, 0, 0));
    verticalLayout->Add(this->_playerNameField, 0, wxTOP | wxLEFT | wxRIGHT, 10);

    wxButton* connectButton = new wxButton(this, wxID_ANY, "Connect", wxDefaultPosition, wxSize(100, 40));
    connectButton->Bind(wxEVT_BUTTON, [](wxCommandEvent& event) {
        GameController::connectToServer();
    });
    verticalLayout->Add(connectButton, 0, wxALIGN_RIGHT | wxALL, 10);

    this->SetSizerAndFit(verticalLayout);
}


wxString ConnectionPanel::getServerAddress() {
    return this->_serverAddressField->getValue();
}


wxString ConnectionPanel::getServerPort() {
    return this->_serverPortField->getValue();
}


wxString ConnectionPanel::getPlayerName() {
    return this->_playerNameField->getValue();
}


float ConnectionPanel::getAspectRatio(int imageWidth, int imageHeight, int windowWidth, int windowHeight, float ratio){
    if(ratio > 1.0 || ratio < 0.1){
        ratio = 0.2;
    }
    if(windowWidth < windowHeight){
        int newImageWidth = 1.0*windowWidth*ratio;
        return 1.0*newImageWidth/imageWidth;
    }
    else{
        int newImageHeight = 1.0*windowHeight*ratio;
        return 1.0*newImageHeight/imageHeight;
    }
}
