#include "game_state_broadcast.h"

#include "../../../exceptions/BohnanzaException.h"
#include "../../../serialization/json_utils.h"

#define BOHN_CLIENT
#ifdef BOHN_CLIENT

#include "../../../../client/GameController.h"
#endif

game_state_broadcast::game_state_broadcast(server_response::base_class_properties props, rapidjson::Value* state_json) :
        server_response(props),
        _state_json(state_json)
{ }

game_state_broadcast::game_state_broadcast(const game_state& state) :
        server_response(server_response::create_base_class_properties(ResponseType::full_state_msg))
{
    this->_state_json = state.to_json();
}


void game_state_broadcast::write_into_json(rapidjson::Value &json,
                                           rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    server_response::write_into_json(json, allocator);
    json.AddMember("state_json", *_state_json, allocator);
}

game_state_broadcast *game_state_broadcast::from_json(const rapidjson::Value& json) {
    if (json.HasMember("state_json")) {
        return new game_state_broadcast(server_response::extract_base_class_properties(json),
                                        json_utils::clone_value(json["state_json"].GetObject()));
    } else {
        throw BohnanzaException("Could not parse full_state_response from json. state is missing.");
    }
}

game_state_broadcast::~game_state_broadcast() {
    if (_state_json != nullptr) {
        delete _state_json;
        _state_json = nullptr;
    }
}

rapidjson::Value* game_state_broadcast::get_state_json() const {
    return _state_json;
}

#ifdef BOHN_CLIENT


void game_state_broadcast::Process() const {
    try {
        game_state* state = game_state::from_json(*_state_json);
        GameController::updateGameState(state);

    } catch(std::exception& e) {
        std::cerr << "Failed to extract game_state from game_state_broadcast" << std::endl
                  << e.what() << std::endl;
    }
}

#endif
