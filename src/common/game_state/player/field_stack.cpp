#include "field_stack.h"

#include "../../exceptions/BohnanzaException.h"
#include "../../serialization/vector_utils.h"

field_stack::field_stack() : unique_serializable(){}

field_stack::field_stack(std::string id) : unique_serializable(id) { }

// deserialization constructor
field_stack::field_stack(std::string id, std::vector<card*> planted_beans) : unique_serializable(id) {
    this->_planted_beans = planted_beans;
}

field_stack::~field_stack() {
    for (int i = 0; i < _planted_beans.size(); i++) {
        delete _planted_beans.at((i));
        _planted_beans.at(i) = nullptr;
    }
    _planted_beans.clear();
}


int field_stack::get_count() const {
    return _planted_beans.size();
}

int field_stack::get_stack_bean_type() const {
    if(this->get_count() == 0){
        return -1;
    }
    else{
        return _planted_beans[0]->get_bean_type();
    }
}

// Should this function also reset the field after it gives back the count?
int field_stack::harvest_beans() const {
    int size = _planted_beans.size();
    if (size == 0) return 0;
    // First determine bean and then amount
    switch(_planted_beans.at(0)->get_bean_type()){
        case 1:
            if(size < 2) return 0;
            else if (size < 4) return 1;
            else if (size < 5) return 2;
            else if (size < 6) return 3;
            else return 4;
            break;
        case 2:
            if(size < 4) return 0;
            else if (size < 6) return 1;
            else if (size < 8) return 2;
            else if (size < 10) return 3;
            else return 4;
            break;
        case 3:
            if(size < 3) return 0;
            else if (size < 5) return 1;
            else if (size < 6) return 2;
            else if (size < 7) return 3;
            else return 4;
            break;
        case 4:
            if(size < 3) return 0;
            else if (size < 6) return 1;
            else if (size < 8) return 2;
            else if (size < 9) return 3;
            else return 4;
            break;
        case 5:
            if(size < 2) return 0;
            else if (size < 3) return 2;
            else return 3;
            break;
        case 6:
            if(size < 2) return 0;
            else if (size < 3) return 1;
            else if (size < 4) return 2;
            else if (size < 5) return 3;
            else return 4;
            break;
        case 7:
            if(size < 3) return 0;
            else if (size < 5) return 1;
            else if (size < 7) return 2;
            else if (size < 8) return 3;
            else return 4;
            break;
        case 8:
            if(size < 2) return 0;
            else if (size < 4) return 1;
            else if (size < 6) return 2;
            else if (size < 7) return 3;
            else return 4;
            break;

        default:
            return 0;
    }
}

bool field_stack::can_plant(const std::string &card_id, card *&field_stack_bean) const {
    auto it = std::find_if(_planted_beans.begin(), _planted_beans.end(),
                           [&card_id](const card* x) { return x->get_id() == card_id;});
    if (it < _planted_beans.end()) {
        field_stack_bean = *it;
        // Should not happen as a card cannot be planted twice
        return false;
    }
    else if(_planted_beans.size() == 0) return true;
    else return (_planted_beans.at(0)->can_be_played_on(field_stack_bean));
}



card* field_stack::remove_card(std::vector<card*>::iterator pos) {
    if (pos >= _planted_beans.begin() && pos < _planted_beans.end()){
        auto temp = *pos;
        _planted_beans.erase(pos);
        return temp;
    }
    else return nullptr;
}


// Not sure if we need this function
std::vector<card*>::iterator field_stack::get_card_iterator() {
    return _planted_beans.begin();
}


//#ifdef BOHNANZA_SERVER
void field_stack::setup_stack() {
    // remove all cards (if any) and clear it
    for (int i = 0; i < _planted_beans.size(); i++) {
        delete _planted_beans[i];
#ifdef DEBUG
        std::cout << "field_stack.cpp@setup_stack: free allocated memory at index " + std::to_string(i) + ", size of _planted beans: " << _planted_beans.size() << std::endl;
#endif
    }
    _planted_beans.clear();
}

// Should this function check if the plant can be planted?
bool field_stack::plant_bean(card* new_card, std::string &err) {
#ifdef DEBUG
    std::cout << "field_stack@write_into_json :> pre plant field_stack size: " << _planted_beans.size() << std::endl;
#endif
    _planted_beans.push_back(new_card);
#ifdef DEBUG
    std::cout << "field_stack@write_into_json :> post plant field_stack size: " << _planted_beans.size() << std::endl;
#endif
    return true;
}

bool field_stack::remove_card(std::string card_id, card*& played_card, std::string &err) {
    played_card = nullptr;
    auto it = std::find_if(_planted_beans.begin(), _planted_beans.end(),
                           [&card_id](const card* x) { return x->get_id() == card_id;});
    if (it < _planted_beans.end()) {
        played_card = remove_card(it);
        return true;
    } else {
        err = "Could not play card, as the requested card was not on the player's hand.";
        return false;
    }
}
//#endif


void field_stack::write_into_json(rapidjson::Value &json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);
    json.AddMember("planted_beans", vector_utils::serialize_vector(_planted_beans, allocator), allocator);
}

field_stack *field_stack::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") && json.HasMember("planted_beans")) {
        std::vector<card*> deserialized_cards = std::vector<card*>();
        for (auto &serialized_card : json["planted_beans"].GetArray()) {
            deserialized_cards.push_back(card::from_json(serialized_card.GetObject()));
        }
        return new field_stack(json["id"].GetString(), deserialized_cards);
    } else {
        throw BohnanzaException("Could not parse field_stack from json. 'planted_beans' were missing.");
    }
}
