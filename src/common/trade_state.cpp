//
// Created by widizzi on 16/05/23.
//

#include <set>
#include "trade_state.h"
#include "serialization/vector_utils.h"
#include "exceptions/BohnanzaException.h"


trading_cards* _trading_cards = nullptr;
game_state* _game_state = nullptr;

trade_state::trade_state(std::string id, game_state* game_state): unique_serializable(id){
    _game_state = game_state;
    _trading_cards = new trading_cards(game_state->get_players(), game_state->get_active_player());
    _finished = new serializable_value<bool>(false);
}

trade_state::trade_state(game_state* game_state) : unique_serializable() {
    _game_state = game_state;
#ifdef DEBUG
    std::cout << "trade_state@trade_state: about to create _trading_cards in trade state" << std::endl;
#endif
    _trading_cards = new trading_cards(game_state->get_players(), game_state->get_active_player());
#ifdef DEBUG
    std::cout << "trade_state@trade_state: created _trading_cards in trade state" << std::endl;
#endif
    _finished = new serializable_value<bool>(false);
}

trade_state::trade_state(std::string id,
                       game_state *game_state,
                       trading_cards *trading_cards,
                       serializable_value<bool>* finished)
        : unique_serializable(id)
{
#ifdef LAMA_CLIENT
    _game_state = game_state;
#endif
    _trading_cards = trading_cards;
    _finished = finished;
}

/* game state accessors */
std::vector<player*>& trade_state::get_players() {
    return _game_state->get_players();
}

player* trade_state::get_active_player() {
    return _game_state->get_active_player();
}

middle_pile* trade_state::get_middle_pile() const {
    return _game_state->get_middle_pile();
}

game_state* trade_state::get_game_state() {
    return _game_state;
}

/* accessors to set the finished state of the trade */
serializable_value<bool>* trade_state::get_finished() {
    return this->_finished;
}

void trade_state::set_finished(serializable_value<bool> finished) {
    *(this->_finished) = finished;
}


/* accessors for the trading sequence storage of offered and requested cards */
trading_cards* trade_state::get_trading_cards() {
    return _trading_cards;
}

/*
 * view of a single row in the trading panel representing the trade between two players
 *
 *  active player       trading partner
 * offered  requested  offered requested
 * ------------------------------------
 * ¦   1   ¦   2   ¦  ¦   3   ¦   4   ¦
 * ------------------------------------
 */
hand* trade_state::get_offered_cards(player* me, player* player) {
#ifdef DEBUG
    std::cout << "trade_state@ get_offered_cards: parameter values" << std::endl;
    std::cout << "trade_state@ get_offered_cards: me: " << me << " player: " << player << std::endl;
    std::cout << "trade_state@ get_offered_cards: map values" << std::endl;
#endif

    auto trade_partners = std::make_pair(me->get_id(), player->get_id());
    auto cards = _trading_cards->get_cards();
#ifdef DEBUG
    std::cout << "trade_state@get_offered_cards :> trying to acces map" << std::endl;
#endif
    auto temp = (*cards).at(trade_partners).first;
#ifdef DEBUG
    std::cout << "trade_state@get_offered_cards :> accessed map, hand count: " << temp.size() << std::endl;
#endif
    auto temp2 = temp[0];
#ifdef DEBUG
    std::cout << "trade_state@get_offered_cards :> accessed hand, returning" << std::endl;
#endif
    return temp2;
}

hand* trade_state::get_requested_cards(player* me, player* player) {
    auto trade_partners = std::make_pair(me->get_id(), player->get_id());
    auto cards = _trading_cards->get_cards();
    return (*cards).at(trade_partners).first[1];
}

/* offered cards cant be added multiple times since they are bound to the hand cards which are limited */
bool trade_state::add_offered_card(player* me, player* player, card* card) {
    assert(card != nullptr);
    hand* offered_cards = get_offered_cards(me, player);

    /* check that this card wasn't already offered */
    for(auto offered_card : offered_cards->get_cards()) {
        if(offered_card->get_id() == card->get_id()) {
#ifdef DEBUG
            std::cout << "trade_state@add_offered_cards :> card already offered" << std::endl;
#endif
            return true;
        }
    }

    if(offered_cards->get_nof_cards() < 3) {
        std::string empty = "";
        offered_cards->add_card(card, empty);
        return false;
    }

    return true;
}

/*
 * requesting is not bound to the hand cards but limited by the capacity of the seed stacks. since our implementation
 * only supports two seedstacks a player can only request two different types of cards in a trade
 */
bool trade_state::add_requested_card(player* me, player* player, card* card) {
    assert(card != nullptr);
    std::string empty = "";
    hand* requested_cards = get_requested_cards(me, player);

    if(requested_cards->get_nof_cards() < 3) {


        /*
         * prevent adding card if seed stack cant hold it. This is the case when more than two card types are requested
         */
        auto seedstacks = me->get_seed_stacks();
        assert(seedstacks.size() == 2);
        std::set<int> seed_stack_types;

        for(auto seedstack : seedstacks) {
            seed_stack_types.insert(seedstack->get_stack_bean_type());
        }
        seed_stack_types.erase(-1);

        for(auto req_card : requested_cards->get_cards()) {
            seed_stack_types.insert(req_card->get_bean_type());
        }

        if(seed_stack_types.size() >= 2) {
            for(auto bean : seed_stack_types) {
                if(card->get_bean_type() == bean) {
                    requested_cards->add_card(card, empty);
                    return true;
                }
            }
            return false;
        }
        requested_cards->add_card(card, empty);
        return true;
    }
    return false;
}

bool trade_state::remove_offered_card(player* me, player* player, card* card) {
    hand* offered_cards = get_offered_cards(me, player);
    if(offered_cards->get_nof_cards() > 0) {
        std::string empty = "";

        for(auto offerd_card : offered_cards->get_cards()) {
#ifdef DEBUG
            std::cout << "trade_state@remove_offered_card :> offerd card ptr: " << offerd_card << std::endl;
#endif
        }
#ifdef DEBUG
        std::cout << "trade_state@remove_offered_card :> current_card ptr: " << card << std::endl;
#endif

        offered_cards->remove_card(card->get_id(), card, empty);
        return 0;
    }
    return 1;
}

bool trade_state::remove_requested_card(player* me, player* player, card* card) {
    hand* requested_cards = get_requested_cards(me, player);
    if(requested_cards->get_nof_cards() >0) {
        std::string empty = "";
        requested_cards->remove_card(card->get_id(), card, empty);
        return 0;
    }
    return 1;
}


/*
 * selects card from middle_pile to build a trade around it.
 * automatically gets added to the requested cards
 */
bool trade_state::select_card_for_trade_base(player* me, card* card) {
    if(me == _game_state->get_active_player()) return false;
    // check if card id is already in requested cards and remove it if so
    // otherwise add it to requested cards
    auto req_cards = get_requested_cards(me, _game_state->get_active_player())->get_cards();
#ifdef DEBUG
    std::cout << "trade_state@select_card_for_trade_base :> requested card size: " << req_cards.size() << std::endl;
#endif
    for(auto req_card : req_cards) {
        if(req_card->get_id() == card->get_id()) {
#ifdef DEBUG
            std::cout << "trade_state@select_card_for_trade_base :> found card in requested cards, removing" << std::endl;
#endif
            remove_requested_card(me, _game_state->get_active_player(), card);
            return true;
        }
    }
#ifdef DEBUG
    std::cout << "trade_state@select_card_for_trade_base :> card not found in requested cards, adding" << std::endl;
#endif
    add_requested_card(me, _game_state->get_active_player(), card);
    return true;
}

/* accepting a trade locks adding offered and requested cards */
void trade_state::accept_trade(player *me, player *player, bool state) {
    auto trade_partners = std::make_pair(player->get_id(), me->get_id());
    auto cards = _trading_cards->get_cards();
    (*cards).at(trade_partners).second = serializable_value<bool>(state);
}

bool trade_state::get_accept(player *me, player *player) {
    auto trade_partners = std::make_pair(player->get_id(), me->get_id());
    auto cards = _trading_cards->get_cards();
    return (*cards).at(trade_partners).second.get_value();
}

/*
 * the trade_handler can update the game state when a trade gets accepted from both sides. when the trade
 * state is sent to the clients they need to update their game state to show changes caused by the trading.
 * but the client never changes the game state so it does not have to be sent back in a request. we use the
 * same function for serializing and deserializing the trade state on client and server side. So reduce message
 * size from client requests the game state is not included in the json when this function is executed on client.
 */
void trade_state::write_into_json(rapidjson::Value &json,
                                 rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    unique_serializable::write_into_json(json, allocator);

    rapidjson::Value game_state_val(rapidjson::kObjectType);

#ifdef LAMA_SERVER
    _game_state->write_into_json(game_state_val, allocator);
    json.AddMember("game_state", game_state_val, allocator);
#endif
#ifdef DEBUG
    std::cout << "trade_state@write_into_json: trying to write trading_cards into json in trade_state class" << std::endl;
#endif
#ifdef DEBUG
    std::cout << "trade_state@write_into_json :> trying to write trading_cards into json in trade_state class" << std::endl;
#endif
    rapidjson::Value trading_cards_val(rapidjson::kObjectType);
    _trading_cards->write_into_json(trading_cards_val, allocator);
    json.AddMember("trading_cards", trading_cards_val, allocator);
#ifdef DEBUG
    std::cout << "trade_state@write_into_json: successfully written trading_cards into json in trade_state class" << std::endl;
    std::cout << "trade_state@write_into_json: trying to write finished into json in trade_state class" << std::endl;
#endif
    rapidjson::Value finished_val(rapidjson::kObjectType);
    _finished->write_into_json(finished_val, allocator);
    json.AddMember("is_finished", finished_val, allocator);
#ifdef DEBUG
    std::cout << "trade_state@write_into_json: successfully written finished into json in trade_state class" << std::endl;
#endif
}

/*
 * the trade state has to be deserialized differently on client and server side to save request message size.
 * read the comment of the function write_into_json for more information
 */
trade_state* trade_state::from_json(const rapidjson::Value& json) {
#ifdef LAMA_CLIENT
    if (json.HasMember("id")
        && json.HasMember("game_state")
        && json.HasMember("trading_cards")
        && json.HasMember("is_finished"))
    {
        auto temp_game_state = game_state::from_json(json["game_state"].GetObject());
#else
        if (json.HasMember("id")
        && json.HasMember("trading_cards")
        && json.HasMember("is_finished"))
    {
        auto temp_game_state = _game_state;
#endif
        auto temp_trading_cards = trading_cards::from_json(json["trading_cards"].GetObject());
        std::map<std::pair<std::string, std::string>, std::pair<std::vector<hand*>, serializable_value<bool>>> objects;
#ifdef DEBUG
        std::cout << "trade_state :> changing pointers to match game_state players" << std::endl;
#endif
        for(auto entry : *(temp_trading_cards->get_cards())) {
            std::pair<std::string, std::string> players;
            for(auto plr : temp_game_state->get_players()) {
#ifdef DEBUG
                std::cout << "trade_state :> searching matches for " << plr << std::endl;
#endif
                if(entry.first.first == plr->get_id()) {
#ifdef DEBUG
                    std::cout << "trade_state :> first match found" << std::endl;
#endif
                    players.first = plr->get_id();
                }

                if(entry.first.second == plr->get_id()) {
#ifdef DEBUG
                    std::cout << "trade_state :> second match found" << std::endl;
#endif
                    players.second = plr->get_id();
                }
            }
#ifdef DEBUG
            std::cout << "trade_state :> insert into correct cards" << std::endl;
#endif
            objects.insert({players, entry.second});
#ifdef DEBUG
            std::cout << "trade_state :> successfully inserted" << std::endl;
#endif
        }
#ifdef DEBUG
        std::cout << "trade_state :> converting objects to map" << std::endl;
#endif
        *(temp_trading_cards->get_cards()) = objects;
#ifdef DEBUG
        std::cout << "trade_state :> pointers changed" << std::endl;
#endif

        return new trade_state(json["id"].GetString(),
                               temp_game_state,
                               temp_trading_cards,
                               serializable_value<bool>::from_json(json["is_finished"].GetObject()));


    } else {
#ifdef DEBUG
        std::cout << "trade_state@from_json: Failed to deserialize trade_state. Required entries were missing." << std::endl;
#endif
        assert(false);
        //throw LamaException("Failed to deserialize game_state. Required entries were missing.");
    }
}