
#ifndef BOHNANZA_PLAYER_H
#define BOHNANZA_PLAYER_H


#include <string>
#include "hand.h"
#include "seed_stack.h"
#include "field_stack.h"
#include "../../serialization/uuid_generator.h"
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../../serialization/unique_serializable.h"
#include "../../serialization/serializable_value.h"

class player : public unique_serializable {
private:
    serializable_value<std::string>* _player_name;
    serializable_value<int>* _coins;
    hand* _hand;
    std::vector<seed_stack*> _seed_stacks;
    std::vector<field_stack*> _bean_fields;

//#ifdef LAMA_SERVER
    std::string _game_id;
//#endif

    /*
     * Deserialization constructor
     */
    player(std::string id,
           serializable_value<std::string>* name,
           serializable_value<int>* coins,
           hand* hand,
           std::vector<seed_stack*>  seed_stacks, 
           std::vector<field_stack*> bean_fields);

public:
// constructors
    explicit player(std::string name);   // for client
    ~player();

//#ifdef LAMA_SERVER
    player(std::string id, std::string name);  // for server

    std::string get_game_id();
    void set_game_id(std::string game_id);
//#endif

    // accessors

    // Checks if both seed stacks are empty
    bool empty_seed_stack();
    int get_score() const noexcept;
    int get_nof_cards() const noexcept;
    hand* get_non_const_hand() const noexcept;
    const hand* get_hand() const noexcept;
    std::string get_player_name() const noexcept;
    std::vector<field_stack*> get_bean_fields() const;
    std::vector<seed_stack*> get_seed_stacks() const;

    // checks if the requested stack can be harvested according to the bean protection rule
    bool test_harvest(int stack, std::string& err);

//#ifdef LAMA_SERVER
    // state update functions
    bool add_card(card* card, std::string& err);

    bool remove_card(std::string card_id, card*& card, std::string& err);

    // Checks if bean is on one of the seed stacks and then plants on one of the field stacks
    // (if possible)
    bool plant_bean(std::string card_id, std::string& err);

    // function moves the top card from the hand to the seed_stack
    bool play_card(std::string& err);

    // function adds card to seed_stack of the player (card can come from the middle or trading)
    bool take_card(card* card, std::string& err);

    // harvests the requested field_stack and increases the balance of the player if required
    bool harvest_beans(int stack, std::string& err);

    void setup_round();
    

//#endif
    // serialization
    static player* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

};


#endif //BOHNANZA_PLAYER_H