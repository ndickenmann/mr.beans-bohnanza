#ifndef BOHN_REQUEST_HANDLER_H
#define BOHN_REQUEST_HANDLER_H

#include "../common/network/responses/server_response.h"
#include "../common/network/requests/client_request.h"
#include "../common/network/responses/game_state_response.h"
#include "../common/network/responses/trade_state_response.h"

class request_handler {
public:
    static server_response* handle_request(const client_request* const req);
};
#endif //BOHN_REQUEST_HANDLER_H
