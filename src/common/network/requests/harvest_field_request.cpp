#include "harvest_field_request.h"

// Public constructor
harvest_field_request::harvest_field_request(std::string player_id, std::string field_id)
        : client_request( client_request::create_base_class_properties(RequestType::harvest_field, uuid_generator::generate_uuid_v4(), player_id) ),
          _field_id(field_id)
{ }


// private constructor for deserialization
harvest_field_request::harvest_field_request(client_request::base_class_properties props, std::string field_id) :
        client_request(props),
        _field_id(field_id)
{ }

void harvest_field_request::write_into_json(rapidjson::Value &json,
                                        rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    client_request::write_into_json(json, allocator);
    rapidjson::Value field_id_val(_field_id.c_str(), allocator);
    json.AddMember("field_id", field_id_val, allocator);
}

harvest_field_request* harvest_field_request::from_json(const rapidjson::Value& json) {
    if (json.HasMember("field_id")) {
        return new harvest_field_request(client_request::extract_base_class_properties(json), json["field_id"].GetString());
    } else {
        throw BohnanzaException("Could not parse harvest_field_request from json. field_id is missing.");
    }
}

