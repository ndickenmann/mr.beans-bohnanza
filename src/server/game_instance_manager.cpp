// The game_instance_manager only exists on the server side. It stores all currently active games and offers
// functionality to retrieve game instances by id and adding players to games.
// If a new player requests to join a game but no valid game_instance is available, then this class
// will generate a new game_instance and add it to the unordered_map of (active) game instances.

#include "game_instance_manager.h"

#include "player_manager.h"
#include "server_network_manager.h"

// static member initialisation
// std::unordered_map<std::string, game_instance*> game_instance_manager::games_lut = {};
game_instance* game_instance_manager::current_game = nullptr;

game_instance* game_instance_manager::create_new_game() {
    game_instance* new_game = new game_instance();
    games_lut_lock.lock();  // exclusive
    game_instance_manager::current_game = new_game;
    games_lut_lock.unlock();
    return new_game;
}

game_instance* game_instance_manager::get_game() {
    if(current_game == nullptr) current_game = create_new_game();
    return current_game;
}

bool game_instance_manager::try_get_player_and_game_instance(const std::string& player_id, player *&player, game_instance *&game_instance_ptr, std::string& err) {
    if (player_manager::try_get_player(player_id, player)) {
        game_instance_ptr = game_instance_manager::current_game;
        return true;
    } else {
        err = "Could not find requested player " + player_id + " in database.";
    }
    return false;
}

bool game_instance_manager::try_add_player_to_game(player *player, game_instance* &game_instance_ptr, std::string& err) {

    // check that player is not already subscribed to another game
    if (player->get_game_id() != "") {
        if (game_instance_ptr != nullptr && player->get_game_id() != game_instance_ptr->get_id()) {
            err = "Could not join game with id " + game_instance_ptr->get_id() + ". Player is already active in a different game with id " + player->get_game_id();
        } else {
            err = "Could not join game. Player is already active in a game";
        }
        return false;
    }


    if (game_instance_ptr == nullptr) {
        // Join any non-full, non-started game
        for (int i = 0; i < 10; i++) {
            // make at most 10 attempts of joining a src (due to concurrency, the game could already be full or started by the time
            // try_add_player_to_any_game() is invoked) But with only few concurrent requests it should succeed in the first iteration.
            game_instance_ptr = get_game();
            if (try_add_player(player, game_instance_ptr, err)) {
                return true;
            }
        }
        return false;
    }
    else {
        return try_add_player(player, game_instance_ptr, err);
    }
}

bool game_instance_manager::try_add_player(player *player, game_instance* &game_instance_ptr, std::string& err) {
    if (player->get_game_id() != "") {
        if (player->get_game_id() != game_instance_ptr->get_id()) {
            err = "Player is already active in a different src with id " + player->get_game_id();
        } else {
            err = "Player is already active in this src";
        }
        return false;
    }

    std::cout<<current_game<<std::endl;
    for(auto i:current_game->get_game_state()->get_players()){
        if(player->get_player_name() == i->get_player_name()){
            err = "Could not join game. A player has already this name!";
            return false;
        }
    }

    if (game_instance_ptr->try_add_player(player, err)) {
        player->set_game_id(game_instance_ptr->get_id());   // mark that this player is playing in a src
        return true;
    } else {
        return false;
    }
}

bool game_instance_manager::try_remove_player(player *player, game_instance *&game_instance_ptr, std::string &err) {
    return game_instance_ptr->try_remove_player(player, err);
}

