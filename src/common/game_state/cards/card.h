

#ifndef BOHNANZA_CARD_H
#define BOHNANZA_CARD_H

#include <string>
#include "../../serialization/unique_serializable.h"
#include "../../serialization/serializable_value.h"
#include "../../../../rapidjson/include/rapidjson/document.h"


class card : public unique_serializable {
private:
    // Using number instead enum because its simpler
    serializable_value<int>* _bean_type;

    // from_diff constructor
    card(std::string id);
public:
    card(int type);

    // deserialization constructor (moved to public only for testing)
    card(std::string id, serializable_value<int>* type);
    

    ~card();

// accessors
    int get_bean_type() const noexcept;

// card functions
    bool can_be_played_on(const card* const other) const noexcept;

// serializable interface
    void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static card* from_json(const rapidjson::Value& json);
};


#endif //BOHNANZA_CARD_H