#include "GameController.h"
#include "../common/network/requests/join_game_request.h"
#include "../common/network/requests/start_game_request.h"
#include "../common/network/requests/harvest_field_request.h"
#include "../common/network/requests/init_trade_request.h"
#include "../common/network/requests/finish_trade_request.h"
#include "../common/network/requests/update_trade_request.h"
#include "../common/network/requests/plant_card_request.h"
#include "network/ClientNetworkManager.h"
#include "../common/serialization/unique_serializable.h"



// initialize static members
GameWindow* GameController::_gameWindow = nullptr;
ConnectionPanel* GameController::_connectionPanel = nullptr;
MainGamePanel* GameController::_mainGamePanel = nullptr;
TradingPanel* GameController::_tradingPanel = nullptr;

player* GameController::_me = nullptr;
game_state* GameController::_currentGameState = nullptr;
trade_state* GameController::_currentTradeState = nullptr;
bool GameController::startedTrading = false;
bool GameController::firstOpenTrading = true;



void GameController::init(GameWindow* gameWindow) {

    GameController::_gameWindow = gameWindow;

    // Set up main panels
    GameController::_connectionPanel = new ConnectionPanel(gameWindow);
    GameController::_mainGamePanel = new MainGamePanel(gameWindow);
    GameController::_tradingPanel = new TradingPanel(gameWindow);

    // Hide all panels
    GameController::_connectionPanel->Show(true);
    GameController::_mainGamePanel->Show(false);
    GameController::_tradingPanel->Show(false);

    // Only show connection panel at the start of the game
    GameController::_gameWindow->showPanel(GameController::_connectionPanel);

    // show mainGamePanel
    //GameController::_gameWindow->showPanel(GameController::_mainGamePanel);


    // Set status bar
    GameController::showStatus("Not connected");
}


void GameController::connectToServer() {

    // get values form UI input fields
    wxString inputServerAddress = GameController::_connectionPanel->getServerAddress().Trim();
    wxString inputServerPort = GameController::_connectionPanel->getServerPort().Trim();
    wxString inputPlayerName = GameController::_connectionPanel->getPlayerName().Trim();

    // check that all values were provided
    if(inputServerAddress.IsEmpty()) {
        GameController::showError("Input error", "Please provide the server's address");
        return;
    }
    if(inputServerPort.IsEmpty()) {
        GameController::showError("Input error", "Please provide the server's port number");
        return;
    }
    if(inputPlayerName.IsEmpty()) {
        GameController::showError("Input error", "Please enter your desired player name");
        return;
    }

    // convert host from wxString to std::string
    std::string host = inputServerAddress.ToStdString();

    // convert port from wxString to uint16_t
    unsigned long portAsLong;
    if(!inputServerPort.ToULong(&portAsLong) || portAsLong > 65535) {
        GameController::showError("Connection error", "Invalid port");
        return;
    }
    uint16_t port = (uint16_t) portAsLong;

    // convert player name from wxString to std::string
    std::string playerName = inputPlayerName.ToStdString();

    // connect to network
    ClientNetworkManager::init(host, port);

    // send request to join game
    GameController::_me = new player(playerName);
    join_game_request request = join_game_request(GameController::_me->get_id(), GameController::_me->get_player_name());
    ClientNetworkManager::sendRequest(request);

}


void GameController::updateGameState(game_state* newGameState) {

    // the existing game state is now old
    game_state* oldGameState = GameController::_currentGameState;

    // save the new game state as our current game state
    GameController::_currentGameState = newGameState;


    if(GameController::_currentGameState->is_finished()) {
        GameController::showGameOverMessage();
    }

    // make sure we are showing the main game panel in the window (if we are already showing it, nothing will happen)
    GameController::_gameWindow->showPanel(GameController::_mainGamePanel);

    // command the main game panel to rebuild itself, based on the new game state
    GameController::_mainGamePanel->buildGameState(GameController::_currentGameState, GameController::_me);
}


void GameController::startGame() {
    start_game_request request = start_game_request(GameController::_me->get_id());
    ClientNetworkManager::sendRequest(request);
}

//handles event on ui
wxEvtHandler* GameController::getMainThreadEventHandler() {
    return GameController::_gameWindow->GetEventHandler();
}

//shows error type
void GameController::showError(const std::string& title, const std::string& message) {
    wxMessageBox(message, title, wxICON_ERROR);
}

//sets status message
void GameController::showStatus(const std::string& message) {
    GameController::_gameWindow->setStatus(message);
}

//exclusive Mr. Beans functions

//sends plantCard request
void GameController::plantCard(std::string card_id) {
    plant_card_request request = plant_card_request(GameController::_currentGameState->get_id(), GameController::_me->get_id(), card_id);
    ClientNetworkManager::sendRequest(request);
}


//sends harvestField request
void GameController::harvestField(std::string field) {
    harvest_field_request request = harvest_field_request(GameController::_me->get_id(),field);
    ClientNetworkManager::sendRequest(request);
}


/* * * * * * * * TRADING * * * * * * * */

/* * * * REQUESTING SIDE * * * */

/* requests */
void GameController::initTrade() {
#ifdef DEBUG
    std::cout << "GameController@updateTrade :> create initTrade request" << std::endl;
#endif
    init_trade_request request = init_trade_request(GameController::_me->get_id());
    ClientNetworkManager::sendRequest(request);
}

void GameController::updateTrade(trade_state* _trade_state) {
#ifdef DEBUG
    std::cout << "GameController@updateTrade :> create updateTrade request" << std::endl;
#endif
    update_trade_request request = update_trade_request(GameController::_me->get_id(),_trade_state);
#ifdef DEBUG
    std::cout << "GameController@updateTrade :> sending updateTrade request" << std::endl;
#endif
    ClientNetworkManager::sendRequest(request);
}

void GameController::finishTrade() {
    finish_trade_request request = finish_trade_request(GameController::_me->get_id());
    ClientNetworkManager::sendRequest(request);
}

/* control functions */
void GameController::add_offered_card(player* me, player* player, card* card) {
    if(!_currentTradeState->get_accept(me, player)) {
        _currentTradeState->add_offered_card(me, player, card);
        updateTrade(_currentTradeState);
    }
}

void GameController::add_requested_card(player* me, player* player, card* card) {
    if(!_currentTradeState->get_accept(me, player)) {
        _currentTradeState->add_requested_card(me, player, card);
        updateTrade(_currentTradeState);
    }
}

void GameController::remove_offered_card(player* me, player* player, card* card) {
    if(!_currentTradeState->get_accept(me, player)) {
#ifdef DEBUG
        std::cout << "GameState@remove_offered_card :> trying to remove offered card" << std::endl;
#endif
        _currentTradeState->remove_offered_card(me, player, card);
        updateTrade(_currentTradeState);
    }
}

void GameController::remove_requested_card(player* me, player* player, card* card) {
    if(!_currentTradeState->get_accept(me, player)) {
#ifdef DEBUG
        std::cout << "GameState@remove_requested_card :> trying to remove requested card" << std::endl;
#endif
        _currentTradeState->remove_requested_card(me, player, card);
        updateTrade(_currentTradeState);
    }
}

void GameController::select_card_for_trade_base(player* me, player* player, card* card) {
    if(!_currentTradeState->get_accept(me, player)) {
        _currentTradeState->select_card_for_trade_base(me, card);
        updateTrade(_currentTradeState);
    }
}

void GameController::accept_trade(player* me, player* player, bool state) {
    _currentTradeState->accept_trade(me, player, state);
    updateTrade(_currentTradeState);
}

/* * * * RESPONDING SIDE * * * */

void GameController::updateTradeState(trade_state* newTradeState) {
#ifdef DEBUG
    std::cout << "GameController@updateTradeState: reachted game controller update trade state" << std::endl;
#endif
    // save the new game state as our current game state
    GameController::_currentTradeState = newTradeState;
    GameController::_currentGameState = newTradeState->get_game_state();

    if(GameController::_currentTradeState->get_finished()->get_value() == true){
#ifdef DEBUG
        std::cout << "GameController@updateTradeState:switching to main game panel" << std::endl;
#endif
        GameController::_gameWindow->showPanel(GameController::_mainGamePanel);
        GameController::_mainGamePanel->buildGameState(_currentGameState, GameController::_me);
        firstOpenTrading = true;
        return;
    }
#ifdef DEBUG
    std::cout << "GameController@updateTradeState:showing panel" << std::endl;
#endif
    if (firstOpenTrading) {
        // make sure we are showing the main game panel in the window (if we are already showing it, nothing will happen)
        GameController::_gameWindow->showPanel(GameController::_tradingPanel);
        firstOpenTrading = false;
    }


#ifdef DEBUG
    std::cout << "building trade state" << std::endl;
#endif
    // command the main game panel to rebuild itself, based on the new game state
    GameController::_tradingPanel->buildTradeState(GameController::_currentTradeState, GameController::_me);
}

void GameController::closeTradingPanel() {
    //GameController::_tradingPanel->Show(false);
    GameController::_mainGamePanel->buildGameState(GameController::_currentGameState, GameController::_me);
    GameController::_gameWindow->showPanel(GameController::_mainGamePanel);
}

void GameController::openTradingPanel() {
    GameController::_gameWindow->showPanel(GameController::_tradingPanel);
}


//should work like this
void GameController::showGameOverMessage() {
    std::string title = "Game Over!";
    std::string message = "Final score:\n";
    std::string buttonLabel = "Close Game";

    // sort players by score
    std::vector<player*> players = GameController::_currentGameState->get_players();
    std::sort(players.begin(), players.end(), [](const player* a, const player* b) -> bool {
        return a->get_score() > b->get_score();
    });

    // list all players
    for(int i = 0; i < players.size(); i++) {

        player* playerState = players.at(i);
        std::string scoreText = std::to_string(playerState->get_score());

        // first entry is the winner
        std::string winnerText = "";
        if(i == 0) {
            winnerText = "     Winner!";
        }

        std::string playerName = playerState->get_player_name();
        if(playerState->get_id() == GameController::_me->get_id()) {
            playerName = "You";

            if(i == 0) {
                winnerText = "     You won!!!";
            }
        }
        message += "\n" + playerName + ":     " + scoreText + winnerText;
    }

    wxMessageDialog dialogBox = wxMessageDialog(nullptr, message, title, wxICON_NONE);
    dialogBox.SetOKLabel(wxMessageDialog::ButtonLabel(buttonLabel));
    int buttonClicked = dialogBox.ShowModal();
    if(buttonClicked == wxID_OK) {
        GameController::_gameWindow->Close();
    }
}

bool GameController::getStartedTrading() {
    return startedTrading;
}

void GameController::setStartedTrading(bool newStartedTrading) {
    startedTrading = newStartedTrading;
}