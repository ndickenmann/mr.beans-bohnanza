#include "ImagePanel.h"

// constructor
ImagePanel::ImagePanel(wxWindow* parent, wxString file, wxBitmapType format, wxPoint position, wxSize size, double rotation) :
        wxPanel(parent, wxID_ANY, position, size)
{
    if(!wxFileExists(file)) {
        wxMessageBox("Could not find file: " + file, "File error", wxICON_ERROR);
        return;
    }

    if(!this->_image.LoadFile(file, format)) {
        wxMessageBox("Could not load file: " + file, "File error", wxICON_ERROR);
        return;
    }

    this->_rotation = rotation;

    this->_width = -1;
    this->_height = -1;

    this->Bind(wxEVT_PAINT, &ImagePanel::paintEvent, this);
    this->Bind(wxEVT_SIZE, &ImagePanel::onSize, this);
}

// redraws ImagePanel
void ImagePanel::paintEvent(wxPaintEvent& event) {
    // this code is called when the system requests this panel to be redrawn.

    if(!this->_image.IsOk()) {
        return;
    }

    wxPaintDC deviceContext = wxPaintDC(this);

    int newWidth;
    int newHeight;
    deviceContext.GetSize(&newWidth, &newHeight);

    if(newWidth != this->_width || newHeight != this->_height) {

        wxImage transformed;

        if(this->_rotation == 0.0) {
            transformed = this->_image.Scale(newWidth, newHeight, wxIMAGE_QUALITY_BILINEAR);

        } else {
            wxPoint centerOfRotation = wxPoint(this->_image.GetWidth() / 2, this->_image.GetHeight() / 2);
            transformed = this->_image.Rotate(this->_rotation, centerOfRotation, true);
            transformed = transformed.Scale(newWidth, newHeight, wxIMAGE_QUALITY_BILINEAR);
        }
        this->_bitmap = wxBitmap(transformed);
        this->_width = transformed.GetWidth();
        this->_height = transformed.GetHeight();

        deviceContext.DrawBitmap(this->_bitmap, 0, 0, false);
    } else {
        deviceContext.DrawBitmap(this->_bitmap, 0, 0, false);
    }

    if(!interactive){
        return;
    }

    wxPaintDC dc(this);
    wxRect rect = GetClientRect();
    wxPen borderPen(wxColour(0, 0, 0), 0); // Green border color
    wxBrush transparentBrush(*wxTRANSPARENT_BRUSH);
    switch (this->borderMode) {
        case 0:
            break;

        case 1:
            borderPen.SetColour(wxColour(0, 255, 0)); // Blue color
            borderPen.SetWidth(5);
            dc.SetPen(borderPen);
            dc.SetBrush(transparentBrush);
            dc.DrawRectangle(rect);
            break;

        case 2:
            borderPen.SetColour(wxColour(255, 0, 0)); // Blue color
            borderPen.SetWidth(5);
            dc.SetPen(borderPen);
            dc.SetBrush(transparentBrush);
            dc.DrawRectangle(rect);
            break;
    }
}

// changes the border of ImagePanel
void ImagePanel::setBorderMode(int mode){
    this->borderMode = mode;
    Refresh();
}


// updates visual behaviour of ImagePanel
void ImagePanel::updateInteraction(int interaction){
    if(!interactive){
        this->clicked = false;
        return;
    }
    switch (interaction) {
        case 0:
            if(!this->clicked){
                this->setBorderMode(0);
            }
            break;

        case 1:
            if(!this->clicked){
                this->setBorderMode(1);
            }
            break;

        case 2:
            if(!this->clicked){
                this->clicked = true;
                this->setBorderMode(2);
            }
            else{
                this->clicked = false;
                this->setBorderMode(0);
            }
            break;

        default:
            this->clicked = false;
            this->setBorderMode(0);
            break;
    }
}

// function to set interactive mode
void ImagePanel::setInteractionMode(bool mode) {
    interactive = mode;
}

// get function
bool ImagePanel::getInteractionMode(){
    return this->interactive;
}

// gets called by a wxSizeEvent
void ImagePanel::onSize(wxSizeEvent& event) {

    // when the user resizes this panel, the image should redraw itself
    Refresh();

    // skip any other effects of this event.
    event.Skip();
}