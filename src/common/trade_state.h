//
// Created by widizzi on 16/05/23.
//

#ifndef TRADE_STATE_H
#define TRADE_STATE_H

#include <vector>
#include <string>
#include <map>
#include <utility>
#include "../../rapidjson/include/rapidjson/document.h"
#include "game_state/player/player.h"
#include "game_state/cards/middle_pile.h"
#include "game_state/game_state.h"
#include "trading_cards.h"
#include "serialization/serializable.h"
#include "serialization/serializable_value.h"
#include "serialization/unique_serializable.h"

class trade_state : public unique_serializable {
private:

    /* variable to determine if the trading sequence is finished to close the panel on client side */
    serializable_value<bool>* _finished;

public:
    trade_state(std::string id, game_state* game_state);
    trade_state(game_state* game_state);

    // desirialization constructor
    trade_state(std::string id, game_state* game_state, trading_cards* trading_cards, serializable_value<bool>* finished);

    std::vector<player*>& get_players();
    player* get_active_player();

    static game_state* get_game_state();
    static trading_cards* get_trading_cards();

    serializable_value<bool>* get_finished();
    void set_finished(serializable_value<bool> finished);

    // TODO: change this return type to the new datatype for offered and requested cards
    static hand* get_offered_cards(player* me, player* player);
    static hand* get_requested_cards(player* me, player* player);

    static bool add_offered_card(player* me, player* player, card* card);
    static bool add_requested_card(player* me, player* player, card* card);

    static bool remove_offered_card(player* me, player* player, card* card);
    static bool remove_requested_card(player* me, player* player, card* card);

    middle_pile* get_middle_pile() const;

    /*
     * when card from the middle pile is selected, ui calls this function and it should be
     * written into the requested_cards of the player
     */
    static bool select_card_for_trade_base(player* me, card* card);

    static void accept_trade(player* me, player* player, bool state);

    static bool get_accept(player* me, player* player);


    /* * * * * * serializable interface * * * * * */
    static trade_state* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

};

extern trading_cards* _trading_cards;
extern game_state* _game_state;

#endif // TRADE_STATE_H