#ifndef BOHNANZA_TRADE_STATE_RESPONSE_H
#define BOHNANZA_TRADE_STATE_RESPONSE_H

#include <string>
#include "server_response.h"


class trade_state_response : public server_response{
private:
    bool _success;
    std::string _err;
    std::string _req_id;
    rapidjson::Value* _state_json = nullptr;

    trade_state_response(base_class_properties props, std::string req_id, bool success, rapidjson::Value* state_json, std::string& err);

public:

    trade_state_response(std::string req_id, bool success, rapidjson::Value* state_json, std::string err);
    ~trade_state_response();

    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static trade_state_response* from_json(const rapidjson::Value& json);

#define BOHN_CLIENT
#ifdef BOHN_CLIENT
    virtual void Process() const override;
#endif
};



#endif //BOHNANZA_TRADE_STATE_RESPONSE_H
