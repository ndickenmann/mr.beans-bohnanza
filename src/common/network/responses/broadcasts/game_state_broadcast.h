#ifndef BOHN_FULL_STATE_RESPONSE_H
#define BOHN_FULL_STATE_RESPONSE_H

#include "../server_response.h"
#include "../../../game_state/game_state.h"

class game_state_broadcast : public server_response {
private:
    rapidjson::Value* _state_json;

    /*
     * Private constructor for deserialization
     */
    game_state_broadcast(base_class_properties props, rapidjson::Value* state_json);

public:

    game_state_broadcast(const game_state& state);
    ~game_state_broadcast();

    rapidjson::Value* get_state_json() const;

    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static game_state_broadcast* from_json(const rapidjson::Value& json);

#define BOHN_CLIENT
#ifdef BOHN_CLIENT //TID
    virtual void Process() const override;
#endif
};


#endif //BOHN_FULL_STATE_RESPONSE_H
