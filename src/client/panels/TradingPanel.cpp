//
// Created by nicolas on 11.05.23.
//
#include "TradingPanel.h"
#include "../uiElements/ImagePanel.h"
#include "../GameController.h"

TradingPanel::TradingPanel(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(1200, 900)) {
}

//global variable to define player who the active player is trading with
std::string specialPlayer = "";
int flag = 0; //first time building interface

//these next two functions are only used for the active player to trade with his chosen trading partner
void TradingPanel::activateRequestingCard(player* me, player* activePlayer,card currentcard, std::vector<player*> players){
    if(specialPlayer != "") {
#ifdef DEBUG
        std::cout << "TradingPanel@activateTrading :> call to add_requested_card" << std::endl;
#endif

        player* dude = nullptr;
        std::vector<player*>::iterator it = std::find_if(players.begin(), players.end(), [](const player* x) {
            return x->get_id() == specialPlayer;
        });
        if (it < players.end()) {
            dude = *it;//muss das so sein??->ist das nicht einf schon me von davor??
        } else {
            GameController::showError("ERROR.", "ERROR");
            return;
        }
        GameController::add_requested_card(me, dude, &currentcard);
    }
}
void TradingPanel::activateOfferingCard(player *me, player *activePlayer, card currentcard, std::vector<player*> players) {
    if(specialPlayer != "") {
#ifdef DEBUG
        std::cout << "TradingPanel@activateTrading :> call to add_offered_card" << std::endl;
#endif
        player* dude = nullptr;
        std::vector<player*>::iterator it = std::find_if(players.begin(), players.end(), [](const player* x) {
            return x->get_id() == specialPlayer;
        });
        if (it < players.end()) {
            dude = *it;//muss das so sein??->ist das nicht einf schon me von davor??
        } else {
            GameController::showError("ERROR.", "ERROR");
            return;
        }
        GameController::add_offered_card(me, dude, &currentcard);
    }
}

//buildTradeState analogous to buildGameState
void TradingPanel::buildTradeState(trade_state* tradeState, player* me) {
    // remove any existing UI
    this->DestroyChildren();

    SetBackgroundColour(wxColour("#f0ead2"));

    std::vector<player*> players = tradeState->get_players();
    int numberOfPlayers = players.size();

    // find our own player object in the list of players
    int myPosition = -1;
    std::vector<player*>::iterator it = std::find_if(players.begin(), players.end(), [me](const player* x) {
        return x->get_id() == me->get_id();
    });
    if (it < players.end()) {
        me = *it;//muss das so sein??->ist das nicht einf schon me von davor??
        myPosition = it - players.begin();
    } else {
        GameController::showError("Trade state error", "Could not find this player among players of server game.");
        return;
    }

    // still has to be implemented
    // find active player object in the list of players
    player* activePlayer = tradeState->get_active_player();
    int activePosition = -1;
    std::vector<player*>::iterator it1 = std::find_if(players.begin(), players.end(), [activePlayer](const player* x) {
        return x->get_id() == activePlayer->get_id();
    });
    if (it1 < players.end()) {
        activePlayer = *it1;
        activePosition = it1 - players.begin();
    } else {
        GameController::showError("Trade state error", "Could not find this active player among players of server game.");
        return;
    }

    //build sizer and first row
    wxBoxSizer* vercticalSizer;
    vercticalSizer = new wxBoxSizer( wxVERTICAL );
    vercticalSizer->Add( 0, 0, 1, wxEXPAND, 5 );

    buildFirstRow(vercticalSizer, tradeState,me, activePlayer);
    buildSecondRow(vercticalSizer);

    std::cout << me->get_id() << std::endl;
    std::cout << activePlayer->get_id() << std::endl;
    //if I am the active player build my lay out
    if(me->get_id() == activePlayer->get_id()){
        for(int i = 0; i < numberOfPlayers; i++) {
            if(i != activePosition) {
                if(flag == 0) {
                    specialPlayer = players[i]->get_id();
                    flag = 1;
                }
                buildActiveRow(vercticalSizer, tradeState, me, players[i]);
            }
        }
    }else {

       // std::vector<wxBoxSizer> sizers;
        specialPlayer = "";
        flag = 0;

        for (int i = 0; i < numberOfPlayers; i++) {
            assert(numberOfPlayers < 6);

            if (i == myPosition) {

            }else if(i == activePosition){
                buildActiveRow(vercticalSizer, tradeState,me, activePlayer);
            } else {
               buildPassivRow(vercticalSizer, tradeState, me, activePlayer, players[i]);

            }

        }
    }
    buildUrCards(vercticalSizer, tradeState, me, activePlayer);
    vercticalSizer->Add( 0, 0, 1, wxEXPAND, 5 );



    this->SetSizer( vercticalSizer );

    this->Layout();

}

//builds palette with all Bohnanza cards to request from
void TradingPanel::buildFirstRow(wxBoxSizer* vercticalSizer, trade_state* tradeState, player* me, player* activePlayer){
    wxBoxSizer* firstRow;
    firstRow = new wxBoxSizer( wxHORIZONTAL );

    firstRow->Add( 0, 0, 1, wxEXPAND, 5 );

    for(int i = 1; i < 9; ++i) {
        std::string cardFile = "assets/bohn_" + std::to_string(i) + ".png";

        card* currentcard_ptr = new card(i);
        card currentcard = *currentcard_ptr;

        ImagePanel *cardButton = new ImagePanel(this, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, cardSize);

        cardButton->SetToolTip("Request card");
        cardButton->SetCursor(wxCursor(wxCURSOR_HAND));
        cardButton->setInteractionMode(true);
        if(me!=activePlayer) {
            cardButton->Bind(wxEVT_LEFT_UP, [cardButton, me, activePlayer, currentcard_ptr](wxMouseEvent &event) {
                cardButton->updateInteraction(2);
                GameController::add_requested_card(me, activePlayer, currentcard_ptr);
            });
        }else{
            cardButton->Bind(wxEVT_LEFT_UP, [this,cardButton,me, activePlayer, currentcard, tradeState](wxMouseEvent &event) {
                cardButton->updateInteraction(2);
                activateRequestingCard(me, activePlayer, currentcard, tradeState->get_players());
            });
        }
        firstRow->Add( cardButton, 0, wxALL, 5 );
    }

    firstRow->Add(0, 0, 1, wxEXPAND, 5);

    vercticalSizer->Add( firstRow, 1, wxALL|wxEXPAND, 5 );
}

//builds the text above the trading fields
void TradingPanel::buildSecondRow(wxBoxSizer* vercticalSizer){
    wxBoxSizer* secondRow;
    secondRow = new wxBoxSizer( wxHORIZONTAL );
    secondRow->Add( 0, 0, 1, wxEXPAND, 5 );

    std::string offeredcardsstring = "offered cards";
    wxStaticText* offeredcardstext = new wxStaticText(this, wxID_ANY,  offeredcardsstring, wxDefaultPosition, wxSize(150, 20), wxID_ANY);
    offeredcardstext->SetForegroundColour(wxColour("#000000"));
    secondRow->Add( offeredcardstext, 1, wxALIGN_CENTER, 5 );

    std::string requestedcardsstring = "requested cards";
    wxStaticText* requestedcardstext = new wxStaticText(this, wxID_ANY,  requestedcardsstring, wxDefaultPosition, wxSize(150, 20), wxID_ANY);
    requestedcardstext->SetForegroundColour(wxColour("#000000"));
    secondRow->Add( requestedcardstext, 1, wxALIGN_CENTER, 5 );

    secondRow->Add( 100, 0, 1, wxALL, 5 );

    std::string offeredcardsstring_other = "offered cards";
    wxStaticText* offeredcardstext_other = new wxStaticText(this, wxID_ANY,  offeredcardsstring_other, wxDefaultPosition, wxSize(150, 20), wxID_ANY);
    offeredcardstext_other->SetForegroundColour(wxColour("#000000"));
    secondRow->Add( offeredcardstext_other, 1, wxALIGN_CENTER, 5 );

    std::string requestedcardsstring_other = "requested cards";
    wxStaticText* requestedcardstext_other = new wxStaticText(this, wxID_ANY,  requestedcardsstring_other, wxDefaultPosition, wxSize(150, 20), wxID_ANY);
    requestedcardstext_other->SetForegroundColour(wxColour("#000000"));
    secondRow->Add( requestedcardstext_other, 1, wxALIGN_CENTER, 5 );


    secondRow->Add( 0, 0, 1, wxEXPAND, 5 );

    vercticalSizer->Add( secondRow, 0.1, wxEXPAND, 5 );

}

//builds a row where you can participate in trading -> if me = activePlayer then the "activePlayer" is my tradingpartner
void TradingPanel::buildActiveRow(wxBoxSizer* vercticalSizer, trade_state* tradeState,player* me, player* activePlayer){
    wxBoxSizer* urRow;
    urRow = new wxBoxSizer( wxHORIZONTAL );

    urRow->Add( 0, 0, 1, wxEXPAND, 5 );

    wxButton* m_buttonAccept = new wxButton( this, wxID_ANY, wxT("✔"), wxDefaultPosition, wxSize(35,35), 0 );
    m_buttonAccept->Bind(wxEVT_BUTTON, [me, activePlayer, tradeState](wxCommandEvent& event) {
        if(tradeState->get_accept(me, activePlayer)) {
            GameController::accept_trade(me, activePlayer, false);
        } else {
            GameController::accept_trade(me, activePlayer, true);
        }
    });

    if(tradeState->get_accept(me, activePlayer)) {
        m_buttonAccept->SetBackgroundColour("#b9ca88");
    } else {
        m_buttonAccept->SetBackgroundColour("#e84f25");
    }

    urRow->Add( m_buttonAccept, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

    urRow->Add( 0, 0, 1, wxEXPAND, 5 );



    wxPanel* panel_2 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_2->SetBackgroundColour(wxColour("#b9ca88"));
    wxBoxSizer* urRow_2 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_2_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_2_2 = new wxBoxSizer( wxHORIZONTAL );


    hand* offered_cards = tradeState->get_offered_cards(me, activePlayer);
#ifdef DEBUG
    std::cout << "trading_panel@build_active_row :> got offered cards" << std::endl;
#endif
    std::vector<card*>* vector_offered_cards = new std::vector<card*>;
    *vector_offered_cards=  offered_cards->get_cards();
    std::cout<<(*vector_offered_cards).size()<<std::endl;



    for(int i = 0; i < (*vector_offered_cards).size(); ++i) {
        card* currentcard_ptr=  (*vector_offered_cards)[i];
        card currentcard = *currentcard_ptr;

        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_2, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51,80));

        cardButton->SetToolTip("Delete offered card");
        cardButton->SetCursor(wxCursor(wxCURSOR_HAND));
        cardButton->setInteractionMode(true);
        if(specialPlayer == activePlayer->get_id()){
            cardButton->updateInteraction(1);

        }
        cardButton->Bind(wxEVT_LEFT_UP, [me, cardButton, activePlayer,currentcard_ptr](wxMouseEvent &event) {
            cardButton->updateInteraction(2);
            GameController::remove_offered_card(me, activePlayer, currentcard_ptr);

        });
        urRow_2_1->Add(cardButton, 0, wxALL, 5);
    }
    for(int i = 0; i < 3 - (*vector_offered_cards).size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_2, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51,80));
        if(specialPlayer == activePlayer->get_id()){
            cardButton->setInteractionMode(true);
            cardButton->updateInteraction(1);
        }
        urRow_2_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string offeredcardsstring = me->get_player_name();
    wxStaticText* offeredcardstext = new wxStaticText(panel_2, wxID_ANY,  offeredcardsstring, wxDefaultPosition, wxDefaultSize, wxID_ANY);
    offeredcardstext->SetForegroundColour(wxColour("#000000"));


    urRow_2_2->Add( offeredcardstext, 1, wxALIGN_CENTER, 5 );


    urRow_2->Add( urRow_2_1, 1, wxEXPAND, 5 );
    urRow_2->Add( urRow_2_2, 0.1, wxEXPAND, 5 );

    panel_2-> SetSizerAndFit( urRow_2 );
    urRow->Add(panel_2, 1, wxALL, 5);


    auto panel_3 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_3->SetBackgroundColour(wxColor("#b9ca88"));
    auto urRow_3 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_3_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_3_2 = new wxBoxSizer( wxHORIZONTAL );

        //set requested cards
        hand* requested_cards = tradeState->get_requested_cards(me, activePlayer);
    std::vector<card*> vector_requested_cards = requested_cards->get_cards();
    for(int i = 0; i < vector_requested_cards.size(); ++i) {
            card* currentcard_ptr = vector_requested_cards[i];
            card currentcard = *currentcard_ptr;

            std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

            ImagePanel *cardButton = new ImagePanel(panel_3, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));

            cardButton->SetToolTip("Delete requested card");
            cardButton->SetCursor(wxCursor(wxCURSOR_HAND));
            cardButton->setInteractionMode(true);
            if(specialPlayer == activePlayer->get_id()){
                cardButton->updateInteraction(1);
            }
            cardButton->Bind(wxEVT_LEFT_UP, [me,cardButton, activePlayer,currentcard_ptr](wxMouseEvent &event) {
                cardButton->updateInteraction(2);
                GameController::remove_requested_card(me, activePlayer, currentcard_ptr);

            });
            urRow_3_1->Add(cardButton, 0, wxALL, 5);
    }
    for(int i = 0; i < 3 - vector_requested_cards.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_3, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        if(specialPlayer == activePlayer->get_id()){
            cardButton->setInteractionMode(true);
            cardButton->updateInteraction(1);

        }
        urRow_3_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string requestedcardsstring = me->get_player_name();
    wxStaticText* requestedcardstext = new wxStaticText(panel_3, wxID_ANY,  requestedcardsstring, wxDefaultPosition, wxDefaultSize, wxID_ANY);
    requestedcardstext->SetForegroundColour(wxColour("#000000"));

    urRow_3_2->Add( requestedcardstext, 1, wxALIGN_CENTER, 5 );


    urRow_3->Add( urRow_3_1, 1, wxEXPAND, 5 );
    urRow_3->Add( urRow_3_2, 0.1, wxEXPAND, 5 );

    panel_3-> SetSizerAndFit( urRow_3 );
    urRow->Add(panel_3, 1, wxALL, 5);


       //set cards on table
    middle_pile* cardsonTable = tradeState->get_middle_pile();
    std::string empty = "";
    assert(cardsonTable->get_nof_cards() != 0);
#ifdef DEBUG
    std::cout << "debug trading 1" << std::endl;
#endif
    card* middlecard1 = cardsonTable->get_card(0, empty);
#ifdef DEBUG
    std::cout << "debug trading 2" << std::endl;
    std::cout << middlecard1 << std::endl;
#endif
    std::string cardFile1 = "assets/bohn_" + std::to_string(middlecard1->get_bean_type()) + ".png";
#ifdef DEBUG
    std::string cardFile1 = "assets/bohn_" + std::to_string(middlecard1->get_bean_type()) + ".png";
    std::cout << "debug trading 3" << std::endl;
#endif
    ImagePanel *cardButton1 = new ImagePanel(this, cardFile1, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
    cardButton1->SetCursor(wxCursor(wxCURSOR_HAND));
    cardButton1->setInteractionMode(true);
        if(me!=tradeState->get_active_player()) {
            cardButton1->SetToolTip("Request trading this card");
            cardButton1->Bind(wxEVT_LEFT_UP, [me, middlecard1, cardButton1, tradeState](wxMouseEvent &event) {
                cardButton1->updateInteraction(2);
                GameController::select_card_for_trade_base(me, tradeState->get_active_player(), middlecard1);
            });
        }else{
            cardButton1->SetToolTip("Trade with this player");
            cardButton1->Bind(wxEVT_LEFT_UP, [this,tradeState, me,activePlayer](wxMouseEvent &event) {
                specialPlayer = activePlayer->get_id();
                buildTradeState(tradeState, me);
            });
        }
        for(auto i: vector_requested_cards){
            if(i->get_id()==middlecard1->get_id()){
                cardButton1->updateInteraction(2);
            }
        }

    urRow->Add( cardButton1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

    if(cardsonTable->get_nof_cards() > 1) {
        card *middlecard2 = cardsonTable->get_card(1, empty);
        std::string cardFile2 = "assets/bohn_" + std::to_string(middlecard2->get_bean_type()) + ".png";
        ImagePanel *cardButton2 = new ImagePanel(this, cardFile2, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        cardButton2->SetCursor(wxCursor(wxCURSOR_HAND));
        cardButton2->setInteractionMode(true);
        if (me != tradeState->get_active_player()) {
            cardButton2->SetToolTip("Request trading this card");
            cardButton2->Bind(wxEVT_LEFT_UP, [me, middlecard2, cardButton2, tradeState](wxMouseEvent &event) {
                cardButton2->updateInteraction(2);
                GameController::select_card_for_trade_base(me, tradeState->get_active_player(), middlecard2);
            });
        } else {
            cardButton2->SetToolTip("Trade with this player");
            cardButton2->Bind(wxEVT_LEFT_UP, [this,tradeState, me,activePlayer](wxMouseEvent &event) {
                specialPlayer = activePlayer->get_id();
                buildTradeState(tradeState, me);
            });
        }
        for(auto i: vector_requested_cards){
            if(i->get_id()==middlecard2->get_id()){
                cardButton2->updateInteraction(2);
            }
        }
        urRow->Add( cardButton2, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
    }else{
        std::string cardFile_onlyOneTradingCard = "assets/seedstack.png";
        ImagePanel *cardButton_onlyOneTradingCard = new ImagePanel(this, cardFile_onlyOneTradingCard, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow->Add(cardButton_onlyOneTradingCard, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    }




    wxPanel* panel_4 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_4->SetBackgroundColour(wxColor("#b9ca88"));
    wxBoxSizer* urRow_4 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_4_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_4_2 = new wxBoxSizer( wxHORIZONTAL );

    //set offered other cards
    hand* offered_cards_other = tradeState->get_offered_cards(activePlayer, me);
    std::vector<card*> vector_offered_cards_other = offered_cards_other->get_cards();
    for(int i = 0; i < vector_offered_cards_other.size(); ++i) {
        card currentcard = *vector_offered_cards_other[i];
        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_4, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_4_1->Add(cardButton, 0, wxALL, 5);
    }
    for(int i = 0; i < 3 - vector_offered_cards_other.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_4, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51,80));
        urRow_4_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string offeredcardsstringother = activePlayer->get_player_name();
    wxStaticText* offeredcardstextother = new wxStaticText(panel_4, wxID_ANY,  offeredcardsstringother, wxDefaultPosition, wxDefaultSize, wxID_ANY);

    offeredcardstextother->SetForegroundColour(wxColour("#95755e"));
    urRow_4_2->Add( offeredcardstextother, 1, wxALIGN_CENTER, 5 );


    urRow_4->Add( urRow_4_1, 1, wxEXPAND, 5 );
    urRow_4->Add( urRow_4_2, 0.1, wxEXPAND, 5 );


    panel_4-> SetSizerAndFit( urRow_4 );
    urRow->Add(panel_4, 1, wxALL, 5);


    wxPanel* panel_5 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_5->SetBackgroundColour(wxColor("#b9ca88"));
    wxBoxSizer* urRow_5 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_5_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_5_2 = new wxBoxSizer( wxHORIZONTAL );

    //set requested other cards
    hand* requested_cards_other = tradeState->get_requested_cards(activePlayer, me);
    std::vector<card*> vector_requested_cards_other = requested_cards_other->get_cards();
    for(int i = 0; i < vector_requested_cards_other.size(); ++i) {
        card currentcard = *vector_requested_cards_other[i];
        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_5, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51,80));
        urRow_5_1->Add( cardButton, 0, wxALL, 5 );

    }
    for(int i = 0; i < 3 - vector_requested_cards_other.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_5, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51,80));
        urRow_5_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string requestedcardsstringother = activePlayer->get_player_name();
    wxStaticText* requestedcardstextother = new wxStaticText(panel_5, wxID_ANY,  requestedcardsstringother, wxDefaultPosition, wxDefaultSize, wxID_ANY);

    requestedcardstextother->SetForegroundColour(wxColour("#95755e"));
    urRow_5_2->Add( requestedcardstextother, 1, wxALIGN_CENTER, 5 );


    urRow_5->Add( urRow_5_1, 1, wxEXPAND, 5 );
    urRow_5->Add( urRow_5_2, 0.1, wxEXPAND, 5 );

    panel_5-> SetSizerAndFit( urRow_5 );
    urRow->Add(panel_5, 1, wxALL, 5);

    urRow->Add( 0, 0, 1, wxEXPAND, 5 );

    urRow->Add( 35, 0, 0,wxALL,  5 );

    urRow->Add( 0, 0, 1, wxEXPAND, 5 );

    vercticalSizer->Add( urRow, 1, wxEXPAND, 5 );
}

//builds a row where you can only view the trading
void TradingPanel::buildPassivRow(wxBoxSizer* vercticalSizer, trade_state* tradeState,player* me, player* activePlayer, player* player){

    wxBoxSizer *xxplayer;
    xxplayer = new wxBoxSizer(wxHORIZONTAL);

    xxplayer->Add( 0, 0, 1, wxEXPAND, 5 );

    //this button is not touchable, just for nice layout
    wxButton* m_buttonAccept = new wxButton( this, wxID_ANY, wxT("✔"), wxDefaultPosition, wxSize(35,35), 0 );
    xxplayer->Add( m_buttonAccept, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxRESERVE_SPACE_EVEN_IF_HIDDEN, 5 );
    m_buttonAccept->Hide();
    xxplayer->Add( 0, 0, 1, wxEXPAND, 5 );


    wxPanel* panel_2 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_2->SetBackgroundColour(wxColor("#dde5b6"));
    wxBoxSizer* urRow_2 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_2_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_2_2 = new wxBoxSizer( wxHORIZONTAL );
    //set offered cards
    hand* offered_cards = tradeState->get_offered_cards(player, activePlayer);
    std::vector<card*> vector_offered_cards = offered_cards->get_cards();
    for(int i = 0; i < vector_offered_cards.size(); ++i) {
        card currentcard = *vector_offered_cards[i];
        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_2, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));

        urRow_2_1->Add(cardButton, 0, wxALL, 5);
    }
    for(int i = 0; i < 3 - vector_offered_cards.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_2, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_2_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string offeredcardsstring = player->get_player_name();
    wxStaticText* offeredcardstext = new wxStaticText(panel_2, wxID_ANY,  offeredcardsstring, wxDefaultPosition, wxDefaultSize, wxID_ANY);

    offeredcardstext->SetForegroundColour(wxColour("#95755e"));
    urRow_2_2->Add( offeredcardstext, 1, wxALIGN_CENTER, 5 );


    urRow_2->Add( urRow_2_1, 1, wxEXPAND, 5 );
    urRow_2->Add( urRow_2_2, 0.1, wxEXPAND, 5 );

    panel_2-> SetSizerAndFit( urRow_2 );
    xxplayer->Add(panel_2, 1, wxALL, 5);

    auto panel_3 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_3->SetBackgroundColour(wxColor("#dde5b6"));
    auto urRow_3 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_3_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_3_2 = new wxBoxSizer( wxHORIZONTAL );

    //set requested
    hand* requested_cards = tradeState->get_requested_cards(player, activePlayer);
    std::vector<card*> vector_requested_cards = requested_cards->get_cards();
    for(int i = 0; i < vector_requested_cards.size(); ++i) {
        card currentcard = *vector_requested_cards[i];
        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_3, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));

        urRow_3_1->Add(cardButton, 0, wxALL, 5);
    }
    for(int i = 0; i < 3 - vector_requested_cards.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_3, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_3_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string requestedcardsstring = player->get_player_name();
    wxStaticText* requestedcardstext = new wxStaticText(panel_3, wxID_ANY,  requestedcardsstring, wxDefaultPosition, wxDefaultSize, wxID_ANY);

    requestedcardstext->SetForegroundColour(wxColour("#95755e"));
    urRow_3_2->Add( requestedcardstext, 1, wxALIGN_CENTER, 5 );


    urRow_3->Add( urRow_3_1, 1, wxEXPAND, 5 );
    urRow_3->Add( urRow_3_2, 0.1, wxEXPAND, 5 );

    panel_3-> SetSizerAndFit( urRow_3 );
    xxplayer->Add(panel_3, 1, wxALL, 5);

    //set cards on table
    middle_pile* cardsonTable = tradeState->get_middle_pile();
    std::string empty = "";
    assert(cardsonTable->get_nof_cards() != 0);
    card* middlecard1 = cardsonTable->get_card(0, empty);
    //std::cout<<"print out value of middlecard1 :"<<middlecard1->get_bean_type()<<std::endl;
    std::string cardFile1 = "assets/bohn_" + std::to_string(middlecard1->get_bean_type()) + ".png";
    ImagePanel *cardButton1 = new ImagePanel(this, cardFile1, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));

    xxplayer->Add( cardButton1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

    if(cardsonTable->get_nof_cards() > 1) {
        card *middlecard2 = cardsonTable->get_card(1, empty);
        std::string cardFile2 = "assets/bohn_" + std::to_string(middlecard2->get_bean_type()) + ".png";
        ImagePanel *cardButton2 = new ImagePanel(this, cardFile2, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));

        xxplayer->Add(cardButton2, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    }else{
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(this, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        xxplayer->Add(cardButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);
    }


    wxPanel* panel_4 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_4->SetBackgroundColour(wxColor("#dde5b6"));
    wxBoxSizer* urRow_4 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_4_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_4_2 = new wxBoxSizer( wxHORIZONTAL );
    //set offered cards
    hand* offered_cards_other = tradeState->get_offered_cards(activePlayer, player);
    std::vector<card*> vector_offered_cards_others = offered_cards_other->get_cards();
    for(int i = 0; i < vector_offered_cards_others.size(); ++i) {
        card currentcard = *vector_offered_cards_others[i];
        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_4, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_4_1->Add(cardButton, 0, wxALL, 5);
    }
    for(int i = 0; i < 3 - vector_offered_cards_others.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_4, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_4_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string offeredcardsstringother = activePlayer->get_player_name();
    wxStaticText* offeredcardstextother = new wxStaticText(panel_4, wxID_ANY,  offeredcardsstringother, wxDefaultPosition, wxDefaultSize, wxID_ANY);

    offeredcardstextother->SetForegroundColour(wxColour("#95755e"));
    urRow_4_2->Add( offeredcardstextother, 1, wxALIGN_CENTER, 5 );


    urRow_4->Add( urRow_4_1, 1, wxEXPAND, 5 );
    urRow_4->Add( urRow_4_2, 0.1, wxEXPAND, 5 );


    panel_4-> SetSizerAndFit( urRow_4 );
    xxplayer->Add(panel_4, 1, wxALL, 5);


    wxPanel* panel_5 = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(1,1));
    panel_5->SetBackgroundColour(wxColor("#dde5b6"));
    wxBoxSizer* urRow_5 = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer* urRow_5_1 = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer* urRow_5_2 = new wxBoxSizer( wxHORIZONTAL );

    //set requested cards
    hand* requested_cards_other = tradeState->get_requested_cards(activePlayer, player);
    std::vector<card*> vector_requested_cards_others = requested_cards_other->get_cards();
    for(int i = 0; i < vector_requested_cards_others.size(); ++i) {
        card currentcard = *vector_requested_cards_others[i];
        std::string cardFile = "assets/bohn_" + std::to_string(currentcard.get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(panel_5, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_5_1->Add( cardButton, 0, wxALL, 5 );
    }
    for(int i = 0; i < 3 - vector_requested_cards_others.size(); ++i){
        std::string cardFile = "assets/seedstack.png";
        ImagePanel *cardButton = new ImagePanel(panel_5, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(51, 80));
        urRow_5_1->Add(cardButton, 0, wxALL, 5);
    }

    std::string requestedcardsstringother = activePlayer->get_player_name();
    wxStaticText* requestedcardstextother = new wxStaticText(panel_5, wxID_ANY,  requestedcardsstringother, wxDefaultPosition, wxDefaultSize, wxID_ANY);

    requestedcardstextother->SetForegroundColour(wxColour("#95755e"));
    urRow_5_2->Add( requestedcardstextother, 1, wxALIGN_CENTER, 5 );


    urRow_5->Add( urRow_5_1, 1, wxEXPAND, 5 );
    urRow_5->Add( urRow_5_2, 0.1, wxEXPAND, 5 );

    panel_5-> SetSizerAndFit( urRow_5 );
    xxplayer->Add(panel_5, 1, wxALL, 5);

    xxplayer->Add( 0, 0, 1, wxEXPAND, 5 );

    xxplayer->Add( 35, 0, 0,wxALL,  5 );

    xxplayer->Add( 0, 0, 1, wxEXPAND, 5 );

    vercticalSizer->Add(xxplayer, 1, wxEXPAND, 5);

}

//builds the cards on your hands
void TradingPanel::buildUrCards(wxBoxSizer* vercticalSizer, trade_state* tradeState, player* me, player* activePlayer) {
    wxBoxSizer *urCards;
    urCards = new wxBoxSizer(wxHORIZONTAL);

    urCards->Add( 0, 0, 1, wxEXPAND, 5 );

    if (me == activePlayer) {
        wxButton *finish = new wxButton(this, wxID_ANY, wxT("Finish"), wxDefaultPosition, wxSize(70,40), 0);
        urCards->Add(finish, 1, wxALL | wxALIGN_CENTER, 5);
        finish->Bind(wxEVT_BUTTON, [](wxCommandEvent& event) {
            GameController::finishTrade();
        });
        finish->SetBackgroundColour("#b9ca88");
    }else{
        urCards->Add( 70, 0, 1, wxALL, 5 );
    }

    urCards->Add( 0, 0, 1, wxEXPAND, 5 );

    for (int i = 0; i < me->get_hand()->get_cards().size(); i++) {

        card *handCard = me->get_hand()->get_cards().at(i);
        std::string cardFile = "assets/bohn_" + std::to_string(handCard->get_bean_type()) + ".png";

        ImagePanel *cardButton = new ImagePanel(this, cardFile, wxBITMAP_TYPE_ANY, wxDefaultPosition, cardSize);


        cardButton->SetToolTip("Offer card");
        cardButton->SetCursor(wxCursor(wxCURSOR_HAND));
        cardButton->setInteractionMode(true);
        if (me != activePlayer) {
            cardButton->Bind(wxEVT_LEFT_UP, [cardButton, me, activePlayer, handCard](wxMouseEvent &event) {
                cardButton->updateInteraction(2);
                GameController::add_offered_card(me, activePlayer, handCard);
            });
        } else {
            cardButton->Bind(wxEVT_LEFT_UP,
                             [this, cardButton, me, activePlayer, handCard, tradeState](wxMouseEvent &event) {
                                 cardButton->updateInteraction(2);
                                 activateOfferingCard(me, activePlayer, *handCard, tradeState->get_players());
                             });
        }
        if (me != activePlayer) {
            hand *offered_cards = tradeState->get_offered_cards(me, activePlayer);
            std::vector<card *> vector_offered_cards = offered_cards->get_cards();
            for(auto i: vector_offered_cards){
                if(i->get_id()==handCard->get_id()){
                    cardButton->updateInteraction(2);
                }
            }
        }else if(specialPlayer !=""){
            player* dude = nullptr;
            std::vector<player*>::iterator it = std::find_if(tradeState->get_players().begin(), tradeState->get_players().end(), [](const player* x) {
                return x->get_id() == specialPlayer;
            });
            if (it < tradeState->get_players().end()) {
                dude = *it;//muss das so sein??->ist das nicht einf schon me von davor??
            } else {
                GameController::showError("ERROR.", "ERROR");
                return;
            }
            hand *offered_cards = tradeState->get_offered_cards(me, dude);
            std::vector<card *> vector_offered_cards = offered_cards->get_cards();
            for(auto i: vector_offered_cards){
                if(i->get_id()==handCard->get_id()){
                    cardButton->updateInteraction(2);
                }
            }
        }


        urCards->Add(cardButton, 0, wxALL, 4);
    }

    urCards->Add( 0, 0, 1, wxEXPAND, 5 );

    wxButton* close = new wxButton( this, wxID_ANY, wxT("Close"), wxDefaultPosition, wxSize(70,40), 0 );
    close->Bind(wxEVT_BUTTON, [](wxCommandEvent& event) {
        GameController::closeTradingPanel();
    });
    close->SetBackgroundColour("#b9ca88");
    urCards->Add( close, 1, wxALIGN_CENTER|wxALL, 5 );

    urCards->Add( 0, 0, 1, wxEXPAND, 5 );

    vercticalSizer->Add( urCards, 1, wxALIGN_CENTER_HORIZONTAL, 5 );
}




