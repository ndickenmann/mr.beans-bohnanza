#include "init_trade_request.h"

// Public constructor
init_trade_request::init_trade_request(std::string player_id)
        : client_request( client_request::create_base_class_properties(RequestType::init_trade, uuid_generator::generate_uuid_v4(), player_id ))

{ }


// private constructor for deserialization
init_trade_request::init_trade_request(client_request::base_class_properties props)
        : client_request(props)

{ }

void init_trade_request::write_into_json(rapidjson::Value &json,
                                        rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    client_request::write_into_json(json, allocator);

}

init_trade_request* init_trade_request::from_json(const rapidjson::Value& json) {

        return new init_trade_request(client_request::extract_base_class_properties(json));

}

