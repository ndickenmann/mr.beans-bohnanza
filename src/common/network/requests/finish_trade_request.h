#ifndef BOHN_FINISH_TRADE_REQUEST_H
#define BOHN_FINISH_TRADE_REQUEST_H


#include <string>
#include "client_request.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class finish_trade_request : public client_request{

private:

    /*
     * Private constructor for deserialization
     */
    explicit finish_trade_request(base_class_properties);

public:
    finish_trade_request(std::string player_id);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static finish_trade_request* from_json(const rapidjson::Value& json);
};

#endif //BOHN_FINISH_TRADE_REQUEST_H
