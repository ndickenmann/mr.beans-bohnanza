//
// Created by fabian on 23.05.23.
//

#ifndef BOHNANZA_TRADE_STATE_BROADCAST_H
#define BOHNANZA_TRADE_STATE_BROADCAST_H

#include "../server_response.h"
#include "../../../trade_state.h"

class trade_state_broadcast : public server_response {
private:
    rapidjson::Value* _state_json;

    /*
     * Private constructor for deserialization
     */
    trade_state_broadcast(base_class_properties props, rapidjson::Value* state_json);

public:

    trade_state_broadcast(const trade_state& state);
    ~trade_state_broadcast();

    rapidjson::Value* get_state_json() const;

    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static trade_state_broadcast* from_json(const rapidjson::Value& json);

#define BOHN_CLIENT
#ifdef BOHN_CLIENT //TID
    virtual void Process() const override;
#endif
};


#endif //BOHNANZA_TRADE_STATE_BROADCAST_H
