#ifndef LAMAUI_GAMECONTROLLER_H
#define LAMAUI_GAMECONTROLLER_H

#include <vector>
#include "windows/GameWindow.h"
#include "panels/ConnectionPanel.h"
#include "panels/MainGamePanel.h"
#include "panels/TradingPanel.h"
#include "network/ResponseListenerThread.h"
#include "../common/game_state/game_state.h"
#include "../common/game_state/player/field_stack.h"
#include "../common/game_state/cards/card.h"
#include "../common/trade_state.h"


class GameController {

public:
    static void init(GameWindow* gameWindow);

    static void connectToServer();
    static void updateGameState(game_state* newGameState);
    static void startGame();

    static void openTradingPanel();
    static void updateTradeState(trade_state* newTradeState);
    static void closeTradingPanel();

    static void plantCard(std::string card_id);
    static void harvestField(std::string field);
    static void initTrade();
    static void updateTrade(trade_state* trade_state);
    static void finishTrade();
    static void add_offered_card(player* me, player* player, card* card);
    static void add_requested_card(player* me, player* player, card* card);
    static void remove_offered_card(player* me, player* player, card* card);
    static void remove_requested_card(player* me, player* player, card* card);
    static void select_card_for_trade_base(player* me, player* player, card* card);
    static void accept_trade(player* me, player* player, bool state);


    static wxEvtHandler* getMainThreadEventHandler();
    static void showError(const std::string& title, const std::string& message);
    static void showStatus(const std::string& message);
    static void showGameOverMessage();
    static bool getStartedTrading();
    static void setStartedTrading(bool newStartedTrading);

private:
    static GameWindow* _gameWindow;
    static ConnectionPanel* _connectionPanel;
    static MainGamePanel* _mainGamePanel;
    static TradingPanel* _tradingPanel;

    static player* _me;
    static game_state* _currentGameState;
    static trade_state* _currentTradeState;
    static bool startedTrading;
    static bool firstOpenTrading;
};


#endif //LAMAUI_GAMECONTROLLER_H
