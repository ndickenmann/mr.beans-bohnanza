#ifndef LAMA_CLIENT_MAINGAMEPANEL_H
#define LAMA_CLIENT_MAINGAMEPANEL_H

#include <wx/wx.h>
#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/button.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include "../uiElements/ImagePanel.h"
#include "../../common/game_state/game_state.h"

// This class displays the main game panel of the game Bohnanza
// The main framework used for that is wxWidgets which mostly works with sizers
class MainGamePanel : public wxPanel {

public:
    MainGamePanel(wxWindow* parent);
    void buildGameState(game_state* gameState, player* me);

private:
    void buildLoadingScreen(game_state* gameState, player* me);
    void buildLeft(game_state* gameState, player* me);
    void buildFirstRow(game_state* gameState, player* me);
    void buildSecondRow(game_state* gameState, player* me);
    void buildThirdRow(game_state* gameState, player* me);
    void buildFourthRow(game_state* gameState, player* me);
    void buildFifthRow(game_state* gameState, player* me);
    void buildRight(game_state* gameState, player* me);
    void buildOtherPlayer(game_state* gameState, player* me, player* player);
    void addLogic(game_state* gameState, player* me);

    wxStaticText* buildStaticText(std::string content, wxPoint position, wxSize size, long textAlignment);
    wxPanel* CreateSpacer(wxWindow* parent, int width = 10, int height = 10);

    int const normalBorder = 5;
    wxSize const cardSize = wxSize(120, 186);
    wxSize const reducedCardSize = wxSize(60, 93);

    wxBoxSizer* mainSizer;
    wxBoxSizer* left;
    wxBoxSizer* right;

    wxBoxSizer* firstLeft;
    wxBoxSizer* secondLeft;
    wxBoxSizer* thirdLeft;
    wxBoxSizer* fourthLeft;
    wxBoxSizer* fifthLeft;

    wxButton* startGameButton;
    wxButton* tradeButton;
    wxButton* harvestButton;
    wxButton* plantButton;

    ImagePanel* drawPile;
    ImagePanel* secondFaceCard;
    ImagePanel* firstFaceCard;
    ImagePanel* firstSeedStack;
    ImagePanel* secondSeedStack;
    ImagePanel* firstBeanField;
    ImagePanel* secondBeanField;
    std::vector<ImagePanel*> hand;
    ImagePanel* lastClicked = nullptr;
};


#endif //LAMA_CLIENT_MAINGAMEPANEL_H
