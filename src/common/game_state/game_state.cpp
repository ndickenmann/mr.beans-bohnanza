#include "game_state.h"

#include "../exceptions/BohnanzaException.h"
#include "../serialization/vector_utils.h"



game_state::game_state() : unique_serializable() {
    this->_draw_pile = new draw_pile();
    this->_middle_pile = new middle_pile();
    this->_players = std::vector<player*>();
    this->_is_started = new serializable_value<bool>(false);
    this->_is_finished = new serializable_value<bool>(false);
    this->_active_player_id = new serializable_value<int>(0);
    this->_current_phase_id = new serializable_value<int>(0);
}

game_state::game_state(std::string id, 
                        draw_pile *draw_pile,
                        middle_pile *middle_pile,
                        std::vector<player *> &players, 
                        serializable_value<bool> *is_started,
                        serializable_value<bool> *is_finished, 
                        serializable_value<int> *active_player_id,
                        serializable_value<int> *current_phase_id)
        : unique_serializable(id),
          _draw_pile(draw_pile),
          _middle_pile(middle_pile),
          _players(players),
          _is_started(is_started),
          _is_finished(is_finished),
          _active_player_id(active_player_id),
          _current_phase_id(current_phase_id)
{

}

game_state::game_state(std::string id) : unique_serializable(id) {
    this->_draw_pile = new draw_pile();
    this->_middle_pile = new middle_pile();
    this->_players = std::vector<player*>();
    this->_is_started = new serializable_value<bool>(false);
    this->_is_finished = new serializable_value<bool>(false);
    this->_active_player_id = new serializable_value<int>(0);
    this->_current_phase_id = new serializable_value<int>(0);
}

game_state::~game_state() {
    if (_is_started != nullptr) {
        delete _is_started;
        delete _is_finished;
        delete _draw_pile;
        delete _active_player_id;
        delete _current_phase_id;

        _is_started = nullptr;
        _is_finished = nullptr;
        _draw_pile = nullptr;
        _active_player_id = nullptr;
        _current_phase_id = nullptr;
    }
}

// accessors
player* game_state::get_active_player() const {
    if(_active_player_id == nullptr || _players.size() == 0) {
        return nullptr;
    }
    return _players[_active_player_id->get_value()];
}


int game_state::get_phase_id() const{
    return _current_phase_id->get_value();
}


draw_pile *game_state::get_draw_pile() const {
    return _draw_pile;
}

middle_pile *game_state::get_middle_pile() const {
    return _middle_pile;
}

bool game_state::is_full() const {
    return _players.size() == _max_nof_players;
}

bool game_state::is_started() const {
    return _is_started->get_value();
}

bool game_state::is_finished() const {
    return _is_finished->get_value();
}


int game_state::get_player_index(player *player) const {
    auto it = std::find(_players.begin(), _players.end(), player);
    if (it == _players.end()) {
        return -1;
    } else {
        return it - _players.begin();
    }
}

bool game_state::is_player_in_game(player *player) const {
    return std::find(_players.begin(), _players.end(), player) < _players.end();
}

bool game_state::is_allowed_to_play_now(player *player) const {
    if(player == get_active_player()){
        return true;
    }
    return get_phase_id() == 2;
}

std::vector<player*>& game_state::get_players() {
    return _players;
}


//#ifdef LAMA_SERVER

// state modification functions without diff
void game_state::setup_round(std::string &err) {

    // setup draw_pile
    _draw_pile->setup_game(err);
    _middle_pile->reset_middle();


    // setup players
    for (int i = 0; i < _players.size(); i++) {
        _players[i]->setup_round();
        // draw 5 cards
        card* drawn_card = nullptr;
        for (int j = 0; j < 5; j++) {
            if (!_draw_pile->draw(_players[i], drawn_card, err)) {
                std::cerr << err << std::endl;
            }
        }
    }
}

bool game_state::end_game(std::string err) {
    for (player* play : _players){
        play->harvest_beans(0, err);
        play->harvest_beans(1, err);
    }
    _is_finished->set_value(true);

    return 0;
}

// Starting in phase 0, the player can only sell
// (Numbering from 0 to 4 and not from 1 to 5 as in srs)
// last case ends the active players move
bool game_state::update_current_phase(std::string& err) {
    card* left;
    card* right;
    card* drawn_card;
    switch(_current_phase_id->get_value()){
        case 0:  // The first card of the active player is moved to the seed_stack
                // The player can move his second card to the seed_stack if he wants to

            // Ask if he wants to move his second card (not sure how to do this)
            // can be implemented later

            // Check the seed_stack of every player
            for (auto player : _players){
                if (!player->empty_seed_stack()){
                    err = "The seed_stack of" + player->get_id() + "player is not empty";
                    return false;
                }
            }

            // Checking if draw pile is empty and game is finished
            if (_draw_pile->get_nof_cards() <= 1){
                return end_game(err);
            }

            // Adding cards to the middle pile
            left = _draw_pile->remove_top(err);
            right = _draw_pile->remove_top(err);
            if(!_middle_pile->add_two_cards(left, right, err)) return false;

            _current_phase_id->set_value(1);
            return true;

        case 1: // The player has/had cards on his seed_stack and can only continue if his seed_stack is empty
                // If his seed_stack is empty, two cards are moved to the middle_pile and trading is started

            //Checking if the middle_pile is empty
            if(!_middle_pile->is_empty()){
                err = "The middle pile is not empty";
                return false;
            }
            _middle_pile->reset_middle();


            _current_phase_id->set_value(2);
            return true;
            break;
        
        case 2: // The trading should be finished and the middle_pile should be empty

            // Check the seed_stack of every player
            for (auto player : _players){
                if (!player->empty_seed_stack()){
                    err = "The seed_stack of" + player->get_id() + "player is not empty";
                    return false;
                }
            }
            _current_phase_id->set_value(3);
            update_current_phase(err);
            return true;
            break;

        case 3: // All the players may have/had cards on their seed_stack and the phase can only continue if their seed_stack is empty
                // If the stack is empty, the active players turn ends and 3 new cards are added to his hand

                // Checking if draw pile is empty and game is finished
                if (_draw_pile->get_nof_cards() <= 1){
                    return end_game(err);
                }

                // draw 2 cards to the active player
                drawn_card = nullptr;
                for (int j = 0; j < 2; j++) {
                    if (!_draw_pile->draw(_players[_active_player_id->get_value()], drawn_card, err)) {
                        std::cerr << err << std::endl;
                    }
                }

                // Change active player
                this->update_active_player(err);

                // Checking if draw pile is empty and game is finished
                if (_draw_pile->is_empty()){
                    return end_game(err);
                }
                // Moving card to seed_stack
                _players[_active_player_id->get_value()]->play_card(err);
                //_players[_active_player_id->get_value()]->play_card(err);

                _current_phase_id->set_value(0);
                
                // Already updating to phase 1
                /*
                if(!this->update_current_phase(err)){
                    return false;
                }
                 */
                return true;
                break;

        default:
            err = "The phase could not be updated as the phase id is wrong";
            break;
    }

    return 0;
}

bool game_state::update_active_player(std::string& err) {
    int idx = (_active_player_id->get_value() + 1) % _players.size();
    _active_player_id->set_value(idx);
    return true;
}

bool game_state::start_game(std::string &err) {
    if (_players.size() < _min_nof_players) {
        err = "You need at least " + std::to_string(_min_nof_players) + " players to start the game.";
        err += " Currently connected: " + std::to_string((this->get_players()).size());
        return false;
    }

    if (!_is_started->get_value()) {
        this->setup_round(err);
        this->_is_started->set_value(true);
        /*
        if(!this->update_current_phase(err)){
            err = "Could not update the first phase";
            return false;
        }
        */
        // Moving card to seed_stack
        _players[_active_player_id->get_value()]->play_card(err);
        // Add second card for testing:
        //_players[_active_player_id->get_value()]->play_card(err);
        return true;
    } else {
        err = "Could not start game, as the game was already started";
        return false;
    }
}

bool game_state::remove_player(player *player_ptr, std::string &err) {
    int idx = get_player_index(player_ptr);
    if (idx != -1) {
        if (idx < _active_player_id->get_value()) {
            // reduce current_player_idx if the player who left had a lower index
            _active_player_id->set_value(_active_player_id->get_value() - 1);
        }
        _players.erase(_players.begin() + idx);
        return true;
    } else {
        err = "Could not leave game, as the requested player was not found in that game.";
        return false;
    }
}


bool game_state::add_player(player* player_ptr, std::string& err) {
    if (_is_started->get_value()) {
        err = "Could not join game, because the requested game is already started.";
        return false;
    }
    if (_is_finished->get_value()) {
        err = "Could not join game, because the requested game is already finished.";
        return false;
    }
    if (_players.size() >= _max_nof_players) {
        err = "Could not join game, because the max number of players is already reached.";
        return false;
    }
    if (std::find(_players.begin(), _players.end(), player_ptr) != _players.end()) {
        err = "Could not join game, because this player is already subscribed to this game.";
        return false;
    }

    _players.push_back(player_ptr);
    return true;
}


// The player takes a card from the middle. Drawing cards from the draw pile is done automatic
// The drawn card lands on the seed_stack
// 
bool game_state::draw_card(player *player, int middle_card, std::string &err) {
    if (!is_player_in_game(player)) {
        err = "Server refused to perform draw_card. Player is not part of the game.";
        return false;
    }
    if (!is_allowed_to_play_now(player)) {
        err = "It's not this players turn yet.";
        return false;
    }
    if (_middle_pile->is_empty()) {
        err = "Middle pile is empty. Cannot draw a card.";
        return false;
    }
    if (_is_finished->get_value()) {
        err = "Could not draw card, because the requested game is already finished.";
        return false;
    }

    // Removing card from middle
    card* drawn_card = _middle_pile->remove_card(middle_card, err);
    
    // Adding card to seed_stack
    return !_players[_active_player_id->get_value()]->take_card(drawn_card, err);
    
}

// The player can put card the card from his hand to the seed_stack
bool game_state::play_card(player *player, card*& card, std::string &err) {
    if (!is_player_in_game(player)) {
        err = "Server refused to perform draw_card. Player is not part of the game.";
        return false;
    }
    if (_is_finished->get_value()) {
        err = "Could not play card, because the requested game is already finished.";
        return false;
    }
    return player->take_card(card, err);
}

// The player can plant a card from the seed_stack onto one of his field stacks
bool game_state::plant_card(player* player, const std::string& card_id, std::string& err){
    std::cout << "game_state@plant_card :> accessed" << std::endl;
    if (!is_player_in_game(player)) {
        err = "Server refused to perform plant_card. Player is not part of the game.";
        return false;
    }
    if (!is_allowed_to_play_now(player)) {
        err = "It's not this players turn yet.";
        return false;
    }
    if (_is_finished->get_value()) {
        err = "Could not play card, because the requested game is already finished.";
        return false;
    }
    std::cout << "game_state@plant_card :> planting card in game state" << std::endl;
    return player->plant_bean(card_id, err);
}

// Tries to harvest the field_stack given by the variable stack
bool game_state::harvest_field(player* player, const int stack, std::string& err){
    if (!is_player_in_game(player)) {
        err = "Server refused to perform harvest_field. Player is not part of the game.";
        return false;
    }
    if (!is_allowed_to_play_now(player)) {
        err = "It's not this players turn yet.";
        return false;
    }
    std::cout << "game_state.cpp@harvest_field: end of function" << std::endl;
    return player->harvest_beans(stack, err);

}

//#endif


// Serializable interface
void game_state::write_into_json(rapidjson::Value &json,
                                 rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    unique_serializable::write_into_json(json, allocator);

    rapidjson::Value is_finished_val(rapidjson::kObjectType);
    _is_finished->write_into_json(is_finished_val, allocator);
    json.AddMember("is_finished", is_finished_val, allocator);

    rapidjson::Value is_started_val(rapidjson::kObjectType);
    _is_started->write_into_json(is_started_val, allocator);
    json.AddMember("is_started", is_started_val, allocator);

    rapidjson::Value current_player_idx_val(rapidjson::kObjectType);
    _active_player_id->write_into_json(current_player_idx_val, allocator);
    json.AddMember("active_player", current_player_idx_val, allocator);

    rapidjson::Value draw_pile_val(rapidjson::kObjectType);
    _draw_pile->write_into_json(draw_pile_val, allocator);
    json.AddMember("draw_pile", draw_pile_val, allocator);

    rapidjson::Value middle_pile_val(rapidjson::kObjectType);
    _middle_pile->write_into_json(middle_pile_val, allocator);
    json.AddMember("middle_pile", middle_pile_val, allocator);

    rapidjson::Value current_phase_val(rapidjson::kObjectType);
    _current_phase_id->write_into_json(current_phase_val, allocator);
    json.AddMember("current_phase", current_phase_val, allocator);

    json.AddMember("players", vector_utils::serialize_vector(_players, allocator), allocator);


}


game_state* game_state::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id")
        && json.HasMember("is_finished")
        && json.HasMember("is_started")
        && json.HasMember("active_player")
        && json.HasMember("current_phase")
        && json.HasMember("players")
        && json.HasMember("draw_pile")
        && json.HasMember("middle_pile"))
    {
        std::vector<player*> deserialized_players;
        for (auto &serialized_player : json["players"].GetArray()) {
            deserialized_players.push_back(player::from_json(serialized_player.GetObject()));
        }
        return new game_state(json["id"].GetString(),
                              draw_pile::from_json(json["draw_pile"].GetObject()),
                              middle_pile::from_json(json["middle_pile"].GetObject()),
                              deserialized_players,
                              serializable_value<bool>::from_json(json["is_started"].GetObject()),
                              serializable_value<bool>::from_json(json["is_finished"].GetObject()),
                              serializable_value<int>::from_json(json["active_player"].GetObject()),
                              serializable_value<int>::from_json(json["current_phase"].GetObject()));


    } else {
        throw BohnanzaException("Failed to deserialize game_state. Required entries were missing.");
    }
}
