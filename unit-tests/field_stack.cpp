#include "gtest/gtest.h"
#include "../src/common/game_state/player/field_stack.h"
#include "../src/common/game_state/cards/card.h"

// helper class

class FieldTest : public::testing::Test{
protected:
    virtual void SetUp() {
        cards.resize(8);
        for (int i = 1; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                cards[i].push_back(new card(i));
            }
        }
    }

    std::vector<std::vector<card*>> cards;
    field_stack bean_field;
    std::string err;
};




// Testing the plant_bean and the get_count functions
TEST_F(FieldTest, AddingCards){

    EXPECT_TRUE(bean_field.plant_bean(cards[1][0], err));

    EXPECT_EQ(1,bean_field.get_count());

    EXPECT_TRUE(bean_field.plant_bean(cards[2][0], err));
    EXPECT_TRUE(bean_field.plant_bean(cards[3][0], err));
    EXPECT_TRUE(bean_field.plant_bean(cards[7][0], err));


    EXPECT_EQ(4,bean_field.get_count());

}

// Testing if the can_plant function works
TEST_F(FieldTest, CanPlant){
    std::string id = "0";
    std::string second_id = "1";
    std::string third_id = "2";
    serializable_value<int> i = 1;
    serializable_value<int>* p = &i;
    card* c_1 = new card(id, p);
    card* c_15 = new card(second_id, p);
    card* c_2 = new card(2);

    EXPECT_TRUE(bean_field.can_plant(id, c_1));

    bean_field.plant_bean(c_1, id);
    EXPECT_EQ(1,bean_field.get_count());

    EXPECT_FALSE(bean_field.can_plant(id, c_1));
    EXPECT_TRUE(bean_field.can_plant(second_id, c_1));
    EXPECT_FALSE(bean_field.can_plant(third_id, c_2));
}

TEST_F(FieldTest, RemoveReset){
    std::string id = "0";
    std::string second_id = "1";
    serializable_value<int> i = 1;
    serializable_value<int>* p = &i;
    card* c_1 = new card(id, p);
    card* c_15 = new card(second_id, p);

    EXPECT_TRUE(bean_field.plant_bean(c_1, err));
    EXPECT_TRUE(bean_field.plant_bean(c_15, err));

    EXPECT_EQ(2,bean_field.get_count());

    // Removing the same card two times should not work
    EXPECT_TRUE(bean_field.remove_card(second_id, c_15, id));
    EXPECT_FALSE(bean_field.remove_card(second_id, c_15, id));

    EXPECT_EQ(1,bean_field.get_count());

    bean_field.setup_stack();

    EXPECT_EQ(0,bean_field.get_count());

}

TEST_F(FieldTest, harvestBeans){
    std::string id = "1";
    std::string second_id = "2";
    serializable_value<int> i = 1;
    serializable_value<int>* p = &i;
    card* c_11 = new card(id, p);
    card* c_12 = new card(second_id, p);
    card* c_13 = new card("3", p);

    EXPECT_TRUE(bean_field.plant_bean(c_11, err));
    EXPECT_EQ(0, bean_field.harvest_beans());
    
    EXPECT_TRUE(bean_field.plant_bean(c_12, err));
    EXPECT_TRUE(bean_field.plant_bean(c_13, err));

    EXPECT_EQ(1, bean_field.harvest_beans());

    // This only works because the stack is reset in the player function
    // and not immediatly in harvest_beans
    card* c_14 = new card("4", p);
    EXPECT_TRUE(bean_field.plant_bean(c_14, err));

    EXPECT_EQ(2, bean_field.harvest_beans());

    bean_field.setup_stack();
    EXPECT_EQ(0, bean_field.harvest_beans());
}

