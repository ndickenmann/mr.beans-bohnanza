#include "game_instance.h"

#include "../common/network/responses/broadcasts/game_state_broadcast.h"
#include "../common/network/responses/broadcasts/trade_state_broadcast.h"
#include "server_network_manager.h"


game_instance::game_instance() {
    _game_state = new game_state();
}

game_state *game_instance::get_game_state() {
    return _game_state;
}

std::string game_instance::get_id() {
    return _game_state->get_id();
}

bool game_instance::is_player_allowed_to_play(player *player) {
    return _game_state->is_allowed_to_play_now(player);
}

bool game_instance::is_full() {
    return _game_state->is_full();
}

bool game_instance::is_started() {
    return _game_state->is_started();
}

bool game_instance::is_finished() {
    return _game_state->is_finished();
}


bool game_instance::plant_card(player *player, const std::string& card_id, std::string& err) {
    modification_lock.lock();
    // we have different functions for planting, taking cards from the middle and one for adding the cards from
    // trading to the seed stack
    // everything documented in game_state
    if(_game_state->plant_card(player, card_id, err)) {
        if(_game_state->get_phase_id() == 0 && _game_state->get_active_player()->empty_seed_stack()){
            _game_state->update_current_phase(err);
        }
        if(_game_state->get_phase_id() == 2) {
            _game_state->update_current_phase(err);
        }
        game_state_broadcast state_update_msg = game_state_broadcast(*_game_state);
        server_network_manager::broadcast_message(state_update_msg, _game_state->get_players(), player);


        modification_lock.unlock();
        return true;
    }
    #ifdef DEBUG
    std::cout << "game_instance.ccp@plant_card: plant_card of game state returned false";
    #endif

    modification_lock.unlock();
    return false;
}

bool game_instance::harvest_field(player *player,const std::string& field_id, std::string& err) {
    modification_lock.lock();
    int field_id_int = 0; 
    if (field_id=="1")
        field_id_int=1;
    if (_game_state->harvest_field(player,field_id_int, err)) {
        std::cout << "game_state.cpp@harvest_field: _game_state->harvest_field returned true" << std::endl;
        game_state_broadcast state_update_msg = game_state_broadcast(*_game_state);
        std::cout << "game_state.cpp@harvest_field: state update message has been generated" << std::endl;
        server_network_manager::broadcast_message(state_update_msg, _game_state->get_players(), player);
        std::cout << "game_state.cpp@harvest_field: message has been broadcasted" << std::endl;
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;

}

bool game_instance::update_trade(player *player, trade_state* _trade_state, std::string& err) {
    modification_lock.lock();
    _trade_handler = get_trade_handler();
    if (_trade_handler != nullptr && _trade_handler->update_trade(_trade_state)) {
        #ifdef DEBUG
        std::cout << "game_instance@update_trade :> pre game state ptr: " << _game_state << std::endl;
        #endif
        _game_state = _trade_handler->get_trade_state()->get_game_state();
        #ifdef DEBUG
        std::cout << "game_instance@update_trade :> post game state ptr: " << _game_state << std::endl;
        #endif
        trade_state_broadcast update_trade_msg = trade_state_broadcast(*(_trade_handler->get_trade_state()));
        server_network_manager::broadcast_message(update_trade_msg, _game_state->get_players(), player);// Check if player should be excluded
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::init_trade(player *player, std::string& err) {
    #ifdef DEBUG
    std::cout << "gameinstance@ init trade" << std::endl;
    #endif
    modification_lock.lock();

    init_trade_handler();
    if(_trade_handler != nullptr) {
#ifdef DEBUG
        std::cout << "generate init trade response with trade state" << std::endl;
#endif
        _trade_handler->start_trade(_game_state);
#ifdef DEBUG
        std::cout << "started trade in trade_handler (called in game_instace)" << std::endl;
#endif
        trade_state_broadcast update_trade_msg = trade_state_broadcast(*(_trade_handler->get_trade_state()));
#ifdef DEBUG
        std::cout << "created trade_state_broadcast message" << std::endl;
#endif
        server_network_manager::broadcast_message(update_trade_msg, _game_state->get_players(), player);
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::finish_trade(player *player, std::string& err) {


    modification_lock.lock();

#ifdef DEBUG
    std::cout << "game_instance.cpp@finish_trade: entered function" << std::endl;
#endif
    _trade_handler = get_trade_handler();
#ifdef DEBUG
    std::cout << "game_instance.cpp@finish_trade: got trade handler" << std::endl;
#endif
    if(_trade_handler != nullptr && _trade_handler->finish_trade()) {
        _game_state = _trade_handler->get_trade_state()->get_game_state();
#ifdef DEBUG
        std::cout << "game_instance.cpp@finish_trade: entered if" << std::endl;
#endif
        trade_state_broadcast update_trade_msg = trade_state_broadcast(*(_trade_handler->get_trade_state()));
#ifdef DEBUG
        std::cout << "game_instance.cpp@finish_trade: got update_trade_msg" << std::endl;
#endif
        server_network_manager::broadcast_message(update_trade_msg, _game_state->get_players(), player);
#ifdef DEBUG
        std::cout << "game_instance.cpp@finish_trade:  broadcasted message" << std::endl;
#endif
        modification_lock.unlock();
#ifdef DEBUG
        std::cout << "game_instance.cpp@finish_trade:  return true" << std::endl;
#endif
        return true;
    }
    modification_lock.unlock();
#ifdef DEBUG
    std::cout << "game_instance.cpp@finish_trade:  return false" << std::endl;
#endif
    return false;
}

bool game_instance::start_game(player* player, std::string &err) {
    modification_lock.lock();
    if (_game_state->start_game(err)) {
        // send state update to all other players
        game_state_broadcast state_update_msg = game_state_broadcast(*_game_state);
        server_network_manager::broadcast_message(state_update_msg, _game_state->get_players(), player);
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::try_remove_player(player *player, std::string &err) {
    modification_lock.lock();
    if (_game_state->remove_player(player, err)) {
        player->set_game_id("");
        // send state update to all other players
        game_state_broadcast state_update_msg = game_state_broadcast(*_game_state);
        server_network_manager::broadcast_message(state_update_msg, _game_state->get_players(), player);
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::try_add_player(player *new_player, std::string &err) {
    modification_lock.lock();
    if (_game_state->add_player(new_player, err)) {
        new_player->set_game_id(get_id());
        // send state update to all other players
        game_state_broadcast state_update_msg = game_state_broadcast(*_game_state);
        server_network_manager::broadcast_message(state_update_msg, _game_state->get_players(), new_player);
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

trade_handler* game_instance::get_trade_handler() {
    return this->_trade_handler;
}

bool game_instance::init_trade_handler(){
    if(this->_trade_handler == nullptr) {
        this->_trade_handler = new trade_handler();
    }
    return true;
}
