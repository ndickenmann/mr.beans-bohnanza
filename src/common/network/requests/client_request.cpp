
#include "client_request.h"
#include "finish_trade_request.h"
#include "init_trade_request.h"
#include "update_trade_request.h"
#include "harvest_field_request.h"
#include "plant_card_request.h"
#include "join_game_request.h"
#include "start_game_request.h"

#include <iostream>

// for deserialization
const std::unordered_map<std::string, RequestType> client_request::_string_to_request_type = {
        {"join_game", RequestType::join_game },
        {"start_game", RequestType::start_game},
        {"plant_card", RequestType::plant_card},
        {"harvest_field", RequestType::harvest_field},
        {"update_trade", RequestType::update_trade},
        {"init_trade", RequestType::init_trade},
        {"finish_trade", RequestType::finish_trade},
};
// for serialization
const std::unordered_map<RequestType, std::string> client_request::_request_type_to_string = {
        
        { RequestType::join_game, "join_game" },
        { RequestType::start_game, "start_game"},
        { RequestType::plant_card, "plant_card"},
        { RequestType::harvest_field, "harvest_field"},
        { RequestType::update_trade, "update_trade"},
        { RequestType::init_trade, "init_trade"},
        { RequestType::finish_trade, "finish_trade"},
};

// protected constructor. only used by subclasses
client_request::client_request(client_request::base_class_properties props) :
        _type(props._type),
        _req_id(props._req_id),
        _player_id(props._player_id)
{ }


// used by subclasses to retrieve information from the json stored by this superclass
client_request::base_class_properties client_request::extract_base_class_properties(const rapidjson::Value& json) {
    if (json.HasMember("player_id") && json.HasMember("req_id")) {
        std::string player_id = json["player_id"].GetString();
        std::string req_id = json["req_id"].GetString();
        return create_base_class_properties(
                client_request::_string_to_request_type.at(json["type"].GetString()),
                req_id,
                player_id
        );
    }
    else
    {
        throw BohnanzaException("Client Request did not contain player_id");
    }
}

client_request::base_class_properties client_request::create_base_class_properties(
        RequestType type,
        std::string req_id,
        std::string& player_id)
{
    client_request::base_class_properties res;
    res._player_id = player_id;
    res._req_id = req_id;
    res._type = type;
    return res;
}


void client_request::write_into_json(rapidjson::Value &json,
                                     rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    rapidjson::Value type_val(_request_type_to_string.at(this->_type).c_str(), allocator);
    json.AddMember("type", type_val, allocator);

    rapidjson::Value player_id_val(_player_id.c_str(), allocator);
    json.AddMember("player_id", player_id_val, allocator);

    rapidjson::Value req_id_val(_req_id.c_str(), allocator);
    json.AddMember("req_id", req_id_val, allocator);
}

client_request* client_request::from_json(const rapidjson::Value &json) {
    if (json.HasMember("type") && json["type"].IsString()) {
        const std::string type = json["type"].GetString();
        const RequestType request_type = client_request::_string_to_request_type.at(type);

        // Check which type of request it is and call the respective from_json constructor
        if (request_type == RequestType::plant_card) {
            return plant_card_request::from_json(json);
        }
        else if (request_type == RequestType::harvest_field) {
            return harvest_field_request::from_json(json);
        }
        else if (request_type == RequestType::update_trade) {
            return update_trade_request::from_json(json);
        }
        else if (request_type == RequestType::init_trade) {
            return init_trade_request::from_json(json);
        }
        else if (request_type == RequestType::finish_trade) {
            return finish_trade_request::from_json(json);
        }
        else if (request_type == RequestType::join_game) {
            return join_game_request::from_json(json);
        }
        else if (request_type == RequestType::start_game) {
            return start_game_request::from_json(json);
        } else {
            throw BohnanzaException("Encountered unknown ClientRequest type " + type);
        }
    }
    throw BohnanzaException("Could not determine type of ClientRequest. JSON was:\n" + json_utils::to_string(&json));
}


std::string client_request::to_string() const {
    return "client_request of type " + client_request::_request_type_to_string.at(_type) + " for playerId " + _player_id;
}






