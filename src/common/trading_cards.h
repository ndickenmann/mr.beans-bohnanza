//
// Created by widizzi on 18/05/23.
//

#ifndef TRADING_CARDS_H
#define TRADING_CARDS_H

#include <string>
#include <map>
#include "game_state/player/player.h"

#include "../../rapidjson/include/rapidjson/document.h"
#include "serialization/serializable.h"
#include "serialization/unique_serializable.h"
#include "serialization/serializable_value.h"

typedef std::map<std::pair<std::string, std::string>, std::pair<std::vector<hand*>, serializable_value<bool>>> card_map;

class trading_cards : public unique_serializable {
private:
    card_map* storage;

    /* deserialization constructor */
    trading_cards(std::string id, card_map& objects);

public:
    // constructors
    trading_cards();
    trading_cards(std::vector<player*>& players, player* active_player);

    // accessors
    card_map* get_cards();

    // serialization
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static trading_cards* from_json(const rapidjson::Value& json);

};


#endif // TRADING_CARDS_H
