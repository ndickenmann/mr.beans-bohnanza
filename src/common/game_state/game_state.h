#ifndef BOHNANZA_GAME_STATE_H
#define BOHNANZA_GAME_STATE_H

#include <vector>
#include <string>
#include "../../../rapidjson/include/rapidjson/document.h"
#include "player/player.h"
#include "cards/draw_pile.h"
#include "cards/middle_pile.h"
#include "../serialization/serializable.h"
#include "../serialization/serializable_value.h"
#include "../serialization/unique_serializable.h"

class game_state : public unique_serializable {
private:

    static const int _max_nof_players = 5;
    static const int _min_nof_players = 3;

    std::vector<player*> _players;
    draw_pile* _draw_pile;
    middle_pile* _middle_pile;
    serializable_value<bool>* _is_started;
    serializable_value<bool>* _is_finished;
    serializable_value<int>* _active_player_id;
    serializable_value<int>* _current_phase_id;
    
    // from_diff constructor
    game_state(std::string id);

    // deserialization constructor
    game_state(
            std::string id,
            draw_pile* draw_pile,
            middle_pile* middle_pile,
            std::vector<player*>& players,
            serializable_value<bool>* is_started,
            serializable_value<bool>* is_finished,
            serializable_value<int>* active_player_id,
            serializable_value<int>* current_phase_id);

    // returns the index of 'player' in the '_players' vector
    int get_player_index(player* player) const;

public:
    game_state();
    ~game_state();

// accessors
    bool is_full() const;
    bool is_started() const;
    bool is_finished() const;
    bool is_player_in_game(player* player) const;
    bool is_allowed_to_play_now(player* player) const;

    std::vector<player*>& get_players();
    draw_pile* get_draw_pile() const;
    middle_pile* get_middle_pile() const;
    player* get_active_player() const;
    int get_phase_id() const;

    //#ifdef LAMA_SERVER
// server-side state update functions
    void setup_round(std::string& err);   // server side initialization
    bool remove_player(player* player, std::string& err);
    bool add_player(player* player, std::string& err);
    bool start_game(std::string& err);

    // Harvest fields of every player and sets is_finished true
    bool end_game(std::string err);

    // The requested card from the middle (int middle_card, 0 is left, 1 is right) is moved to the seed_stack
    bool draw_card(player* player, int middle_card, std::string& err);

    // The card is moved to the seed_stack
    // Works for all players (can be used for all cards gained in trading)
    bool play_card(player* player, card*& card, std::string& err);
    // The card is planted from the seed_stack to one of the field_stacks
    bool plant_card(player* player, const std::string& card_id, std::string& err);
    // The requested field_stack (int stack, 0 is left, 1 is right) is harvested and the gained coins
    // are added to the player
    bool harvest_field(player* player, const int stack, std::string& err);


    // end of round/phase functions
    bool update_active_player(std::string& err);
    bool update_current_phase(std::string& err);

    //#endif

// serializable interface
    static game_state* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

};


#endif //BOHNANZA_GAME_STATE_H