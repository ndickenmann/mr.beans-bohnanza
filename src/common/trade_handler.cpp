#include "trade_handler.h"
#include <utility>
#include <map>

/*
 * the trade handler implements the logic for executing trades. It is held as a single instance in the game_instance.
 * the trade_handler itself holds a single instance of the trade_state each time a trade gets initialized. It can check
 * if a trade matches and then manipulate the game state to save the newly shifted cards.
 */


trade_handler::trade_handler() {
    this->_trade_state = nullptr;
}

bool trade_handler::start_trade(game_state* game_state) {

    this->_trade_state = new trade_state(game_state);
    return true;
}


/*
 * finishing a trade automatically moves all cards left in the middlepile into the seedstacks of the active player
 * then it sets the flag finished to inform clients to read the new game state and close the trading panel
 */
bool trade_handler::finish_trade() {
    // move middle piles into seed stacks of active player
    std::string err = "";
    while(_game_state->get_middle_pile()->is_empty() != true) {
        auto card = _game_state->get_middle_pile()->get_card(0, err);
        auto seedstacks = _game_state->get_active_player()->get_seed_stacks();
        assert(seedstacks.size() == 2);
        if(seedstacks[0]->get_stack_bean_type() == card->get_bean_type()) {
            std::cout << "trade_handler@finish_trade :> put card on seed stack 0 due to matching type" << std::endl;
            seedstacks[0]->add_card(card, err);
            _game_state->get_middle_pile()->remove_card(0, err);
        }
        else if(seedstacks[1]->get_stack_bean_type() == card->get_bean_type()){
            std::cout << "trade_handler@finish_trade :> put card on seed stack 1 due to matching type" << std::endl;
            seedstacks[1]->add_card(card, err);
            _game_state->get_middle_pile()->remove_card(0, err);
        }
        else if(seedstacks[0]->get_stack_bean_type() == -1){
            std::cout << "trade_handler@finish_trade :> put card on seed stack 0 due to empty field" << std::endl;
            seedstacks[0]->add_card(card, err);
            _game_state->get_middle_pile()->remove_card(0, err);
        }
        else if(seedstacks[1]->get_stack_bean_type() == -1){
            std::cout << "trade_handler@finish_trade :> put card on seed stack 1 due to empty field" << std::endl;
            seedstacks[1]->add_card(card, err);
            _game_state->get_middle_pile()->remove_card(0, err);
        }
        else{
            std::cout << "trade_handler@finish_trade :> err no matching seed stack found" << std::endl;
            return false;
        }
    }
    this->_trade_state->set_finished(serializable_value<bool>(true));
    _game_state->update_current_phase(err);
    std::cout << "trade_handler@finish_trade :> successfully finished trade" << std::endl;
    return true;
}

bool trade_handler::update_trade(trade_state* trade_state) {
    this->_trade_state = trade_state;
    return check_accept(this->_trade_state);
}

/*
 * that a trade is executed the offered and requested cards need to be checked to match. This includes
 * checking if middle pile cards are included in this specific trade. when a trade is valid it can be
 * executed via the execute_trade function
 */
bool trade_handler::check_accept(trade_state* trade_state) {
    std::string err = "";
    /* checks the trade_state accepted vector and    trade_state->get executes the trade when neccessary */
    auto trading_cards = trade_state->get_trading_cards()->get_cards();
    auto players = trade_state->get_players();
    auto active_player = trade_state->get_active_player();
    std::cout << "trade_handler@check_accept :> started checking players accept status" << std::endl;
    for(int i = 0; i < players.size(); ++i) {
        for(int j = 0; j < players.size(); ++j) {
            if(i != j && (players.at(i) == active_player || players.at(j) == active_player)) {
                auto pair = std::make_pair(players.at(i)->get_id(), players.at(j)->get_id());
                auto pair_reversed = std::make_pair(players.at(j)->get_id(), players.at(i)->get_id());
                std::cout << "trade_handler@check_accept :> value from pair: " << trading_cards->at(pair).second.get_value() << std::endl;
                std::cout << "trade_handler@check_accept :> value from reversed pair: " << trading_cards->at(pair_reversed).second.get_value() << std::endl;
                if(trading_cards->at(pair).second.get_value() && trading_cards->at(pair_reversed).second.get_value()) {

                    /*
                     * comparing the requested and offered cards to match the trade
                     * if they don't match the trade can not be executed
                     */
                    std::vector<int> offered_cards_pl_one;
                    for(auto card : trading_cards->at(pair).first[0]->get_cards()) {offered_cards_pl_one.push_back(card->get_bean_type()); std::cout << "bean type opl 1: " << card->get_bean_type() << std::endl;}

                    std::vector<int> requested_cards_pl_one;
                    for(auto card : trading_cards->at(pair).first[1]->get_cards()) {
                        requested_cards_pl_one.push_back(card->get_bean_type()); std::cout << "bean type rpl 1: " << card->get_bean_type() << std::endl;
                        for(int q = 0; q < trade_state->get_middle_pile()->get_nof_cards(); ++q) {
                            auto middle_card = trade_state->get_middle_pile()->get_card(q, err);
                            if(card->get_id() == middle_card->get_id()) {requested_cards_pl_one.pop_back();}
                        }
                    }

                    std::vector<int> offered_cards_pl_two;
                    for(auto card : trading_cards->at(pair_reversed).first[0]->get_cards()) {offered_cards_pl_two.push_back(card->get_bean_type()); std::cout << "bean type opl 2: " << card->get_bean_type() << std::endl;}

                    std::vector<int> requested_cards_pl_two;
                    for(auto card : trading_cards->at(pair_reversed).first[1]->get_cards()) {
                        requested_cards_pl_two.push_back(card->get_bean_type()); std::cout << "bean type rpl 2: " << card->get_bean_type() << std::endl;
                        for(int q = 0; q < trade_state->get_middle_pile()->get_nof_cards(); ++q) {
                            auto middle_card = trade_state->get_middle_pile()->get_card(q, err);
                            if(card->get_id() == middle_card->get_id()) {requested_cards_pl_two.pop_back();}
                        }
                    }

                    std::sort(offered_cards_pl_one.begin(), offered_cards_pl_one.end());
                    std::sort(requested_cards_pl_one.begin(), requested_cards_pl_one.end());
                    std::sort(offered_cards_pl_two.begin(), offered_cards_pl_two.end());
                    std::sort(requested_cards_pl_two.begin(), requested_cards_pl_two.end());


                    if(offered_cards_pl_one == requested_cards_pl_two && offered_cards_pl_two == requested_cards_pl_one) {
                        std::cout << "trade_handler@check_accept :> executing trade" << std::endl;
                        execute_trade(trade_state, players[i], players[j]);
                        std::cout << "trade_handler@check_accept :> clearing accepts" << std::endl;
                        trading_cards->at(pair).second = trading_cards->at(pair_reversed).second = serializable_value<bool>(false);
                    }
                }
            }
        }
    }
    std::cout << "trade_handler@check_accept :> returning from check accept" << std::endl;
    return true;
}

/*
 * this function manipulates the game state by applying the changes which are wanted to be made in the trade
 * it assumes that the trade is valid
 */
bool trade_handler::execute_trade(trade_state* trade_state, player* player_one, player* player_two) {
    assert(player_one != nullptr && player_two != nullptr);
    std::string err = "";

    std::cout << "trade_handler::execute_trade :> getting active player" << std::endl;
    /* getting active player and his partner form parameters */
    player* active_player = nullptr;
    player* trade_partner = nullptr;
    if(trade_state->get_active_player() == player_one) {
        active_player = player_one;
        trade_partner = player_two;
    } else if(trade_state->get_active_player() == player_two) {
        active_player = player_two;
        trade_partner = player_one;
    } else {
        std::cout << "trade_handler@execute_trade :> ERROR active player not found in trading partners" << std::endl;
        assert(false);
    }

    std::cout << "trade_handler::execute_trade :> getting requested cards" << std::endl;
    auto requested_cards_active_player = trade_state->get_requested_cards(active_player, trade_partner);
    auto requested_cards_trade_partner = trade_state->get_requested_cards(trade_partner, active_player);

    std::cout << "trade_handler::execute_trade :> doin trade magic for middle card" << std::endl;
    /* remove from trading partner requested middle pile cards from the middle pile and add them to the seed stack of the trading partner */
    auto middle_pile = trade_state->get_game_state()->get_middle_pile();

    for(auto card : requested_cards_trade_partner->get_cards()) {
        for(int i = 0; i < middle_pile->get_nof_cards(); ++i) {
            auto middle_card = middle_pile->get_card(i, err);
            if(card->get_id() == middle_card->get_id()) {

                auto seedstacks = trade_partner->get_seed_stacks();
                assert(seedstacks.size() == 2);
                if(seedstacks[0]->get_stack_bean_type() == middle_card->get_bean_type()) {
                    std::cout << "trade_handler@execute_trade :> put card on seed stack 0 due to matching type" << std::endl;
                    seedstacks[0]->add_card(middle_card, err);
                }
                else if(seedstacks[1]->get_stack_bean_type() == middle_card->get_bean_type()){
                    std::cout << "trade_handler@execute_trade :> put card on seed stack 1 due to matching type" << std::endl;
                    seedstacks[1]->add_card(middle_card, err);
                }
                else if(seedstacks[0]->get_stack_bean_type() == -1){
                    std::cout << "trade_handler@execute_trade :> put card on seed stack 0 due to empty field" << std::endl;
                    seedstacks[0]->add_card(middle_card, err);
                }
                else if(seedstacks[1]->get_stack_bean_type() == -1){
                    std::cout << "trade_handler@execute_trade :> put card on seed stack 1 due to empty field" << std::endl;
                    seedstacks[1]->add_card(middle_card, err);
                }
                else{
                    std::cout << "trade_handler@execute_trade :> err no matching seed stack found" << std::endl;
                    return false;
                }
                middle_pile->remove_card(i, err);
                --i;
            }
        }
    }

    std::cout << "trade_handler::execute_trade :> clearing requested hands" << std::endl;
    /* clear requested cards from involved players */
    requested_cards_active_player->clear_hand();
    requested_cards_trade_partner->clear_hand();

    /*
     * swap requested and offered cards into seed stacks of corresponding players
     * this part also removed the cards from the offered cards and from the hands
     */
    std::cout << "trade_handler::execute_trade :> getting offered cards" << std::endl;
    auto offered_cards_player_one = trade_state->get_offered_cards(player_one, player_two);
    auto offered_cards_player_two = trade_state->get_offered_cards(player_two, player_one);

    std::cout << "trade_handler::execute_trade :> doin trade magic for requested and offerd cards for player one" << std::endl;
    while(offered_cards_player_one->get_nof_cards() != 0) {
        auto card = offered_cards_player_one->get_first_card();
        auto seedstacks = player_two->get_seed_stacks();
        assert(seedstacks.size() == 2);
        if(seedstacks[0]->get_stack_bean_type() == card->get_bean_type()) {
            std::cout << "trade_handler@execute_trade :> put card on seed stack 0 due to matching type" << std::endl;
            seedstacks[0]->add_card(card, err);
        }
        else if(seedstacks[1]->get_stack_bean_type() == card->get_bean_type()){
            std::cout << "trade_handler@execute_trade :> put card on seed stack 1 due to matching type" << std::endl;
            seedstacks[1]->add_card(card, err);
        }
        else if(seedstacks[0]->get_stack_bean_type() == -1){
            std::cout << "trade_handler@execute_trade :> put card on seed stack 0 due to empty field" << std::endl;
            seedstacks[0]->add_card(card, err);
        }
        else if(seedstacks[1]->get_stack_bean_type() == -1){
            std::cout << "trade_handler@execute_trade :> put card on seed stack 1 due to empty field" << std::endl;
            seedstacks[1]->add_card(card, err);
        }
        else{
            std::cout << "trade_handler@execute_trade :> err no matching seed stack found" << std::endl;
            return false;
        }
        offered_cards_player_one->remove_card(card->get_id(), card, err);
        std::cout << "trade_handler@execute_trade :> removing handcards from player one" << std::endl;
        player_one->get_non_const_hand()->remove_card(card->get_id(), card, err);
    }
    std::cout << "trade_handler::execute_trade :> doin trade magic for requested and offerd cards for player two" << std::endl;
    while(offered_cards_player_two->get_nof_cards() != 0) {
        auto card = offered_cards_player_two->get_first_card();
        auto seedstacks = player_one->get_seed_stacks();
        assert(seedstacks.size() == 2);
        if(seedstacks[0]->get_stack_bean_type() == card->get_bean_type()) {
            std::cout << "trade_handler@execute_trade :> put card on seed stack 0 due to matching type" << std::endl;
            seedstacks[0]->add_card(card, err);
        }
        else if(seedstacks[1]->get_stack_bean_type() == card->get_bean_type()){
            std::cout << "trade_handler@execute_trade :> put card on seed stack 1 due to matching type" << std::endl;
            seedstacks[1]->add_card(card, err);
        }
        else if(seedstacks[0]->get_stack_bean_type() == -1){
            std::cout << "trade_handler@execute_trade :> put card on seed stack 0 due to empty field" << std::endl;
            seedstacks[0]->add_card(card, err);
        }
        else if(seedstacks[1]->get_stack_bean_type() == -1){
            std::cout << "trade_handler@execute_trade :> put card on seed stack 1 due to empty field" << std::endl;
            seedstacks[1]->add_card(card, err);
        }
        else{
            std::cout << "trade_handler@execute_trade :> err no matching seed stack found" << std::endl;
            return false;
        }
        offered_cards_player_two->remove_card(card->get_id(), card, err);
        std::cout << "trade_handler@execute_trade :> removing handcards from player two" << std::endl;
        player_two->get_non_const_hand()->remove_card(card->get_id(), card, err);
    }

    std::cout << "trade_handler::execute_trade :> checking if middle pile is empty" << std::endl;
    /* finish tradeing sequence if there are no more cards left on the middle stack */
    if(trade_state->get_middle_pile()->is_empty()) {
        std::cout << "trade_handler@execute_trade :> finishing trade due to empty middle stack" << std::endl;
        finish_trade();
    }
    std::cout << "trade_handler@execute_trade :> trade executed" << std::endl;
    return true;
}

trade_state* trade_handler::get_trade_state() {
    return this->_trade_state;
}