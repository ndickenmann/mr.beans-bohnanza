//
// Created by nicolas on 11.05.23.
//
#ifndef LAMA_TRADINGPANEL_H
#define LAMA_TRADINGPANEL_H

#include <wx/wx.h>
#include "../../common/trade_handler.h"
#include "../../common/trade_state.h"
#include "../../common/game_state/cards/middle_pile.h"



class TradingPanel : public wxPanel {

public:
    TradingPanel(wxWindow *parent);
    void buildTradeState(trade_state* tradeState, player* me);

private:
    void buildFirstRow(wxBoxSizer* vercticalSizer, trade_state* tradeState, player* me, player* activePlayer);
    void buildSecondRow(wxBoxSizer* vercticalSizer);
    void buildActiveRow(wxBoxSizer* vercticalSizer, trade_state* tradeState,player* me, player* activePlayer);
    void buildPassivRow(wxBoxSizer* vercticalSizer, trade_state* tradeState,player* me, player* activePlayer, player* player);
    void buildUrCards(wxBoxSizer* vercticalSizer, trade_state* tradeState, player* me, player* activePlayer);

    void activateRequestingCard(player* me, player* activePlayer,card currentcard, std::vector<player*> players);
    void activateOfferingCard(player *me, player *activePlayer, card currentcard, std::vector<player*> players);

    // define key constant layout values
    wxSize const cardSize = wxSize(80, 124);
    wxSize const cardSizeSmaller = wxSize(51, 80);


};
#endif //LAMA_TRADINGPANEL_H
