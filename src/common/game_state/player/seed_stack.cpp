#include "seed_stack.h"

#include "../../exceptions/BohnanzaException.h"
#include "../../serialization/vector_utils.h"

seed_stack::seed_stack() : unique_serializable() { }

seed_stack::seed_stack(std::string id) : unique_serializable(id) { }

// deserialization constructor
seed_stack::seed_stack(std::string id, std::vector<card*> traded_beans) : unique_serializable(id) {
    this->_traded_beans = traded_beans;
}

seed_stack::~seed_stack() {
    for (int i = 0; i < _traded_beans.size(); i++) {
        delete _traded_beans.at((i));
        _traded_beans.at(i) = nullptr;
    }
    _traded_beans.clear();
}

int seed_stack::get_nof_beans() const {
    return _traded_beans.size();
}

int seed_stack::get_stack_bean_type() const {
    if(this->get_nof_beans() == 0){
        return -1;
    }
    else{
        return _traded_beans[0]->get_bean_type();
    }
}


const std::vector<card*>  seed_stack::get_beans() const {
    return _traded_beans;
}


bool seed_stack::try_get_bean(const std::string &card_id, card *&seed_stack_bean) const {
    auto it = std::find_if(_traded_beans.begin(), _traded_beans.end(),
                           [&card_id](const card* x) { return x->get_id() == card_id;});
    if (it < _traded_beans.end()) {
        seed_stack_bean = *it;
        return true;
    }
    return false;
}

bool seed_stack::can_plant(const std::string &card_id, card *&field_stack_bean) const {
    auto it = std::find_if(_traded_beans.begin(), _traded_beans.end(),
                           [&card_id](const card* x) { return x->get_id() == card_id;});
    if (it < _traded_beans.end()) {
        field_stack_bean = *it;
        // Should not happen as a card cannot be planted twice
        return false;
    }
    else if(_traded_beans.size() == 0) return true;
    else return (_traded_beans.at(0)->can_be_played_on(field_stack_bean));
}



card* seed_stack::remove_card(card* card) {
    auto pos = std::find(_traded_beans.begin(), _traded_beans.end(), card);
    if (pos >= _traded_beans.begin() && pos < _traded_beans.end()){
        auto temp = *pos;
        _traded_beans.erase(pos);
        return temp;
    }
    else return nullptr;
}


// Not sure if we need this function
std::vector<card*>::iterator seed_stack::get_card_iterator() {
    return _traded_beans.begin();
}


//#ifdef BOHNANZA_SERVER
void seed_stack::empty_stack() {
    // remove all cards (if any) and clear it
    for (int i = 0; i < _traded_beans.size(); i++) {
        delete _traded_beans[i];
    }
    _traded_beans.clear();
}

bool seed_stack::add_card(card* new_card, std::string &err) {
    _traded_beans.push_back(new_card);
    return true;
}

bool seed_stack::remove_card(std::string card_id, card*& played_card, std::string &err) {
    played_card = nullptr;
    auto it = std::find_if(_traded_beans.begin(), _traded_beans.end(),
                           [&card_id](const card* x) { return x->get_id() == card_id;});
    if (it < _traded_beans.end()) {
        played_card = remove_card(*it);
        return true;
    } else {
        err = "Could not play card, as the requested card was not on the player's hand.";
        return false;
    }
}
//#endif


void seed_stack::write_into_json(rapidjson::Value &json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);
    json.AddMember("seed_stack", vector_utils::serialize_vector(_traded_beans, allocator), allocator);
}

seed_stack *seed_stack::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") && json.HasMember("seed_stack")) {
        std::vector<card*> deserialized_cards = std::vector<card*>();
        for (auto &serialized_card : json["seed_stack"].GetArray()) {
            deserialized_cards.push_back(card::from_json(serialized_card.GetObject()));
        }
        return new seed_stack(json["id"].GetString(), deserialized_cards);
    } else {
        throw BohnanzaException("Could not parse seed_stack from json. 'seed_stack' were missing.");
    }
}
