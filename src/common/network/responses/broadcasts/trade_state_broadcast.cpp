#include "trade_state_broadcast.h"

#include "../../../exceptions/BohnanzaException.h"
#include "../../../serialization/json_utils.h"

#define BOHN_CLIENT
#ifdef BOHN_CLIENT

#include "../../../../client/GameController.h"
#endif

trade_state_broadcast::trade_state_broadcast(server_response::base_class_properties props, rapidjson::Value* state_json) :
        server_response(props),
        _state_json(state_json)
{ }

trade_state_broadcast::trade_state_broadcast(const trade_state& state) :
        server_response(server_response::create_base_class_properties(ResponseType::trade_update_broadcast))
{
#ifdef DEBUG
    std::cout << "trade_state.cpp.cpp@trade_state_broadcast: entered function" << std::endl;
#endif
    this->_state_json = state.to_json();
#ifdef DEBUG
    std::cout << "trade_state.cpp.cpp@trade_state_broadcast: end of function" << std::endl;
#endif
}


void trade_state_broadcast::write_into_json(rapidjson::Value &json,
                                            rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    server_response::write_into_json(json, allocator);
    json.AddMember("trade_state_json", *_state_json, allocator);
}

trade_state_broadcast *trade_state_broadcast::from_json(const rapidjson::Value& json) {
    if (json.HasMember("trade_state_json")) {
        return new trade_state_broadcast(server_response::extract_base_class_properties(json),
                                         json_utils::clone_value(json["trade_state_json"].GetObject()));
    } else {
        throw BohnanzaException("Could not parse trade_state_broadcast from json. state is missing.");
    }
}

trade_state_broadcast::~trade_state_broadcast() {
    if (_state_json != nullptr) {
        delete _state_json;
        _state_json = nullptr;
    }
}

rapidjson::Value* trade_state_broadcast::get_state_json() const {
    return _state_json;
}

#ifdef BOHN_CLIENT


void trade_state_broadcast::Process() const {
    try {
        trade_state* state = trade_state::from_json(*_state_json);
#ifdef DEBUG
        std::cout << "trade_state_broadcast@Process :> trade_state_broadcast@Process :> finished: " << state->get_finished()->get_value() << std::endl;
#endif
        if(state->get_finished()->get_value()){
            GameController::setStartedTrading(false);
#ifdef DEBUG
            std::cout << "trade_state_broadcast@Process :> setting startedTrading false" << std::endl;
#endif
        } else {
            GameController::setStartedTrading(true);
#ifdef DEBUG
            std::cout << "trade_state_broadcast@Process :> setting startedTrading true" << std::endl;
#endif
        }
        GameController::updateTradeState(state);

    } catch(std::exception& e) {
        std::cerr << "Failed to extract trade_state from trade_state_broadcast" << std::endl
                  << e.what() << std::endl;
    }
}

#endif
