#ifndef BOHNANZA_FIELD_STACK_H
#define BOHNANZA_FIELD_STACK_H

#include <vector>
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../cards/card.h"

class field_stack : public unique_serializable {

private:
    std::vector<card*> _planted_beans;

    field_stack(std::string id);
    field_stack(std::string id, std::vector<card*> planted_beans);
    card* remove_card(std::vector<card*>::iterator pos);
public:
    field_stack();
    ~field_stack();


// serializable interface
    static field_stack* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;

// accessors
    int get_count() const;
    int get_stack_bean_type() const;

    // returns number of coins gained from harvest or -1 if harvest did not work
    int harvest_beans() const;

    // checks if card can be planted on field stack. Actual planting is done by plant_bean by server
    bool can_plant(const std::string& card_id, card*& field_stack_card) const;

//#ifdef LAMA_SERVER
    // state update functions
    void setup_stack();

    // Adds a card to the field stack
    bool plant_bean(card* card, std::string& err);

    bool remove_card(std::string card_id, card*& played_card, std::string& err);
//#endif

    std::vector<card*>::iterator get_card_iterator();
};


#endif // BOHNANZA_SEED_STACK_H