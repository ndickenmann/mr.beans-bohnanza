#include "server_response.h"
#include "game_state_response.h"
#include "trade_state_response.h"
#include "broadcasts/trade_state_broadcast.h"
#include "broadcasts/game_state_broadcast.h"


// for deserialization
const std::unordered_map<std::string, ResponseType> server_response::_string_to_response_type = {
        {"req_response", ResponseType::req_response },
        {"trade_update_response", ResponseType::trade_update_response },
        {"trade_update_broadcast", ResponseType::trade_update_broadcast },
        {"state_diff_msg", ResponseType::state_diff_msg},
        {"full_state_msg", ResponseType::full_state_msg}
};
// for serialization
const std::unordered_map<ResponseType, std::string> server_response::_response_type_to_string = {
        { ResponseType::req_response,   "req_response" },
        { ResponseType::trade_update_response,   "trade_update_response" },
        { ResponseType::trade_update_broadcast,  "trade_update_broadcast" },
        { ResponseType::state_diff_msg, "state_diff_msg"},
        { ResponseType::full_state_msg, "full_state_msg"}
};

server_response::server_response(server_response::base_class_properties params):
        _type(params.type)
{ }

ResponseType server_response::get_type() const {
    return this->_type;
}

server_response::base_class_properties
server_response::create_base_class_properties(ResponseType type) {
    server_response::base_class_properties params;
    params.type = type;
    return params;
}

server_response::base_class_properties server_response::extract_base_class_properties(const rapidjson::Value& json) {
    if (json.HasMember("type") ) {
        return create_base_class_properties(
                server_response::_string_to_response_type.at(json["type"].GetString())
        );
    }
    else
    {
        throw BohnanzaException("Server Response did not contain type");
    }
}


server_response *server_response::from_json(const rapidjson::Value& json) {

    if (json.HasMember("type") && json["type"].IsString()) {
        std::string type = json["type"].GetString();
        ResponseType response_type = server_response::_string_to_response_type.at(type);

        if (response_type == ResponseType::req_response) {
            return game_state_response::from_json(json);
        }
        else if(response_type == ResponseType::trade_update_response) {
            return trade_state_response::from_json(json);
        }
        else if(response_type == ResponseType::trade_update_broadcast) {
            return trade_state_broadcast::from_json(json);
        }
        else if (response_type == ResponseType::full_state_msg) {
            return game_state_broadcast::from_json(json);
        } else {
            throw BohnanzaException("Encountered unknown ServerResponse type " + response_type);
        }
    }
    throw BohnanzaException("Could not determine type of ClientRequest");
}

void server_response::write_into_json(rapidjson::Value &json,
                                      rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    rapidjson::Value type_val(_response_type_to_string.at(this->_type).c_str(), allocator);
    json.AddMember("type", type_val, allocator);

}



