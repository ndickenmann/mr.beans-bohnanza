#include "trade_state_response.h"
#include "../../serialization/json_utils.h"
#include "../../exceptions/BohnanzaException.h"
#include "../../game_state/game_state.h"


#define BOHN_CLIENT
#ifdef BOHN_CLIENT
#include "../../../client/GameController.h"
#endif


trade_state_response::trade_state_response(server_response::base_class_properties props, std::string req_id, bool success, rapidjson::Value* state_json, std::string &err) :
        server_response(props),
        _req_id(req_id),
        _state_json(state_json),
        _success(success),
        _err(err)
{ }

trade_state_response::trade_state_response(std::string req_id, bool success, rapidjson::Value* state_json, std::string err):
        server_response(server_response::create_base_class_properties(ResponseType::trade_update_response )),
        _req_id(req_id),
        _state_json(state_json),
        _success(success),
        _err(err)
{ }


trade_state_response::~trade_state_response() {
    if (_state_json != nullptr) {
        delete _state_json;
        _state_json = nullptr;
    }
}

void trade_state_response::write_into_json(rapidjson::Value &json,
                                           rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    server_response::write_into_json(json, allocator);

    rapidjson::Value err_val(_err.c_str(), allocator);
    json.AddMember("err", err_val, allocator);

    rapidjson::Value req_id_val(_req_id.c_str(), allocator);
    json.AddMember("req_id", req_id_val, allocator);

    json.AddMember("success", _success, allocator);

    if (_state_json != nullptr) {
        json.AddMember("state_json", *_state_json, allocator);
    }
}


trade_state_response *trade_state_response::from_json(const rapidjson::Value& json) {
    if (json.HasMember("err") && json.HasMember("success")) {
        std::string err = json["err"].GetString();

        rapidjson::Value* state_json = nullptr;
        if (json.HasMember("state_json")) {
            state_json = json_utils::clone_value(json["state_json"].GetObject());
        }
        return new trade_state_response(
                server_response::extract_base_class_properties(json),
                json["req_id"].GetString(),
                json["success"].GetBool(),
                state_json,
                err);
    } else {
        throw BohnanzaException("Could not parse trade_state_response from json. err or success is missing.");
    }
}

#ifdef BOHN_CLIENT
void trade_state_response::Process() const {
    std::cout << "reached trade_state_response process" << std::endl;
    if (_success) {
        if (this->_state_json != nullptr) {
            trade_state* state = trade_state::from_json(*_state_json);
#ifdef DEBUG
            std::cout << "successfully read trade state from json in trade_state_response" << std::endl;
            std::cout << "trade_state_response@Process :> finished: " << state->get_finished()->get_value() << std::endl;
#endif
            if(state->get_finished()->get_value()){
                GameController::setStartedTrading(false);
#ifdef DEBUG
                std::cout << "trade_state_response@Process :> setting startedTrading false" << std::endl;
#endif
            } else {
                GameController::setStartedTrading(true);
#ifdef DEBUG
                std::cout << "trade_state_response@Process :> setting startedTrading true" << std::endl;
#endif
            }
            GameController::updateTradeState(state);
#ifdef DEBUG
            std::cout << "updated trade state in game controller" << std::endl;
#endif
        } else {
            GameController::showError("Network error", "Expected a state as JSON inside the trade_state_response. But there was none.");
        }
    } else {
        GameController::showError("Not possible", _err);
    }

}

#endif
