#include "middle_pile.h"

#include <random>
#include "../../serialization/vector_utils.h"

#include "../../exceptions/BohnanzaException.h"

// deserialization constructor
middle_pile::middle_pile(std::string id, std::vector<card*> &cards)
        : unique_serializable(id),
          _cards(cards)
{ }

// from_diff constructor
middle_pile::middle_pile(std::string id) : unique_serializable(id) { }


middle_pile::middle_pile(std::vector<card*> &cards)
        : unique_serializable(), _cards(cards)
{ }


middle_pile::middle_pile() : unique_serializable() { }

middle_pile::~middle_pile() {
    for (card* & _card : _cards) {
        delete _card;
    }
    _cards.clear();
}

bool middle_pile::is_empty() const noexcept  {
    return _cards.empty();
}

int middle_pile::get_nof_cards() const noexcept  {
    return _cards.size();
}


// still have to be changed accordingly

//#ifdef LAMA_SERVER
void middle_pile::reset_middle() {
    // remove all cards (if any) and add the change to the "cards" array_diff
    for (int i = 0; i < _cards.size(); i++) {
        delete _cards[i];
    }
    _cards.clear();
}

bool middle_pile::add_two_cards(card*& first_card, card*& second_card, std::string& err)  {
    if (_cards.empty()) {
        _cards.push_back(first_card);
        _cards.push_back(second_card);
        return 1;

    } else {
        err = "There are still cards in the middle";
    }
    return false;
}

card* middle_pile::get_card(int card, std::string& err){
    if (!is_empty()) return _cards.at(card);
    else{
        err = "Card could not be removed from middle_pile";
        return nullptr;
    }
}

card* middle_pile::remove_card(int remove_card, std::string& err){
    if (!is_empty()){
        card* temp = _cards.at(remove_card);
        if(temp != nullptr) {
            _cards.erase(_cards.begin() + remove_card);
            std::cout << "middle_pile@remove_card :> successfully removed card" << std::endl;
        } else {
            std::cout << "middle_pile@remove_card :> failed to removed card" << std::endl;
            return nullptr;
        }
        //_cards.at(remove_card) = nullptr; // nullptr cant be handled by to json
        return temp;
    }
}

//#endif


void middle_pile::write_into_json(rapidjson::Value &json, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    unique_serializable::write_into_json(json, allocator);
    json.AddMember("middle_cards", vector_utils::serialize_vector(_cards, allocator), allocator);
}


middle_pile *middle_pile::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") && json.HasMember("middle_cards")) {
        std::vector<card*> deserialized_cards = std::vector<card*>();
        for (auto &serialized_card : json["middle_cards"].GetArray()) {
            deserialized_cards.push_back(card::from_json(serialized_card.GetObject()));
        }
        return new middle_pile(json["id"].GetString(), deserialized_cards);
    } else {
        throw BohnanzaException("Could not parse middle_pile from json. 'id' or 'cards' were missing.");
    }
}