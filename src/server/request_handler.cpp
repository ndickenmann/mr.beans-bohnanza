#ifndef BOHN_REQUEST_HANDLER_CPP
#define BOHN_REQUEST_HANDLER_CPP

#include "request_handler.h"
#include <vector>
#include "../common/serialization/unique_serializable.h"

#include "player_manager.h"
#include "game_instance_manager.h"
#include "game_instance.h"

#include "../common/network/requests/join_game_request.h"
#include "../common/network/requests/plant_card_request.h"
#include "../common/network/requests/harvest_field_request.h"
#include "../common/network/requests/update_trade_request.h"

server_response* request_handler::handle_request(const client_request* const req) {

#ifdef DEBUG
    std::cout<<"request_handler@handler_request: Request handling" <<std::endl;
#endif

    // Prepare variables that are used by every request type
    player* player;
    std::string err;
    game_instance* game_instance_ptr = nullptr;

    // Get common properties of requests
    RequestType type = req->get_type();
    std::string req_id = req->get_req_id();
    std::string player_id = req->get_player_id();


    // Switch behavior according to request type
    switch(type) {

        // ##################### JOIN GAME #####################  //
        case RequestType::join_game: {
            std::string player_name = ((join_game_request *) req)->get_player_name();
#ifdef DEBUG
            std::cout<<"request_handler@handler_request:Want to join: " << player_name<<std::endl;
#endif
            // Create new player or get existing one with that name
            player_manager::add_or_get_player(player_name, player_id, player);
#ifdef DEBUG
            std::cout<<"request_handler@handler_request:Added player" <<std::endl;
#endif
            
            if (game_instance_manager::try_add_player_to_game(player, game_instance_ptr, err)) {
                 // game_instance_ptr got updated to the joined game
#ifdef DEBUG
                 std::cout<<"request_handler@handler_request:Player added to game"<<std::endl;
#endif
                 // return response with full game_state attached
                return new game_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                game_instance_ptr->get_game_state()->to_json(), err);
            } else {
                // failed to find game to join
#ifdef DEBUG
                std::cout << "failed adding player to game"<<std::endl;
#endif
                return new game_state_response(/*"",*/ req_id, false, nullptr, err);
            }
        }

        // ##################### START GAME ##################### //
        case RequestType::start_game: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                if (game_instance_ptr->start_game(player, err)) {
                    return new game_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                    game_instance_ptr->get_game_state()->to_json(), err);
                }
            }
            return new game_state_response(/*"",*/ req_id, false, nullptr, err);
        }

        // ##################### PLANT CARD ##################### //
        case RequestType::plant_card: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                std::string card_id = ((plant_card_request *) req)->get_card_id();
                if (game_instance_ptr->plant_card(player, card_id, err)) {
                    return new game_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                    game_instance_ptr->get_game_state()->to_json(), err);
                }
            }
            return new game_state_response(/*"",*/ req_id, false, nullptr, err);
        }

        // ##################### HARVEST FIELD ##################### //
        case RequestType::harvest_field: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                std::string field_id = ((harvest_field_request *) req)->get_field_id();
                if (game_instance_ptr->harvest_field(player, field_id, err)) {
                    return new game_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                    game_instance_ptr->get_game_state()->to_json(), err);
                }
            }
            return new game_state_response(/*"",*/ req_id, false, nullptr, err);
        }

        // ##################### UPDATE TRADE  ##################### //
        case RequestType::update_trade: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                trade_state* _trade_state = ((update_trade_request *) req)->get_trade_state();
                if (game_instance_ptr->update_trade(player, _trade_state, err)) {
                    return new trade_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                    game_instance_ptr->get_trade_handler()->get_trade_state()->to_json(), err);
                }
            }
            return new trade_state_response(/*"",*/ req_id, false, nullptr, err);
        }

        // ##################### INIT TRADE  ##################### //
        case RequestType::init_trade: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                if (game_instance_ptr->init_trade(player, err)) {
#ifdef DEBUG
                    std::cout << "request_handler@handler_request:returning successful init_trade response" << std::endl;
#endif
                    return new trade_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                     game_instance_ptr->get_trade_handler()->get_trade_state()->to_json(), err);
                }
            }
#ifdef DEBUG
            std::cout << "request_handler@handler_request:returning unsuccessful init_trade response" << std::endl;
#endif
            return new trade_state_response(/*"",*/ req_id, false, nullptr, err);
        }

        // ##################### FINISH TRADE  ##################### //
        case RequestType::finish_trade: {
#ifdef DEBUG
            std::cout << "request_handler.cpp@handle_request: Trying to handle finish_trade request" << std::endl;
#endif
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
#ifdef DEBUG
                std::cout << "request_handler.cpp@handle_request: successfully got player and game instance" << std::endl;
#endif
                if (game_instance_ptr->finish_trade(player, err)) {
#ifdef DEBUG
                    std::cout << "request_handler.cpp@handle_request: finished trade" << std::endl;
#endif
                    return new trade_state_response(/*game_instance_ptr->get_id(),*/ req_id, true,
                                                                                    game_instance_ptr->get_trade_handler()->get_trade_state()->to_json(), err);
                }
#ifdef DEBUG
                std::cout << "request_handler.cpp@handle_request: did not finish trade" << std::endl;
#endif
            }
            return new trade_state_response(/*"",*/ req_id, false, nullptr, err);
        }

        // ##################### UNKNOWN REQUEST ##################### //
        default:
            return new game_state_response(/*"",*/ req_id, false, nullptr, "Unknown RequestType " + type);
    }
}

#endif //BOHN_REQUEST_HANDLER_CPP
