#include "Bohnanza.h"


// Application entry point
bool Bohnanza::OnInit()
{
    // Allow loading of JPEG  and PNG image files
    wxImage::AddHandler(new wxJPEGHandler());
    wxImage::AddHandler(new wxPNGHandler());

    // Open main game window
    GameWindow* gameWindow = new GameWindow(
            "Bohnanza", // title of window,
            wxDefaultPosition, // position of the window
            wxDefaultSize // size of the window
    );
    gameWindow->Show(true);
    gameWindow->Fit();

    // Initialize game controller
    GameController::init(gameWindow);

    return true;
}